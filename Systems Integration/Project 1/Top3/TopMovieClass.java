import java.io.Serializable;

public class TopMovieClass implements Serializable{
	private int rank;
	private String titulo;
	private String genero;

	TopMovieClass(int rank,String titulo){
		this.rank=rank;
		this.titulo=titulo;

	}

	public int getRank(){
		return rank;
	}

	public String getTitulo(){
		return titulo;
	}

	public String getGenero(){
		return genero;
	}

	public void setRank(int rank){
		this.rank=rank;
	}

	public void setTitulo(String titulo){
		this.titulo=titulo;
	}

	public void setGenero(String genero){
		this.genero=genero;
	}	
}