import java.util.ArrayList;
import java.io.Serializable;

public class TopMoviesClass implements Serializable{
	private ArrayList<TopMovieClass> arrayTop;

	TopMoviesClass(){
		arrayTop=new ArrayList<TopMovieClass>();
	}

	public TopMovieClass getMovie(int i){
		return arrayTop.get(i);
	}

	public void setMovie(TopMovieClass movie,int pos){
		arrayTop.add(pos,movie);
	}

	public void setMovie1(TopMovieClass movie){
		arrayTop.add(movie);
	}

	public void remMovie(int pos){
		arrayTop.remove(pos);
	}

	public int getSize(){
		return arrayTop.size();
	}
}