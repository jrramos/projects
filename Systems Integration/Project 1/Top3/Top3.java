	import javax.jms.Connection;
	import javax.jms.ConnectionFactory;
	import javax.jms.Destination;
	import javax.jms.JMSException;
	import javax.jms.MessageConsumer;
	import javax.jms.Session;
	import javax.jms.TextMessage;
	import javax.naming.InitialContext;
	import javax.naming.NamingException;
	import javax.jms.*;
	import java.io.*;
	import org.w3c.dom.ProcessingInstruction;
	import javax.xml.bind.JAXBContext;
	import javax.xml.bind.JAXBException;
	import javax.xml.bind.Marshaller;
	import javax.xml.bind.Unmarshaller;
	import java.io.FileInputStream;
	import javax.xml.validation.Schema;
	import javax.xml.validation.SchemaFactory;
	import javax.xml.validation.Validator;
	import javax.xml.XMLConstants;
	import org.xml.sax.SAXException;
	import Movies.*;
	import java.util.regex.Pattern;
	import java.util.regex.Matcher;
	import org.w3c.dom.ProcessingInstruction;
	import javax.naming.InitialContext;
	import java.io.*;
	import javax.xml.bind.PropertyException;
	import org.jboss.remoting3.NotOpenException;

	public class Top3 {
		TopicSubscriber topicSubscriber;
		InitialContext ctx;
		Topic topic;
		TopicConnectionFactory connFactory;
		TopicConnection topicConn;
		ConnectionState connectionState;
		KeyboardReader kBReader;

		public static void main(String[] args)  {
			new Top3();
		}

		Top3(){
			connectionState=new ConnectionState();
			while(!initSubscribe());
			kBReader=new KeyboardReader(topicConn,connectionState);
			receiveFromTopic();
		}


		public boolean initSubscribe() {

			 // get the initial context
			try{
				ctx = new InitialContext();
																																						
				 // lookup the topic object
				topic = (Topic) ctx.lookup("jms/topic/project");
																																						
				 // lookup the topic connection factory
				connFactory = (TopicConnectionFactory) ctx.lookup("jms/RemoteConnectionFactory");
																																						
				 // create a topic connection
				topicConn = connFactory.createTopicConnection("admin1", "admin");

				topicConn.setClientID("admin2");
																																						
				 // create a topic session
				TopicSession topicSession = topicConn.createTopicSession(false,Session.AUTO_ACKNOWLEDGE);
																																						
				 // create a topic subscriber
				topicSubscriber=topicSession.createDurableSubscriber(topic, "mySub");
				

				topicConn.start();

				return true;

			}catch(JMSException|NamingException e){
				System.out.println(e.getMessage());
				System.out.println("Topic is down! We will retry the connection in 5 seconds...");
				try{
					Thread.sleep(5000);
				}catch(InterruptedException s){
					System.out.println(s.getMessage());
				}
				return false;
			}
		}

		public boolean retryConnection() {

			try{
				topicConn.close();
			 // create a topic connection
				topicConn = connFactory.createTopicConnection("admin1", "admin");

				topicConn.setClientID("admin2");
																																						
				 // create a topic session
				TopicSession topicSession = topicConn.createTopicSession(false,Session.AUTO_ACKNOWLEDGE);
																																						
				 // create a topic subscriber
				topicSubscriber=topicSession.createDurableSubscriber(topic, "mySub");

				topicConn.start();

				kBReader.topicConn=topicConn;
				
				System.out.println("Connection to the topic retablished!");
				return true;
			}catch(Exception e){
				//System.out.println(e.getMessage());
				System.out.println("Topic is down! We will retry the connection in 5 seconds...");
				try{
					Thread.sleep(5000);
				}catch(InterruptedException s){
					System.out.println(s.getMessage());
				}

				return false;
			}
		}
		public void receiveFromTopic(){

			Movies moviesXML=null;
			while(true){
				try{
					Message message=topicSubscriber.receive();
					String docXML=message.getStringProperty("xml");
					docXML=docXML.replace('\u0000', ' ');

					moviesXML=getClassesFromXML(docXML);
					System.out.println("Message received from topic!\n");
					verNovoRank(moviesXML);
				}catch(JMSException|NullPointerException e){
					//System.out.println(e.getMessage());
	
					while(connectionState.state==1 && !retryConnection());
					
				}
			}
		}

		public void verNovoRank(Movies moviesXML){
			int a,e,i,igual=0;
			TopMoviesClass topMovies=null;
			try{
				FileInputStream fin = new FileInputStream("Top3.file");
				ObjectInputStream ois = new ObjectInputStream(fin);
				topMovies = (TopMoviesClass) ois.readObject();
				ois.close();
			}catch(FileNotFoundException w){
				System.out.println(w.getMessage());
			}catch(IOException t){
				 System.out.println(t.getMessage());
			}catch(ClassNotFoundException u){
				 System.out.println(u.getMessage());
			}

			for(i=0;i<moviesXML.getMovie().size();i++){
				for(a=topMovies.getSize()-1;a>=0;a--){
					if(moviesXML.getMovie().get(i).getRank()<=topMovies.getMovie(a).getRank()){
						if(moviesXML.getMovie().get(i).getTitle().compareTo(topMovies.getMovie(a).getTitulo())==0){
							igual=1;
							break;
						}	
						else if(a==2){
							break;
						}else{
							topMovies.setMovie(new TopMovieClass(moviesXML.getMovie().get(i).getRank(),moviesXML.getMovie().get(i).getTitle()),a+1);
							topMovies.remMovie(3);
							break;
						}
					}else if(moviesXML.getMovie().get(i).getRank()>topMovies.getMovie(a).getRank())
						continue;
				}
				if(igual==0 && a<0){
					topMovies.setMovie(new TopMovieClass(moviesXML.getMovie().get(i).getRank(),moviesXML.getMovie().get(i).getTitle()),0);
					topMovies.remMovie(3);
				} 
				igual=0;
			}

			for(e=0;e<topMovies.getSize();e++)
				System.out.println("Top "+(e+1)+": \n"+"Name: "+topMovies.getMovie(e).getTitulo()+"\nRank:"+topMovies.getMovie(e).getRank()+"\n");
			
			try{
				FileOutputStream fin = new FileOutputStream("Top3.file");
				ObjectOutputStream ois = new ObjectOutputStream(fin);
				ois.writeObject(topMovies);
				ois.close();
			}catch(Exception o){
				System.out.println(o.getMessage());
			} 

	}

	public Movies getClassesFromXML(String docXML){
		PrintStream out = null;
		Movies moviesXML=null;
		 try {
					out = new PrintStream(new FileOutputStream("XMLFile.xml"));
					out.print(docXML);
				}catch(Exception q){
					System.out.println(q.getMessage());
				}
				finally {
					if (out != null)
						out.close();
				}

				try{
					JAXBContext context = JAXBContext.newInstance(Movies.class);
					Unmarshaller unMarshaller = context.createUnmarshaller();
					moviesXML = (Movies) unMarshaller.unmarshal(new FileInputStream("XMLFile.xml"));
				}catch(JAXBException b){
					System.out.println(b.getMessage());
				}catch(FileNotFoundException c){
					System.out.println(c.getMessage());
				}
		return moviesXML;
	 }
}

class KeyboardReader extends Thread{
	TopicConnection topicConn;
	ConnectionState connectionState;

	KeyboardReader(TopicConnection topicConn,ConnectionState connectionState){
		this.topicConn=topicConn;
		this.connectionState=connectionState;
		this.start();
	}

	public void run(){
		InputStreamReader input= new InputStreamReader(System.in);;
		BufferedReader reader= new BufferedReader(input);
		System.out.println("\nYou are connected to the Topic");
		try{
			while(true){
				if(((String)(reader.readLine())).compareTo("exit")==0){
					connectionState.state=0;
					this.topicConn.close();
					System.exit(0);
				}
			}
		}catch(IOException|JMSException e){
			System.out.println(e.getMessage());
		}
	}
}

class ConnectionState{
	char state;

	ConnectionState(){
		state=1;
	}
}