	import java.util.Scanner;
	import java.util.ArrayList;
	import javax.jms.Connection;
	import javax.jms.ConnectionFactory;
	import javax.jms.Destination;
	import javax.jms.JMSException;
	import javax.jms.MessageConsumer;
	import javax.jms.Session;
	import javax.jms.TextMessage;
	import javax.naming.InitialContext;
	import javax.naming.NamingException;
	import javax.jms.*;
	import java.io.*;

	public class MoviesSummary{
		TopicSubscriber topicSubscriber;
		InitialContext ctx;
		Topic topic;
		TopicConnectionFactory connFactory;
		TopicConnection topicConn;
		ConnectionState connectionState;
		KeyboardReader kBReader;

		public static void main(String[] args) {
			new MoviesSummary();
		}

		MoviesSummary(){
			connectionState=new ConnectionState();
			while(!initSubscribe());
			kBReader=new KeyboardReader(topicConn,connectionState);
			receiveFromTopic();
		}

		public boolean initSubscribe() {

			try{
			 // get the initial context
			ctx = new InitialContext();
																																					
			 // lookup the topic object
			topic = (Topic) ctx.lookup("jms/topic/project");
																																					
			 // lookup the topic connection factory
			connFactory = (TopicConnectionFactory) ctx.lookup("jms/RemoteConnectionFactory");
																																					
			 // create a topic connection
			topicConn = connFactory.createTopicConnection("admin1", "admin");

			topicConn.setClientID("admin1");
																																					
			 // create a topic session
			TopicSession topicSession = topicConn.createTopicSession(false,Session.AUTO_ACKNOWLEDGE);
																																					
			 // create a topic subscriber
			topicSubscriber =topicSession.createDurableSubscriber(topic, "mySub");
			

			topicConn.start();

			return true;

			}catch(JMSException|NamingException e ){
				System.out.println("Topic is down! We will retry the connection in 5 seconds...");
				try{
					Thread.sleep(5000);
				}catch(InterruptedException s){
					System.out.println(s.getMessage());
				}
				return false;
			}
		}

		public boolean retryConnection() {

			try{
				topicConn.close();
			 // create a topic connection
				topicConn = connFactory.createTopicConnection("admin1", "admin");

				topicConn.setClientID("admin1");
																																						
				 // create a topic session
				TopicSession topicSession = topicConn.createTopicSession(false,Session.AUTO_ACKNOWLEDGE);
																																						
				 // create a topic subscriber
				topicSubscriber=topicSession.createDurableSubscriber(topic, "mySub");

				topicConn.start();

				kBReader.topicConn=topicConn;
				
				System.out.println("Connection to the topic retablished!");
				return true;
			}catch(Exception e){

				System.out.println("Topic is down! We will retry the connection in 5 seconds...");
				try{
					Thread.sleep(5000);
				}catch(InterruptedException s){
					System.out.println(s.getMessage());
				}

				return false;
			}
		}

		 public void receiveFromTopic(){
			 
			PrintStream out = null;
			String docXML=null;
			Message message;
			while(true){
				try{
					message=topicSubscriber.receive();
					docXML=message.getStringProperty("xml");
					docXML=docXML.replace('\u0000', ' ');
					docXML = docXML.substring(0, 56) + "<?xml-stylesheet type=\"text/xsl\" href=\"XSLFile.xsl\"?>\n" + docXML.substring(56, docXML.length());
					System.out.println("Message received from topic!");

					try {
						out = new PrintStream(new FileOutputStream("XMLFile.xml"));
						out.print(docXML);
					}catch(Exception q){}
					finally {
							if (out != null) out.close();
					}
				}catch(JMSException|NullPointerException e){
					while(connectionState.state==1 && !retryConnection());
				}

			}

		}
			
	}

class KeyboardReader extends Thread{
	TopicConnection topicConn;
	ConnectionState connectionState;

	KeyboardReader(TopicConnection topicConn,ConnectionState connectionState){
		this.topicConn=topicConn;
		this.connectionState=connectionState;
		this.start();
	}

	public void run(){
		InputStreamReader input= new InputStreamReader(System.in);;
		BufferedReader reader= new BufferedReader(input);
		System.out.println("\nYou are connected to the Topic");
		try{
			while(true){
				if(((String)(reader.readLine())).compareTo("exit")==0){
					connectionState.state=0;
					this.topicConn.close();
					System.exit(0);
				}
			}
		}catch(IOException|JMSException e){
			System.out.println(e.getMessage());
		}
	}
}

class ConnectionState{
	char state;

	ConnectionState(){
		state=1;
	}
}