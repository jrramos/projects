<?xml version="1.0" encoding="UTF-8"?>
<html xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xsl:version="1.0">
	<head>
		<title> Movies </title>
	</head>
	<body>
		<p>	
			<h1> Drama</h1>
			<table border="1">
				<tr>
				<td><b> Title </b></td>
				<td><b> Year </b></td>
				<td><b> Rank </b></td>
				<td><b> Genre </b></td>
				<td><b> Director </b></td>
				<td><b> Actor </b></td>
				</tr>
				<xsl:for-each select="//Movie[genres/genre='Drama']">
				<tr>
				<td> <xsl:value-of select="title"/> </td>
				<td> <xsl:value-of select="year"/> </td>
				<td> <xsl:value-of select="rank"/> </td>
				<td> <p><xsl:value-of select="'Drama'"/></p>
				</td>
				<td>
				<xsl:for-each select="directors/director">
					<p><xsl:value-of select="."/></p>
				</xsl:for-each>
				</td>
				<td>
				<xsl:for-each select="actors/actor">
					<p><xsl:value-of select="."/></p>
				</xsl:for-each>
				</td>
				</tr>
				</xsl:for-each>
			</table>
		</p>
		<p>	
			<h1> Thriller</h1>
			<table border="1">
				<tr>
				<td><b> Title </b></td>
				<td><b> Year </b></td>
				<td><b> Rank </b></td>
				<td><b> Genre </b></td>
				<td><b> Director </b></td>
				<td><b> Actor </b></td>
				</tr>
				<xsl:for-each select="//Movie[genres/genre='Thriller']">
				<tr>
				<td> <xsl:value-of select="title"/> </td>
				<td> <xsl:value-of select="year"/> </td>
				<td> <xsl:value-of select="rank"/> </td>
				<td> 
					<p><xsl:value-of select="'Thriller'"/></p>
				 	
				</td>
				<td>
				<xsl:for-each select="directors/director">
					<p><xsl:value-of select="."/></p>
				</xsl:for-each>
				</td>
				<td>
				<xsl:for-each select="actors/actor">
					<p><xsl:value-of select="."/></p>
				</xsl:for-each>
				</td>
				</tr>
				</xsl:for-each>
			</table>
		</p>
		<p>	
			<h1> Action</h1>
			<table border="1">
				<tr>
				<td><b> Title </b></td>
				<td><b> Year </b></td>
				<td><b> Rank </b></td>
				<td><b> Genre </b></td>
				<td><b> Director </b></td>
				<td><b> Actor </b></td>
				</tr>
				<xsl:for-each select="//Movie[genres/genre='Action']">
				<tr>
				<td> <xsl:value-of select="title"/> </td>
				<td> <xsl:value-of select="year"/> </td>
				<td> <xsl:value-of select="rank"/> </td>
				<td><p><xsl:value-of select="'Action'"/></p>
				</td>
				<td>
				<xsl:for-each select="directors/director">
					<p><xsl:value-of select="."/></p>
				</xsl:for-each>
				</td>
				<td>
				<xsl:for-each select="actors/actor">
					<p><xsl:value-of select="."/></p>
				</xsl:for-each>
				</td>
				</tr>
				</xsl:for-each>
			</table>
		</p>
	</body>
</html>