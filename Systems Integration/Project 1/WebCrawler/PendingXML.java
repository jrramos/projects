import java.util.ArrayList;
import java.io.Serializable;

public class PendingXML implements Serializable{
	private ArrayList<String> xml;

	PendingXML(){
		xml=new ArrayList<String>();
	}

	public void setXMl(String newXML){
		xml.add(newXML);
	}

	public String getXML(int pos){
		return xml.get(pos);
	}

	public void remXML(int pos){
		xml.remove(pos);
	}

	public int getSize(){
		return xml.size();
	}
}