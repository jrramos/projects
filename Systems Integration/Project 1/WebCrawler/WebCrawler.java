import java.util.Scanner;
import java.util.ArrayList;
import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.IOException;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import javax.xml.transform.dom.DOMSource;
import org.w3c.dom.Document;
import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.FileInputStream;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.XMLConstants;
import org.xml.sax.SAXException;
import Movies.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import org.w3c.dom.ProcessingInstruction;
import javax.jms.Message;
import javax.jms.Topic;
import javax.jms.Session;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.JMSException;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;
import javax.jms.TopicConnection;
import javax.naming.NamingException;
import javax.jms.TopicConnectionFactory;
import javax.naming.InitialContext;
import java.io.*;
import java.util.List;
import javax.xml.bind.PropertyException;


public class WebCrawler{

    Document docXML=null;
    File file=null;
    TopicPublisher topicPublisher;
    TopicSession topicSession;
    TopicConnection topicConn;

    public static void main(String[] args) throws IOException{
    	
    	if(args.length!=1){
    		System.out.println("Wrong number of input Strings!");
    		System.exit(0);
    	}
    	new WebCrawler(args[0]);
    }

    public WebCrawler(String page){
    	Movies moviesClass=null;
    	try{
    		while(moviesClass==null){
    			moviesClass=classProducer(page);
    		}
    		xmlProducer(moviesClass);
    		xmlValidation();
    		initProducer();
    		sendMessage();
    	}catch(Exception e){
    		System.out.println(e.getMessage());
    	}
    }

    public String getContent() {
    	String content=null;
    	try {
	       FileReader reader = new FileReader(file);
	       char[] chars = new char[(int) file.length()];
	       reader.read(chars);
	       content = new String(chars);
	       reader.close();
		} catch (IOException e) {
		       e.printStackTrace();
		}

		return content;
    }

	public void initProducer() {

     	PendingXML arrayPending=null;

		try{
	   		// get the initial context
	       	InitialContext ctx = new InitialContext();
	    	// lookup the topic object
	       	Topic topic = (Topic) ctx.lookup("jms/topic/project");
	                                                                          
	       // lookup the topic connection factory
	       	TopicConnectionFactory connFactory = (TopicConnectionFactory) ctx.
	           lookup("jms/RemoteConnectionFactory");
	                                                                          
	       // create a topic connection
	       	topicConn = connFactory.createTopicConnection("admin", "admin1");
	                                                                          
	       // create a topic session
	       	topicSession = topicConn.createTopicSession(false, Session.AUTO_ACKNOWLEDGE);
	                                                                          
	      	// create a topic publisher
	      	topicPublisher = topicSession.createPublisher(topic);

	      	
      	}catch(JMSException|NamingException e){

      		System.out.println("Server is down! Your message will be pending...");

      		arrayPending=getPendingMessages();

			arrayPending.setXMl(getContent());

			setPendingMessages(arrayPending);

			System.exit(0);
      	}
      }

    public PendingXML getPendingMessages(){
    	ObjectInputStream ois=null;
     	FileInputStream fin=null;
     	PendingXML pending=null;

    	try{
			fin = new FileInputStream("pendingXML.file");
			ois = new ObjectInputStream(fin);
			pending = (PendingXML) ois.readObject();
			ois.close();
		}catch(FileNotFoundException w){
			System.out.println(w.getMessage());
		}catch(IOException t){
			 System.out.println(t.getMessage());
		}catch(ClassNotFoundException u){
			 System.out.println(u.getMessage());
		}
		return pending;
    }

	public void setPendingMessages(PendingXML pending){
    	ObjectOutputStream oos=null;
     	FileOutputStream fon=null;

    	try{
	 		fon = new FileOutputStream("pendingXML.file");
			oos = new ObjectOutputStream(fon);
			oos.writeObject(pending);
			oos.close();
		}catch(IOException b){
			System.out.println(b.getMessage());
		}

    }

    public void sendMessage(){                                                                  
       // create a simple message
     	Message message=null;
     	PendingXML arrayPending=null;
     	int i;
     	String content=getContent();
     	ObjectInputStream ois=null;
     	FileInputStream fin=null;
     	int queueSize;

		arrayPending=getPendingMessages();

		try{
	
			if(arrayPending.getSize()!=0){
				queueSize=arrayPending.getSize();
				System.out.println("There are "+queueSize+" messages pending...");

	      		for(i=0;i<queueSize;i++){
	      			message=topicSession.createMessage();
	      			message.setStringProperty("xml",arrayPending.getXML(0));                                                                                                                
	    			topicPublisher.publish(message);   
	    			arrayPending.remXML(0);                                                           
	      			System.out.println("XML Pending String sent to topic!");
	      		}
	      	}

      	}catch(JMSException e){
      		System.out.println("Server is down! Pending messages will be sent later...");
      		setPendingMessages(arrayPending);
      		System.exit(0);
   		}

   		try{
   			message=topicSession.createMessage();
	      	message.setStringProperty("xml",content);                                                                                                                
			topicPublisher.publish(message);                                                              
	  		System.out.println("XML String sent to topic!");
	                                                                          
	        setPendingMessages(arrayPending);

	        topicConn.close();

   		}catch(JMSException e){
   			System.out.println("Server is down! Your message will be pending...");
			arrayPending.setXMl(content);

			setPendingMessages(arrayPending);

			System.exit(0);
   		}

    }
    public Movies classProducer(String page){

    	int i,a;
 		org.jsoup.nodes.Document doc=null;
 		Movies moviesXML=null;
		Elements movies=null;
		Movies moviesClass=new Movies();
		List<Movie> listMovie;
		listMovie=moviesClass.getMovie();
 		Movie newMovie;
 		Genres generos;
 		List<String> listaGeneros;
 		Directors directors;
 		List<String> listaDirectors;
 		Actors actors;
 		List<String> listaActors;
		String year;

		try{
			doc= Jsoup.connect(page).get();
		}catch(IOException e){
			System.out.println("Failed attempt to connect to HTML server...");
			try{
				Thread.sleep(5000);
			}catch(InterruptedException s){
				System.out.println(s.getMessage());
			}
			return null;
		}
		movies = doc.select("div[itemtype=http://schema.org/Movie]");
		for(i=0;i<movies.size();i++){

			newMovie=new Movie();

			Elements tituloAux = movies.get(i).getElementsByTag("h4");

			newMovie.setTitle(tituloAux.text().substring(0,tituloAux.text().indexOf("(")));

			year=tituloAux.text().substring(tituloAux.text().indexOf("(")+1,tituloAux.text().length()-1);
			newMovie.setYear(Integer.parseInt(year));
				
			Elements genreAux = movies.get(i).getElementsByAttributeValue("itemprop","genre");
	
			generos=new Genres();
			listaGeneros=generos.getGenre();
			for(a=0;a<genreAux.size();a++){
				listaGeneros.add(genreAux.get(a).text());

			}
			newMovie.setGenres(generos);

			Elements rankAux = movies.get(i).getElementsByAttributeValue("class","rating_txt");

			Pattern pattern = Pattern.compile("[0-9]+");
			Matcher matcher = pattern.matcher(rankAux.text());
			if (matcher.find()) {
			   newMovie.setRank(Integer.parseInt(matcher.group(0)));
			}
	
			Elements directorAux = movies.get(i).getElementsByAttributeValue("itemprop","director");
			directors=new Directors();
			listaDirectors=directors.getDirector();
			for(a=0;a<directorAux.size();a++){

				listaDirectors.add(directorAux.get(a).text());

			}
			newMovie.setDirectors(directors);

			Elements actorsAux = movies.get(i).getElementsByAttributeValue("itemprop","actors");
			actors=new Actors();
			listaActors=actors.getActor();
			for(a=0;a<actorsAux.size();a++){
				listaActors.add(actorsAux.get(a).text());
			}
			newMovie.setActors(actors);


			listMovie.add(newMovie);
		}
		

		return moviesClass;
	}

	public void xmlProducer(Movies moviesClass) {
	  	try{
			File file = new File("XMLFile.xml");
			JAXBContext jaxbContext = JAXBContext.newInstance(Movies.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	 
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	 
			jaxbMarshaller.marshal(moviesClass, file);

		}catch(JAXBException q){
			System.out.println(q.getMessage());
		}

	}

	public void xmlValidation(){
		File schemaFile;
		SchemaFactory schemaFactory;
		Schema schema;
		StreamSource xmlFile=null;

	  	try {
	  		file= new File("XMLFile.xml");
	        schemaFile = new File("XSDFile.xsd");
	        xmlFile = new StreamSource(new File("XMLFile.xml"));
	        schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
	        schema = schemaFactory.newSchema(schemaFile);
	        Validator validator = schema.newValidator();
	    } catch (SAXException e) {
	        System.out.println(xmlFile.getSystemId() + " is NOT valid");
	        System.out.println("Reason: " + e.getLocalizedMessage());
		}
		
    }
    
}