import java.util.Scanner;
import java.util.ArrayList;
import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.IOException;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import javax.xml.transform.dom.DOMSource;
import org.w3c.dom.Document;
import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.FileInputStream;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.XMLConstants;
import org.xml.sax.SAXException;
import Movies.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import org.w3c.dom.ProcessingInstruction;

public class HTMLtoXML{

    Document docXML=null;

    public static void main(String[] args) throws IOException{
    	new HTMLtoXML();
    }

    public HTMLtoXML(){
    	try{
    		xmlProducer();

    	}catch(Exception e){
    		System.out.println(e.getMessage());
    	}
    }

    public void xmlProducer() throws IOException{
    	int i,a;
 		org.jsoup.nodes.Document doc = Jsoup.connect("http://www.imdb.com/movies-coming-soon/2013-12").get();
 		Movies moviesXML=null;
		Elements movies = doc.select("div[itemtype=http://schema.org/Movie]");
		File schemaFile;
		SchemaFactory schemaFactory;
		Schema schema;
		StreamSource xmlFile=null;
		String yearAux;
		String nome;
		try
		{
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			docXML = docBuilder.newDocument();
			ProcessingInstruction pi=docXML.createProcessingInstruction("xml-stylesheet", "type=\"text/xsl\" href=\"XSLFile.xsl\"");

			//root of xml
			org.w3c.dom.Element rootElement = docXML.createElement("Movies");
	 		docXML.appendChild(rootElement);
	 		docXML.insertBefore(pi, rootElement);

			for(i=0;i<movies.size();i++){
				 //staff elements
	  			org.w3c.dom.Element movie = docXML.createElement("Movie");
	 			rootElement.appendChild(movie);
				/*//Mostrar o titulo
				Elements titulo = movies.get(i).getElementsByTag("h4");
				System.out.println(titulo.text());*/
				//Criar o titulo
				org.w3c.dom.Element title = docXML.createElement("title");
				Elements tituloAux = movies.get(i).getElementsByTag("h4");
				nome=tituloAux.text().substring(0,tituloAux.text().indexOf("("));

				title.appendChild(docXML.createTextNode(nome));
				movie.appendChild(title);


				org.w3c.dom.Element year = docXML.createElement("year");
				yearAux=tituloAux.text().substring(tituloAux.text().indexOf("(")+1,tituloAux.text().length()-1);
				year.appendChild(docXML.createTextNode(yearAux));
				movie.appendChild(year);
				//Criar genero
				org.w3c.dom.Element genres = docXML.createElement("genres");
				movie.appendChild(genres);


				org.w3c.dom.Element genre;
				Elements genreAux = movies.get(i).getElementsByAttributeValue("itemprop","genre");
		
				
				for(a=0;a<genreAux.size();a++){
					genre = docXML.createElement("genre");
					genre.appendChild(docXML.createTextNode(genreAux.get(a).text()));
					genres.appendChild(genre);
				}
				/*//Mostrar o rank
				Elements rank=movies.get(i).getElementsByAttributeValue("class","rating_txt");
				System.out.println(rank.text());*/
				//Criar rank
				org.w3c.dom.Element rank = docXML.createElement("rank");
				Elements rankAux = movies.get(i).getElementsByAttributeValue("class","rating_txt");

				Pattern pattern = Pattern.compile("[0-9]+");
				Matcher matcher = pattern.matcher(rankAux.text());
				if (matcher.find()) {
				   rank.appendChild(docXML.createTextNode(matcher.group(0)));
				}
				movie.appendChild(rank);
				/*//Mostar o realizador
				Elements realizador=movies.get(i).getElementsByAttributeValue("itemprop","director");
				System.out.println(realizador.text());*/
				org.w3c.dom.Element directors = docXML.createElement("directors");
				movie.appendChild(directors);

				org.w3c.dom.Element director;
				Elements directorAux = movies.get(i).getElementsByAttributeValue("itemprop","director");
				for(a=0;a<directorAux.size();a++){
					director = docXML.createElement("director");
					director.appendChild(docXML.createTextNode(directorAux.get(a).text()));
					directors.appendChild(director);
				}
				/*//Mostrar os atores
				org.w3c.dom.Elements atores=movies.get(i).getElementsByAttributeValue("itemprop","actors");
				System.out.println(atores.text());*/
				org.w3c.dom.Element actors = docXML.createElement("actors");
				movie.appendChild(actors);

				org.w3c.dom.Element actor;
				Elements actorsAux = movies.get(i).getElementsByAttributeValue("itemprop","actors");
				for(a=0;a<actorsAux.size();a++){
					actor = docXML.createElement("actor");
					actor.appendChild(docXML.createTextNode(actorsAux.get(a).text()));
					actors.appendChild(actor);
				}
				
			}

			//write the content into xml file
		 	TransformerFactory transformerFactory = TransformerFactory.newInstance();
		 	Transformer transformer = transformerFactory.newTransformer();
		 	transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		  	DOMSource source = new DOMSource(docXML);

		  	StreamResult result =  new StreamResult(new File("XMLFile.xml"));
		  	transformer.transform(source, result);


		  	/*try{
			  	JAXBContext context = JAXBContext.newInstance(Movies.class);
	    		Unmarshaller unMarshaller = context.createUnmarshaller();
	    		moviesXML = (Movies) unMarshaller.unmarshal(new FileInputStream("XMLFile.xml"));
    		}catch(JAXBException e){
    			System.out.println("Erro no JAXB");
    		}*/

    		

	  	}catch(ParserConfigurationException pce){
			System.out.println(pce.getMessage());
		}catch(TransformerException tfe){
			System.out.println(tfe.getMessage());
		}
    }
    
}