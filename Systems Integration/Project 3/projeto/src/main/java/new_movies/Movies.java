package new_movies;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Movies implements Serializable{
	List<Movie> list;
	
	public Movies(){
		list=new ArrayList<Movie>();
	}
	
	public void setMovie(Movie movie){
		list.add(movie);
	}
	
	public List<Movie> getMovies(){
		return list;
	}
}
