package new_movies;

import java.io.Serializable;

public class Genre implements Serializable{
	String name;
	String titulo;
	
	public Genre(){
		
	}
	
	public Genre(String name,String titulo){
		this.titulo=titulo;
		this.name=name;
	}
	
	public String getName(){
		return name;
	}
	
	public String getTitulo(){
		return titulo;
	}
}
