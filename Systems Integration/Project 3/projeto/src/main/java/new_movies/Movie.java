package new_movies;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Movie implements Serializable{
	String title;
	int year;
	int rank;
	List<Genre> generos;
	List<String> generos1;
	
	public Movie(){
		
	}
	
	public Movie(String title,int year,int rank){
		this.title=title;
		this.year=year;
		this.rank=rank;
		generos=new ArrayList<Genre>();
		generos1=new ArrayList<String>();
	}
	
	public String getTitle(){
		return title;
	}
	
	public int getYear(){
		return year;
	}
	
	public int getRank(){
		return rank;
	}
	
	public void setGenre(Genre genre){
		generos.add(genre);
	}
	
	public void setGenre1(String genre){
		generos1.add(genre);
	}
	
	public List<String> getGenres1(){
		return generos1;
	}
	
	public List<Genre> getGenres(){
		return generos;
	}

}
