package service;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.annotation.Resource;
import javax.jws.WebService;
import util.Content;

@WebService(endpointInterface = "service.UsersInterface",serviceName = "UsersInterface")
public class Users implements UsersInterface{
	
	@Override
	public Content subscribe(String email,String type){
		Content content=new Content();
		System.out.println(email+" "+type);
		content.setEmail(email);
		content.setType(type);
		return content;
	}
	@Override
	public String unsubscribe(String email){
		return "Nao existe!";	
	}
	
}
