package service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;
import org.mule.util.CaseInsensitiveHashMap;

public class NonDigest extends AbstractMessageTransformer {
	
	Connection con;
	
	void connect(){
		String url = "jdbc:postgresql://localhost/IS2";
        String user = "josericardoramos";
        String password = "189300";
        
		try{
		
	        con = DriverManager.getConnection(url, user, password);
           // System.out.println(con==null);
     
	    }catch(SQLException e){
            System.out.println(e.getMessage());
        }
	}
	
	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding)
			throws TransformerException {
		
		HashMap<String,List<String>> returnType=new HashMap<String,List<String>>();
		List<String> mails=new ArrayList<String>();
		List<String> titles=new ArrayList<String>();
		
		CaseInsensitiveHashMap title=(CaseInsensitiveHashMap)message.getPayload();
		
		for(Object key:title.keySet())
			System.out.println("------------>"+key.toString());
		
		titles.add((String)title.get("title"));
		connect();
		
		String stm = "SELECT mail from users_table where type='non_digest'";
		PreparedStatement pst;

		try {
			
			pst = con.prepareStatement(stm);          
			ResultSet rs=pst.executeQuery();
	
	        while(rs.next()){
	        	mails.add(rs.getString(1));
	        }
	        
	        returnType.put("mails", mails);
	        returnType.put("title", titles);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		message.setPayload(returnType);
		return message;
	}

}
