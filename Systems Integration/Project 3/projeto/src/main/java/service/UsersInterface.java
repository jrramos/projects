package service;

import java.util.ArrayList;

import javax.jws.WebService;

import util.Content;

@WebService
public interface UsersInterface {
 
	public Content subscribe(String email,String type);
	public String unsubscribe(String email);
}
