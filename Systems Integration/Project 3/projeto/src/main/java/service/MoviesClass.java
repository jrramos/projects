package service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import Movies.*;

@WebService(endpointInterface = "service.MoviesInterface",serviceName = "MoviesInterface")
public class MoviesClass implements MoviesInterface{
	
	Connection con;
	
	void connect(){
		String url = "jdbc:postgresql://localhost/IS2";
        String user = "josericardoramos";
        String password = "189300";
        
		try{
		
	        con = DriverManager.getConnection(url, user, password);
           // System.out.println(con==null);
     
	    }catch(SQLException e){
            System.out.println(e.getMessage());
        }
	}
	
	@Override
	public new_movies.Movies addMovies(String xmlString){
		new_movies.Movies movies=new new_movies.Movies();
		Movies moviesXML=null;

		int i,a;
		new_movies.Movie newMovie;
		new_movies.Genre genre;
		
		connect();
		String stm;
		PreparedStatement pst;
		ResultSet rs;
		
		
		
		
		try{
			PrintStream out = new PrintStream(new FileOutputStream("XMLFile.xml"));
			out.print(xmlString);
			JAXBContext context = JAXBContext.newInstance(Movies.class);
			Unmarshaller unMarshaller = context.createUnmarshaller();
			moviesXML = (Movies) unMarshaller.unmarshal(new FileInputStream("XMLFile.xml"));
			
			for(i=0;i<moviesXML.getMovie().size();i++){
				System.out.println(moviesXML.getMovie().get(i).getTitle());
				stm = "select title from movies_table where title='"+moviesXML.getMovie().get(i).getTitle().substring(0, moviesXML.getMovie().get(i).getTitle().length()-1)+"'";
				
				try {
					pst = con.prepareStatement(stm);          
					rs=pst.executeQuery();
			
			        if(rs.next()){
			        	System.out.println("Entrou");
			        	continue;
			        }
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				newMovie=new new_movies.Movie(moviesXML.getMovie().get(i).getTitle(),moviesXML.getMovie().get(i).getYear(),moviesXML.getMovie().get(i).getRank());
				for(a=0;a<moviesXML.getMovie().get(i).getGenres().getGenre().size();a++){
					genre=new new_movies.Genre(moviesXML.getMovie().get(i).getGenres().getGenre().get(a),moviesXML.getMovie().get(i).getTitle());
					newMovie.setGenre1(moviesXML.getMovie().get(i).getGenres().getGenre().get(a));
					newMovie.setGenre(genre);
				}
				movies.setMovie(newMovie);
			}
			

		}catch(JAXBException b){
			System.out.println(b.getMessage());
		}catch(FileNotFoundException c){
			System.out.println(c.getMessage());
		}
		
		return movies;
	}

}
