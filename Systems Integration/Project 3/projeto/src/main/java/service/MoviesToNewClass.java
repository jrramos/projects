package service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mule.api.MuleMessage;
import org.mule.api.annotations.param.Payload;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

public class MoviesToNewClass extends AbstractMessageTransformer {
	Connection con;
	
	void connect(){
		String url = "jdbc:postgresql://localhost/IS2";
        String user = "josericardoramos";
        String password = "189300";
        
		try{
		
	        con = DriverManager.getConnection(url, user, password);
           // System.out.println(con==null);
     
	    }catch(SQLException e){
            System.out.println(e.getMessage());
        }
	}
	
	public void registNonDigest(String title){
		String stm = "Insert into non_digest(title,timestamps) values ('"+title+"',current_timestamp)";
		PreparedStatement pst;
		
		try {
			pst = con.prepareStatement(stm);
			pst.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		
		List<String> lista=new ArrayList<String>();
		
		connect();
		
		String stm = "SELECT mail from users_table where type='digest'";
		PreparedStatement pst;
		

		HashMap<String,Object> payload=(HashMap<String,Object>)message.getPayload();
		
		//System.out.println(payload.get("title"));
		registNonDigest(payload.get("title").toString());
		try {
			
			pst = con.prepareStatement(stm);          
			ResultSet rs=pst.executeQuery();
	
	        while(rs.next()){
	        	lista.add(rs.getString(1));
	        }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		payload.put("mails", lista);
		
		return payload;
	}
	
	

}
