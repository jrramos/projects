package service;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;

import util.Content;
import Movies.*;

@WebService
public interface MoviesInterface {
 
	public new_movies.Movies addMovies(String xmlString);
}