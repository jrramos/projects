package service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;


import javax.jws.WebService;


@WebService(endpointInterface = "service.StatisticsInterface",serviceName = "StatisticsInterface")
public class Statistics implements StatisticsInterface{
	Connection con;
	
	void connect(){
		String url = "jdbc:postgresql://localhost/IS2";
        String user = "josericardoramos";
        String password = "189300";
        
		try{
		
	        con = DriverManager.getConnection(url, user, password);
           // System.out.println(con==null);
     
	    }catch(SQLException e){
            System.out.println(e.getMessage());
        }
	}
	
	@Override
	public String getStatistics(){
		String result;
		int nMovies=0;
		int nDigest=0;
		int nNDigest=0;
		
		HashMap<String,Integer> generos=new HashMap<String,Integer>();
		connect();
		
		
		String stm = "SELECT digest,non_digest from statistics,movies_table";
		
		PreparedStatement pst;
		
		try {
			pst = con.prepareStatement(stm);        
			ResultSet rs=pst.executeQuery();
			
	        while(rs.next()){
	        	nDigest=rs.getInt(1);
	        	nNDigest=rs.getInt(2);
	        }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		stm = "select count(*) from movies_table";
		
        
		try {
			pst = con.prepareStatement(stm);        
			ResultSet rs=pst.executeQuery();
			
	        while(rs.next()){
	        	nMovies=rs.getInt(1);
	        }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
		stm = "select distinct genre,count(*) from movie_has_genres,genres_table where genres_table.genres_id=movie_has_genres.genres_id group by genre";
		
        
		try {
			pst = con.prepareStatement(stm);        
			ResultSet rs=pst.executeQuery();
			
	        while(rs.next()){
	        		generos.put(rs.getString(1), rs.getInt(2));
	        }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
        result= "Numero de filmes ="+nMovies+"\nN de Filmes de:\n";
        
        for(String name:generos.keySet()){
        	result+=name+": "+generos.get(name)+"\n\n";
        }
        
        result+="Numero de mails digest: "+nDigest;
        result+="\nNumero de mails digest: "+nNDigest;
        
		return result;
	}

}
