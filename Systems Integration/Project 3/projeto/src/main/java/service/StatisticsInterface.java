package service;


import javax.jws.WebService;


@WebService
public interface StatisticsInterface {
 
	public String getStatistics();
}