<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%
com.common.User user =(com.common.User) session.getAttribute("sessao");
if (user != null && user.checkSession()){ 
%>	<jsp:forward page="home_page.jsp"></jsp:forward><%
}
%>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8"> 
    <title>.:Movie Website:. Home Page</title>
    <link rel="stylesheet" href="css/bootstrap.css"  type="text/css"/>
    <style type="text/css">
      .divElement{
        position: absolute;
        left: 50%;
        margin-left: -50px;
      }​
    </style>
  </head>

<body>

  <div class="hero-unit ">
      <h1 style="text-align:center">.:Movie Website:.</h1>
    </div>
    <div class="navbar navbar-inverse">
      <div class="navbar-inner">
        <ul class="nav">
          <li ><a href="index.jsp?pagina=login">Login</a></li>
          <li class="divider-vertical"></li>
          <li><a href="index.jsp?pagina=registo">Registo</a></li>
          <li class="divider-vertical"></li>
        </ul>
      </div>
    </div>


  <div id="apDiv1">
  <% if(request.getParameter("resposta")!=null){
       out.println(request.getParameter("resposta"));
    }
    
    if(request.getParameter("pagina")!=null){
      if(request.getParameter("pagina").compareTo("registo")==0){
  %>
<jsp:include page="registo.jsp"></jsp:include>
  <%
  
    }else{
  %>
      <jsp:include page="login.jsp"></jsp:include>
  <%
    }
  }else{
  %>
     <jsp:include page="login.jsp"></jsp:include>
  <%
  }
  %>
</div>
</body>
</html>