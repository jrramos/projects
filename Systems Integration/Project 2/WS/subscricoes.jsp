<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@page import="com.common.User,java.util.List,new_movies.NewGenres"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8"> 
    <title>.:Movie Website:. Subscricoes</title>
    <link rel="stylesheet" href="css/bootstrap.css"  type="text/css"/>
    <script type="text/javascript">
		var play = new Array();
	
	    function playButtonPress(i) {
		    if (document.getElementById('subscribed'+i).innerHTML=='Subscrito') {
			    document.getElementById('subscribed'+i).className='btn btn-danger';
			    document.getElementById('subscribed'+i).innerHTML='Nao Subscrito';
			    window.location.href="subscricoes.jsp?u="+i;
			} else {
				document.getElementById('subscribed'+i).className='btn btn-success';
			    document.getElementById('subscribed'+i).innerHTML='Nao Subscrito';
			    window.location.href="subscricoes.jsp?s="+i;
			}
	 	}


	    document.location.href = header.jsp;
	</script>
	<style type="text/css">
      .centro{
        position: absolute;
        left: 50%;
        margin-left: -50px;
      }​
    </style>
  </head>

  <body>
  	<jsp:include page="header.jsp"></jsp:include>

		<%
	
		User user = (User) session.getAttribute("sessao");
		List<NewGenres> categorias = user.getCategorias();
		List<NewGenres> listaCheck=user.getCategoriasCheck(user.getId());

		
			if(request.getParameter("s")!=null){
				user.subscribe(user.getId(), categorias.get(Integer.parseInt(request.getParameter("s"))));
				listaCheck=user.getCategoriasCheck(user.getId());
			 }if(request.getParameter("u")!=null){
				 user.unsubscribe(user.getId(), Integer.parseInt(request.getParameter("u")));
				 listaCheck=user.getCategoriasCheck(user.getId());
			 }
		
			int a=0,size;
			if(listaCheck!=null)
				size=listaCheck.size();
			else
				size=0;
			
			%>
			<table class="table table-striped">
		  	<tbody>
		  	<%
			for(int i=0;i<categorias.size();i++){
			%>
			<tr>
			<td>
			<%= categorias.get(i).getGenre()%></td>
			<td>
			<%
			if(size!=a && listaCheck.get(a).getId()==i+1){
				a++;
			%>	
		  		<button id="subscribed<%=i%>" class="btn btn-success" onclick='playButtonPress(<%=i%>)'>Subscrito</button> </td></tr>
			<%
			}else{
			%>
				<button id="subscribed<%=i%>" class="btn btn-danger" onclick='playButtonPress(<%=i%>)'>Nao Subscrito</button> </td></tr>
			<%
			}
			}

		%>
		</table>
		</br></br></br>
  </body>

</html>