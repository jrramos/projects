<jsp:include page="pesquisas.jsp"></jsp:include>
<%@page import="com.common.User,new_movies.NewMovie,java.util.List,new_movies.NewGenres"%>
<%
	User user = (User) session.getAttribute("sessao");
	List<NewMovie> listaFilmes= user.getAlfabeticamente();
	
	List<NewGenres> categorias = user.getCategorias(); 

	%>
	 <div class="span2">
	    <h4>Ordenamento</h4><br></br>
				<ul class="nav nav-pills nav-stacked">
				<li class="active"><a href="alfabeto.jsp">Alfabeto</a></li>
				<li ><a href="metascore.jsp">Metascore</a></li>
				<li class="dropdown-submenu">
					<a tabindex="-1" href="#">Categorias</a>
				 	<ul class="dropdown-menu">
	<% 			 		for(int i=0;i<categorias.size();i++){
							%><li><a href="categorias.jsp?c=<%=categorias.get(i).getGenre()%>"><%=categorias.get(i).getGenre()%></a></li>
	<%
						}
		
	%>
				 	</ul>
				</li>
			</ul>
		</div>
	<div class="span10">
	<table class="table table-striped">
  	<thead>
  		<tr>
  		<td><b>Titulo</b></td>
  		<td><b>Rank</b></td>
  		<td><b>Diretores</b></td>
  		<td><b>Actores</b></td>
  		</tr>
  	</thead>
  	<tbody>
<% 

	for(int i=0;i<listaFilmes.size();i++){
%>
		<tr>
			<td><%=listaFilmes.get(i).getTitle()%></td>
			<td><%=listaFilmes.get(i).getRank()%></td>
			<td>
<%
			for(int a=0;a<listaFilmes.get(i).getActors().size();a++){
				out.println(listaFilmes.get(i).getActors().get(a).getActor());
			}
%>
			</td>
			<td>
<%
			for(int a=0;a<listaFilmes.get(i).getDirectors().size();a++){
				out.println(listaFilmes.get(i).getDirectors().get(a).getDirector());
			}
%>
			</td>
		</tr>
<%
	}
%>
	</tbody>
	</table>
	</div>