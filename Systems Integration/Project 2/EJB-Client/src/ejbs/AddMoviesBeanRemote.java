package ejbs;
import java.util.List;

import javax.ejb.Remote;

import new_movies.*;

@Remote
public interface AddMoviesBeanRemote {
	public void add(List<NewMovie> movies);
}