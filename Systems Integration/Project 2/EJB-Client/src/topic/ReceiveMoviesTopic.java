package topic;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.jms.JMSException;
import javax.jms.Session;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.jms.*;

import new_movies.NewActors;
import new_movies.NewDirectors;
import new_movies.NewGenres;
import new_movies.NewMovie;
import ejbs.AddMoviesBeanRemote;
import Movies.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class ReceiveMoviesTopic{
	TopicSubscriber topicSubscriber;
	InitialContext ctx;
	Topic topic;
	TopicConnectionFactory connFactory;
	TopicConnection topicConn;
	ConnectionState connectionState;
	KeyboardReader kBReader;
	
	public static void main(String[] args)  {
		new ReceiveMoviesTopic();
	}
	
    public ReceiveMoviesTopic() {
    	connectionState=new ConnectionState();
		while(!initSubscribe());
		kBReader=new KeyboardReader(topicConn,connectionState);
		receiveFromTopic();
    }

    public void persists(List<NewMovie> movies) {
    	try{
	    	InitialContext ctx = new InitialContext();
	    	AddMoviesBeanRemote addbean = (AddMoviesBeanRemote) ctx.lookup("jboss/exported/EJB/AddMoviesBean!ejbs.AddMoviesBeanRemote");
	    	addbean.add(movies);
    	}catch(NamingException e){
    		System.out.print(e.getMessage());
    	}
    }
    
    public boolean initSubscribe() {

		try{
		 // get the initial context
		ctx = new InitialContext();
																																				
		 // lookup the topic object
		topic = (Topic) ctx.lookup("jms/topic/project");
																																				
		 // lookup the topic connection factory
		connFactory = (TopicConnectionFactory) ctx.lookup("jms/RemoteConnectionFactory");
																																				
		 // create a topic connection
		topicConn = connFactory.createTopicConnection("admin1", "admin");

		topicConn.setClientID("admin1");
																																				
		 // create a topic session
		TopicSession topicSession = topicConn.createTopicSession(false,Session.AUTO_ACKNOWLEDGE);
																																				
		 // create a topic subscriber
		topicSubscriber =topicSession.createDurableSubscriber(topic, "mySub");
		

		topicConn.start();

		return true;

		}catch(JMSException e ){
			System.out.println("->Topic is down! We will retry the connection in 5 seconds...");
			try{
				Thread.sleep(5000);
			}catch(InterruptedException s){
				System.out.println(s.getMessage());
			}
			return false;
		}catch(NamingException e){
			System.out.println("!Topic is down! We will retry the connection in 5 seconds...");
			try{
				Thread.sleep(5000);
			}catch(InterruptedException s){
				System.out.println(s.getMessage());
			}
			return false;
		}
	}

	public boolean retryConnection() {

		try{
			topicConn.close();
		 // create a topic connection
			topicConn = connFactory.createTopicConnection("admin1", "admin");

			topicConn.setClientID("admin1");
																																					
			 // create a topic session
			TopicSession topicSession = topicConn.createTopicSession(false,Session.AUTO_ACKNOWLEDGE);
																																					
			 // create a topic subscriber
			topicSubscriber=topicSession.createDurableSubscriber(topic, "mySub");

			topicConn.start();

			kBReader.topicConn=topicConn;
			
			System.out.println("Connection to the topic retablished!");
			return true;
		}catch(Exception e){

			System.out.println("Topic is down! We will retry the connection in 5 seconds...");
			try{
				Thread.sleep(5000);
			}catch(InterruptedException s){
				System.out.println(s.getMessage());
			}

			return false;
		}
	}

	 public void receiveFromTopic(){
		 
		 Movies moviesXML=null;
		 List<NewMovie> newMovies;
			while(true){
				try{
					Message message=topicSubscriber.receive();
					String docXML=message.getStringProperty("xml");
					docXML=docXML.replace('\u0000', ' ');

					moviesXML=getClassesFromXML(docXML);
					newMovies=convertClasses(moviesXML);
					System.out.println("Message received from topic!\n");
					
					System.out.println("->"+newMovies.get(0).getTitle());
					persists(newMovies);
				}catch(JMSException e){
					System.out.println(e.getMessage());
	
					while(connectionState.state==1 && !retryConnection());
					
				}catch(NullPointerException e){
					System.out.println(e.getMessage());
					while(connectionState.state==1 && !retryConnection());
				}
			}

	}
	 
	public List<NewMovie> convertClasses(Movies movies){
		List<NewMovie> novaLista = new ArrayList<NewMovie>();
		int i,a;
		List<Movie> lista=movies.getMovie();
		NewMovie newMovie;
		Movie movie;
		List<String> actors;
		NewActors newActor;
		NewDirectors newDirector;
		NewGenres newGenre;
		List<NewActors> nListaActores;
		List<NewDirectors> nListaDirectors;
		List<NewGenres> nListaGenres;
		
		for(i=0;i<lista.size();i++){
			movie=lista.get(i);
			newMovie=new NewMovie();
			nListaActores=newMovie.getActors();
			nListaDirectors=newMovie.getDirectors();
			nListaGenres=newMovie.getGenres();
			newMovie.setTitle(movie.getTitle());
			newMovie.setRank(movie.getRank());
			newMovie.setYear(movie.getYear());
			
			//actores
			actors=movie.getActors().getActor();
			for(a=0;a<actors.size();a++){
				newActor=new NewActors();
				newActor.setActor(actors.get(a));
				nListaActores.add(newActor);
			}
			//directores
			actors=movie.getDirectors().getDirector();
			for(a=0;a<actors.size();a++){
				newDirector=new NewDirectors();
				newDirector.setDirector(actors.get(a));
				nListaDirectors.add(newDirector);
			}
			//Generos
			actors=movie.getGenres().getGenre();
			for(a=0;a<actors.size();a++){
				newGenre=new NewGenres();
				newGenre.setGenre(actors.get(a));
				nListaGenres.add(newGenre);
			}
			
			novaLista.add(newMovie);
		}
		
		return novaLista; 
	}
	
	public Movies getClassesFromXML(String docXML){
			PrintStream out = null;
			Movies moviesXML=null;
			 try {
						out = new PrintStream(new FileOutputStream("XMLFile.xml"));
						out.print(docXML);
					}catch(Exception q){
						System.out.println(q.getMessage());
					}
					finally {
						if (out != null)
							out.close();
					}

					try{
						JAXBContext context = JAXBContext.newInstance(Movies.class);
						Unmarshaller unMarshaller = context.createUnmarshaller();
						moviesXML = (Movies) unMarshaller.unmarshal(new FileInputStream("XMLFile.xml"));
					}catch(JAXBException b){
						System.out.println(b.getMessage());
					}catch(FileNotFoundException c){
						System.out.println(c.getMessage());
					}
			return moviesXML;
	}
    
}

class KeyboardReader extends Thread{
	TopicConnection topicConn;
	ConnectionState connectionState;

	KeyboardReader(TopicConnection topicConn,ConnectionState connectionState){
		this.topicConn=topicConn;
		this.connectionState=connectionState;
		this.start();
	}

	public void run(){
		InputStreamReader input= new InputStreamReader(System.in);;
		BufferedReader reader= new BufferedReader(input);
		System.out.println("\nYou are connected to the Topic");
		try{
			while(true){
				if(((String)(reader.readLine())).compareTo("exit")==0){
					connectionState.state=0;
					this.topicConn.close();
					System.exit(0);
				}
			}
		}catch(IOException e){
			System.out.println(e.getMessage());
		}catch(JMSException e){
			System.out.println(e.getMessage());
		}
	}
}

class ConnectionState{
	char state;

	ConnectionState(){
		state=1;
	}
}
