package com.common;

import java.sql.Timestamp;

public class Online {
	Long id;
	Timestamp timestamp;
	
	public Online(Long id,Timestamp timestamp){
		this.id=id;
		this.timestamp=timestamp;
	
	}
	
	public Long getId(){
		return id;
	}
	
	public Timestamp getTimestamp(){
		return timestamp;
	}
	
	public void setTimestamp(Timestamp timestamp){
		this.timestamp=timestamp;
	}
}
