package com.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import new_movies.*;

@Entity
@Table(name="USERS_TABLE")
public class Registo implements Serializable{
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
	private Long id;
	private String name;
	@Column(unique=true)
	private String username;
	private String password;
	@Column(unique=true)
	private String email;
	@ManyToMany(fetch= FetchType.EAGER,cascade = {CascadeType.ALL})
    @JoinTable(name="user_has_genres", joinColumns={@JoinColumn(name="user_id")}, inverseJoinColumns={@JoinColumn(name="genres_id")})
	@OrderBy("id ASC")
    private List<NewGenres> genres;

	public Registo(String name,String password,String username,String email){
		this.username=username;
		this.password=password;
		this.email=email;
		this.name=name;
	}
	
	Registo(){
		genres=new ArrayList<NewGenres>();
	}
	
	public Long getId(){
		return id;
	}
	
	public Registo(String username,String password){
		this.username=username;
		this.password=password;
	}

	public String getUsername(){
		return username;
	}

	public String getPassword(){
		return password;
	}

	public void setPassword(String password){
		this.password=password;
	}
	
	public String getEmail(){
		return email;
	}
	
	public void setEmail(String email){
		this.email=email;
	}
	
	public List<NewGenres> getGenres(){
		return genres;
	}
	
	public void setGenre(NewGenres genre){
		genres.add(genre);
	}
	
	public void remGenre(int pos){
		int i;
		
		for(i=0;i<genres.size();i++){
			if(genres.get(i).getId()==pos+1)
				genres.remove(i);
		}
		
	}
	
}
