package ejbs;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import com.common.Online;

import java.sql.Timestamp;
import java.util.Date;

@Startup
@Singleton
public class OnlineUsersBean {
  private ArrayList<Online> listaOnline;
  
  @PostConstruct
  public void init(){
    listaOnline=new ArrayList<Online>();
  }
  
  public void insertUser(Long id){
	 Online online;
	 Date date= new java.util.Date();
	  
	 if((online=verify(id))==null){
		 listaOnline.add(new Online(id,new Timestamp(date.getTime())));
	 }else{
		 online.setTimestamp(new Timestamp(date.getTime()));
	 }
	 
  }
  
  @Schedule(second="*/5", minute="*", hour="*", persistent=false)
  public void checkList(){
	  Date date= new java.util.Date();
	  
	  for(int i=0;i<listaOnline.size();i++){
		  System.out.println("USER ONLINE-> "+((new Timestamp(date.getTime()).getTime()))+ " "+listaOnline.get(i).getTimestamp().getTime());
		  if(((new Timestamp(date.getTime()).getTime())-listaOnline.get(i).getTimestamp().getTime())>5*60*1000)
		  	remUser(listaOnline.get(i));
	  }
  }
  
  public void remUser(Online online){
	  System.out.println("ELIMINADO");
	  listaOnline.remove(online);
  }
  
  
  public void updateTimestamp(Long id){
	  Online online=verify(id);
	  Date date= new java.util.Date();
	  
	  online.setTimestamp(new Timestamp(date.getTime()));
  }
  
  public Online verify(Long id){
	  
	  for(int i=0;i<listaOnline.size();i++){
		  if(listaOnline.get(i).getId()==id){
			  return listaOnline.get(i);
		  }
	  }
	  return null;
	  
  }
  
}