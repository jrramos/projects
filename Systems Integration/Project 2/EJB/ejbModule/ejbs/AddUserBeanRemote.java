package ejbs;

import java.util.List;

import javax.ejb.Remote;

import new_movies.NewGenres;
import new_movies.NewMovie;

import com.common.Registo;

@Remote
public interface AddUserBeanRemote {
	public boolean add(Registo user);
	public boolean remove(Registo user);
	public Long find(Registo r);
	public List<NewGenres> getCategorias(Long reg); 
	public List<NewGenres> getCategoriasCheck(Long id);
	public boolean subscribe(Long reg,NewGenres id);
	public boolean unSubscribe(Long reg,int id);
	public List<NewMovie> getAlfabeticamente(Long reg);
	public List<NewMovie> getGenre(Long reg,String genero);
	public List<NewMovie> getMetascore(Long reg,int min,int max);
	public boolean checkSession(Long id);
}