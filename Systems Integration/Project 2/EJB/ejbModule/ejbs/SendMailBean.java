package ejbs;

import java.util.List;
import java.util.Properties;

import javax.ejb.Stateless;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import new_movies.*;

@Stateless
public class SendMailBean{
	
	final String username = "ricardocmramos@gmail.com";
    final String password = "18A5WKB0a";
    Session session;
    Properties props;
    
    @PersistenceContext(name = "TestPersistence")
    private EntityManager em;
	
	public SendMailBean(){
		props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        session = Session.getInstance(props,
          new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
          });
        
	}
	
	 public void sendMail(NewMovie m){
	    	List<String> listaEmails=null;
	    	int i;
	    	String bodyMail = "Caro cliente,\n\n Chegou o novo filme "+m.getTitle()+" que esta inserido nas categorias de ";
	    	int generosSize=0;
	    	int sizeMails=0;
	    	
	    	if(m.getGenres()!=null){
	    		generosSize= m.getGenres().size();
	    		Query query=(Query) em.createQuery("Select distinct r.email from Registo r JOIN r.genres g WHERE g.genre IN (SELECT g.genre FROM NewMovie m JOIN m.genres g WHERE m.title=:titulo)");
	        	query.setParameter("titulo",m.getTitle());
	        	listaEmails=query.getResultList();
	    	}else
	    		return;
	    	
	    	
	    	for(i=0;i<generosSize;i++){
	    		bodyMail+=m.getGenres().get(i).getGenre()+" ";
	    	}
	    	
	    	bodyMail+="\n\nBons filmes!";
	    	

	    	
	   
	    	if(listaEmails!=null && listaEmails.size()!=0){
	    		sizeMails=listaEmails.size();
	    		System.out.println("Mails a enviar : "+sizeMails);
	    	}else
	    		return;
	   
	    	InternetAddress[] lista=new InternetAddress[sizeMails];
	    	
	    	try {
		    	for(i=0;i<sizeMails;i++)
						lista[i]=new InternetAddress(listaEmails.get(i));
		    	
	    	} catch (AddressException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
	    	
		    try {
		        javax.mail.Message message = new MimeMessage(session);
		        message.setFrom(new InternetAddress("ricardocmramos@gmail.com"));
		        message.setRecipients(javax.mail.Message.RecipientType.TO,lista);
		        message.setSubject("Movies Website");
		        message.setText(bodyMail);
		
		        Transport.send(message);
		
		
		    } catch (MessagingException e) {
		        throw new RuntimeException(e);
		    }
	    }
}
