package ejbs;


import java.security.MessageDigest;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.validation.ConstraintViolationException;

import new_movies.NewGenres;
import new_movies.NewMovie;

import com.common.Online;
import com.common.Registo;



/**
 * Session Bean implementation class AddBean
 */
@Stateless
public class AddUserBean implements AddUserBeanRemote{
	
	
	@PersistenceContext(name = "TestPersistence")
	private EntityManager em;
	@EJB
	OnlineUsersBean onlineUsers;
	
    public AddUserBean() {
    	
    }
    
    public boolean add(Registo user) {
    	
    	//if(!findUser(user.getUsername())){
    	if(!findUser(user.getUsername(),user.getEmail())){
	    	user.setPassword(convertHash(user.getPassword()));
	    	em.persist(user);
	    	return true;
    	}
    		System.out.println("Ja existe este user!");
    		return false;
    		
    }
    
    
    
    
    
    public boolean findUser(String username,String email){
    	
    	String query="SELECT s FROM Registo s WHERE s.username=:username OR s.email=:email";
	   	Query query1= em.createQuery(query,Registo.class);
	    query1.setParameter("username",username);
	    query1.setParameter("email",email);
	    List<Registo> lista=query1.getResultList();
	   	if(lista.size()>0){
	   		return true;
	   	}else
	   		return false;
        
    }
    
	 public boolean remove(Registo user) {
	    
	    	em.remove(user);
	
	    	return true;
	    }
	 
	 
	 public Long find(Registo r){
		 
		 String query="SELECT s FROM Registo AS s WHERE s.username =:username AND s.password =:password";
		 Query query1=(Query) em.createQuery(query);
		 query1.setParameter("username",r.getUsername());
		 query1.setParameter("password",convertHash(r.getPassword()));
		 List<Registo> res=query1.getResultList();
		 
		 
		 if(res.size()==1){
			 onlineUsers.insertUser(res.get(0).getId());
			 return res.get(0).getId();
		 }else
			 return null;
	 }
	 
	 public boolean checkSession(Long id){
		 if(onlineUsers.verify(id)==null)
				return false;
		 
		 return true;
	 }
	 
	 public List<NewGenres> getCategorias(Long reg){
		 
		 if(onlineUsers.verify(reg)==null)
				return null;
		 
		 String query="SELECT s FROM NewGenres AS s";
		 Query query1=(Query) em.createQuery(query);
	
		 List<NewGenres> res=query1.getResultList();
		 
		 onlineUsers.updateTimestamp(reg);
		 
		 return res;
	 }
	 
	public List<NewGenres> getCategoriasCheck(Long id){
		
		 if(onlineUsers.verify(id)==null)
				return null;
		 
		Registo user=em.find(Registo.class, id);
		
		onlineUsers.updateTimestamp(id);
		
		return user.getGenres();
	 }
	
	
	public boolean subscribe(Long reg,NewGenres genre){
		List<NewGenres> listaGeneros;
		Online online;
		
		if(onlineUsers.verify(reg)==null)
			return false;
		
		String query="SELECT s FROM Registo AS s WHERE s.id=:id";
		Query query1=(Query) em.createQuery(query);
		query1.setParameter("id",reg);
		Registo user =(Registo) query1.getResultList().get(0);
		
		user.setGenre(genre);
		em.merge(user);
		onlineUsers.updateTimestamp(reg);
		
		return true;
	}
	
	public boolean unSubscribe(Long reg,int i){
		List<NewGenres> listaGeneros;
		
		if(onlineUsers.verify(reg)==null)
			return false;
		
		String query="SELECT s FROM Registo AS s WHERE s.id=:id";
		Query query1=(Query) em.createQuery(query);
		query1.setParameter("id",reg);
		Registo user =(Registo) query1.getResultList().get(0);
		
		user.remGenre(i);
		em.merge(user);
		onlineUsers.updateTimestamp(reg);
		
		return true;
	}
	
	public List<NewMovie> getAlfabeticamente(Long reg){
		
		if(onlineUsers.verify(reg)==null)
			return null;
		
		List<NewMovie> movies=em.createQuery("Select a from NewMovie a ORDER BY a.title",NewMovie.class).getResultList();
		
		onlineUsers.updateTimestamp(reg);
	
		return movies;
	}
	
	public List<NewMovie> getMetascore(Long reg,int min,int max){
		
		if(onlineUsers.verify(reg)==null)
			return null;
		
		List<NewMovie> movies;
		Query query=em.createQuery("Select a from NewMovie a WHERE a.rank>=:min AND a.rank<=:max ORDER BY a.rank",NewMovie.class);
		query.setParameter("min",min);
		query.setParameter("max",max);
		movies=query.getResultList();
		
		onlineUsers.updateTimestamp(reg);
		
		return movies;
	}
	
	public List<NewMovie> getGenre(Long reg,String genero){
		if(onlineUsers.verify(reg)==null)
			return null;
		
		List<NewMovie> movies;
		Query query=em.createQuery("Select m from NewMovie m JOIN m.genres g WHERE g.genre=:genero",NewMovie.class);
		query.setParameter("genero",genero);
		movies=query.getResultList();
	
		onlineUsers.updateTimestamp(reg);
			
		return movies;
	}
	
	public String convertHash(String password){
	    String password1=null;
	    try{
	       
	        MessageDigest md = MessageDigest.getInstance("MD5");
	        md.update(password.getBytes());
	        byte[] hashMd5 = md.digest();
	        password1=stringHexa(hashMd5);
	    }catch(Exception SecurityException){
	        System.out.println("Fail no hash");
	        System.exit(-1);
	    }
	    return password1;
	}
	
	private static String stringHexa(byte[] bytes) {
	    StringBuilder s = new StringBuilder();
	    for (int i = 0; i < bytes.length; i++) {
	        int parteAlta = ((bytes[i] >> 4) & 0xf) << 4;
	        int parteBaixa = bytes[i] & 0xf;
	        if (parteAlta == 0) s.append('0');
	        s.append(Integer.toHexString(parteAlta | parteBaixa));
	    }
	    return s.toString();
	 }
    
}