package ejbs;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.mail.*;
import javax.mail.internet.*;

import new_movies.NewActors;
import new_movies.NewDirectors;
import new_movies.NewGenres;
import new_movies.NewMovie;

import javax.mail.Message.RecipientType;

import Movies.*;

/**
 * Message-Driven Bean implementation class for: MyBean
 *
 */
@MessageDriven(
  activationConfig = {
   @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic"),
   @ActivationConfigProperty(propertyName = "destination", propertyValue = "topic/project")
   },
  mappedName = "topic/project")
public class MessageMovieBean implements MessageListener {

    /**
     * Default constructor. 
     */
	
	@PersistenceContext(name = "TestPersistence")
	private EntityManager em;
	private List<NewGenres> listaGeneros;
	private List<NewActors> listaActors;
	private List<NewDirectors> listaDirectors;
	@EJB
	private SendMailBean sendMail;
	
    public MessageMovieBean() {
    	
    }
    
    public void getCategorias(){
    	String query="SELECT s FROM NewGenres s";
	   	Query query1=(Query) em.createQuery(query,NewGenres.class);
	   	
	   	listaGeneros=query1.getResultList();
	   	if(listaGeneros==null)
	   		listaGeneros=new ArrayList<NewGenres>();
    }
    
    public void getActors(){
    	String query="SELECT s FROM NewActors s";
	   	Query query1=(Query) em.createQuery(query,NewActors.class);
	   	
	   	listaActors=query1.getResultList();
	   	if(listaActors==null)
	   		listaActors=new ArrayList<NewActors>();
    }
    
    public void getDirectors(){
    	String query="SELECT s FROM NewDirectors s";
	   	Query query1=(Query) em.createQuery(query,NewDirectors.class);
	   	
	   	listaDirectors=query1.getResultList();
		if(listaDirectors==null)
			listaDirectors=new ArrayList<NewDirectors>();
    }
 
 /**
     * @see MessageListener#onMessage(Message)
     */
    public void onMessage(Message message) {
    	String xml;
    	NewMovie m=null;
    	int i;
    
		try {
			getCategorias();
			getActors();
			getDirectors();
			xml = message.getStringProperty("xml");

	    	Movies movies=getClassesFromXML(xml);
	    	System.out.println(movies.getMovie().get(0).getTitle());
	    	List<NewMovie> newMovies=convertClasses(movies);
	    	
	    	for(i=0;i<newMovies.size();i++){
	    		m=newMovies.get(i);
    			if(!findTitle(m.getTitle())){
	    			em.persist(m);
	    			sendMail.sendMail(m);
    			}
	 
	    	}
	    	
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    
    
    public Movies getClassesFromXML(String docXML){
		Movies moviesXML=null;
	
		try{
			docXML=docXML.replace('\u0000', ' ');
			JAXBContext context = JAXBContext.newInstance(Movies.class);
			Unmarshaller unMarshaller = context.createUnmarshaller();
			StringReader reader = new StringReader(docXML);
			moviesXML = (Movies) unMarshaller.unmarshal(reader);
	
		}catch(JAXBException b){
			//System.out.print(docXML);
			System.out.println("Deu mal! "+b.getMessage());
			//b.printStackTrace();
		}
		return moviesXML;
    }
    
    public List<NewMovie> convertClasses(Movies movies){
		List<NewMovie> novaLista = new ArrayList<NewMovie>();
		int i,a;
		List<Movie> lista=movies.getMovie();
		NewMovie newMovie;
		Movie movie;
		List<String> actors;
		NewActors newActor;
		NewDirectors newDirector;
		NewGenres newGenre;
		List<NewActors> nListaActores;
		List<NewDirectors> nListaDirectors;
		List<NewGenres> nListaGenres;
		
		for(i=0;i<lista.size();i++){
			movie=lista.get(i);
			newMovie=new NewMovie();
			nListaActores=newMovie.getActors();
			nListaDirectors=newMovie.getDirectors();
			nListaGenres=newMovie.getGenres();
			newMovie.setTitle(movie.getTitle());
			newMovie.setRank(movie.getRank());
			newMovie.setYear(movie.getYear());
			
			//actores
			actors=movie.getActors().getActor();
			if(actors!=null){
				for(a=0;a<actors.size();a++){
					if((newActor=findActor(actors.get(a)))==null){
						newActor=new NewActors();
						newActor.setActor(actors.get(a));
						listaActors.add(newActor);
					}
					nListaActores.add(newActor);
				}
			}
			//directores
			actors=movie.getDirectors().getDirector();
			if(actors!=null){
				for(a=0;a<actors.size();a++){
					if((newDirector=findDirector(actors.get(a)))==null){
						newDirector=new NewDirectors();
						newDirector.setDirector(actors.get(a));
						listaDirectors.add(newDirector);
					}
					nListaDirectors.add(newDirector);
				}
			}
			//Generos
			actors=movie.getGenres().getGenre();
			if(actors!=null){
				for(a=0;a<actors.size();a++){
					if((newGenre=findGenre(actors.get(a)))==null){
						newGenre=new NewGenres();
						newGenre.setGenre(actors.get(a));
						listaGeneros.add(newGenre);
					}
					nListaGenres.add(newGenre);
				}
			}
			
			novaLista.add(newMovie);
		}
		
		return novaLista; 
	}
    
    
    public NewGenres findGenre(String nome){
    	boolean resp;
    	int i;
    	
    	for(i=0;i<listaGeneros.size();i++){
    		if(nome.compareTo(listaGeneros.get(i).getGenre())==0){
    			return listaGeneros.get(i);
    		}
    	}
    	return null;
    }
    
    public NewActors findActor(String nome){
    	boolean resp;
    	int i;
    	
    	for(i=0;i<listaActors.size();i++){
    		if(nome.compareTo(listaActors.get(i).getActor())==0){
    			return listaActors.get(i);
    		}
    	}
    	return null;
    }
    
    public NewDirectors findDirector(String nome){
    	boolean resp;
    	int i;
    	
    	for(i=0;i<listaDirectors.size();i++){
    		if(nome.compareTo(listaDirectors.get(i).getDirector())==0){
    			return listaDirectors.get(i);
    		}
    	}
    	return null;
    }
    
    public boolean findTitle(String title){
    	String query="SELECT s FROM NewMovie s WHERE s.title=:title";
	   	Query query1=(Query) em.createQuery(query,NewMovie.class);
	    query1.setParameter("title",title);
	    
	   	if(query1.getResultList().size()>0)
	   		return true;
	   	else
	   		return false;
    }

}