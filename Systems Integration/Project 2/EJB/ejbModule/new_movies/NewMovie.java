package new_movies;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.persistence.OrderColumn;

import org.hibernate.annotations.IndexColumn;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;


@Entity
@Table(name="MOVIES_TABLE")
public class NewMovie implements Serializable
{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "movie_id")
    private Long id;

    @Column(unique=true)
    protected String title;

    protected int year;

    @ManyToMany(cascade = {CascadeType.ALL} )
    @JoinTable(name="movie_has_genres", joinColumns={@JoinColumn(name="movie_id")}, inverseJoinColumns={@JoinColumn(name="genres_id")})
    @LazyCollection(LazyCollectionOption.FALSE)   
    protected List<NewGenres> genres;

    protected int rank;

    @ManyToMany(cascade = {CascadeType.ALL} )
    @JoinTable(name="movie_has_directors", joinColumns={@JoinColumn(name="movie_id")}, inverseJoinColumns={@JoinColumn(name="director_id")})
    @LazyCollection(LazyCollectionOption.FALSE)         
    protected List<NewDirectors> directors;

    @ManyToMany(cascade = {CascadeType.ALL} )
    @JoinTable(name="movie_has_actors", joinColumns={@JoinColumn(name="movie_id")}, inverseJoinColumns={@JoinColumn(name="actor_id")})
    @LazyCollection(LazyCollectionOption.FALSE)          
    protected List<NewActors> actors;


    public String getTitle() {
        return title;
    }


    public void setTitle(String value) {
        this.title = value;
    }

    public boolean isSetTitle() {
        return (this.title!= null);
    }


    public int getYear() {
        return year;
    }

    
    public void setYear(int value) {
        this.year = value;
    }

    public boolean isSetYear() {
        return true;
    }


    public List<NewGenres> getGenres() {
        if (genres == null) {
            genres = new ArrayList<NewGenres>();
        }
        return this.genres;
    }

    public boolean isSetGenres() {
        return ((this.genres!= null)&&(!this.genres.isEmpty()));
    }

    public void unsetGenres() {
        this.genres = null;
    }

  
    public int getRank() {
        return rank;
    }

   
    public void setRank(int value) {
        this.rank = value;
    }

    public boolean isSetRank() {
        return true;
    }

    public List<NewDirectors> getDirectors() {
        if (directors == null) {
            directors = new ArrayList<NewDirectors>();
        }
        return this.directors;
    }

    public boolean isSetDirectors() {
        return ((this.directors!= null)&&(!this.directors.isEmpty()));
    }

    public void unsetDirectors() {
        this.directors = null;
    }

    /**
     * Gets the value of the actors property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the actors property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getActors().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Actors }
     * 
     * 
     */
    public List<NewActors> getActors() {
        if (actors == null) {
            actors = new ArrayList<NewActors>();
        }
        return this.actors;
    }

    public boolean isSetActors() {
        return ((this.actors!= null)&&(!this.actors.isEmpty()));
    }

    public void unsetActors() {
        this.actors = null;
    }

}
