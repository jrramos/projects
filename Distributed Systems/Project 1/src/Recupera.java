import rmi.*;

class Recupera{
	String menu;
	int opcao;
	Mensagem mensagem;
	String texto;

	Recupera(String menu,int opcao,Mensagem mensagem,String texto){
		this.menu=menu;
		this.opcao=opcao;
		this.mensagem=mensagem;
		this.texto=texto;
	}

	Recupera(){
		this.opcao=-1;
		this.texto="";
		this.mensagem=null;
	}

	String getMenu(){
		return menu;
	}

	int getOpcao(){
		return opcao;
	}

	Mensagem getMensagem(){
		return mensagem;
	}


	String getTexto(){
		return texto;
	}

	void setMenu(String menu){
		this.menu=menu;
	}

	void setOpcao(int opcao){
		this.opcao=opcao;
	}

	void setMensagem(Mensagem mensagem){
		this.mensagem=mensagem;
	}


	void setTexto(String texto){
		this.texto=texto;
	}



}