import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.lang.*;
import java.net.*; 
import java.security.*;
import java.rmi.*;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import javax.imageio.ImageIO;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;



import rmi.*;

public class Server extends UnicastRemoteObject{
    private ServerSocket server;
    private FicheiroDeObjectos fUsernames;
    private GerirLigacoes arrayLigacoes;
    private ARegistos arrayUsers;
    private MensagensUtilizador aMensagens;
    private AClientesRMI arrayRMI;
    private ConnectionHandler h;
    private ASalasChat aSalasChat;

    public static void main(String[] args) throws IOException,ClassNotFoundException, InterruptedException{
        //argumentos =1
        System.getProperties().put("java.security.policy", "policy.all");
        System.setSecurityManager(new RMISecurityManager());
        new Server(args);
    }


    Server(String[] args) throws IOException,ClassNotFoundException, InterruptedException{
        if(args.length!=2){
            System.out.println("O número de dados introduzidos nao e valido");
            System.exit(-1);
        }

        int port=Integer.parseInt(args[0]);

        if(args[1].compareTo("p")==0){
            VerificaServerBackup serverBack=new VerificaServerBackup();
        }else{
            VerificaServerPrincipal serverPrinc=new VerificaServerPrincipal();
            serverPrinc.t.join();
            VerificaServerBackup serverBack=new VerificaServerBackup();
        }


        arrayUsers=new ARegistos();
        fUsernames=new FicheiroDeObjectos();
        arrayLigacoes=new GerirLigacoes();
        fUsernames.abreLeitura("utilizadores.txt");
        arrayUsers=(ARegistos)fUsernames.leObjecto();
        fUsernames.fechaLeitura();
        fUsernames.abreLeitura("mensagens.txt");
        aMensagens=(MensagensUtilizador)fUsernames.leObjecto();
        fUsernames.fechaLeitura();
        fUsernames.abreLeitura("salasChat.txt");
        aSalasChat=(ASalasChat) fUsernames.leObjecto();
        fUsernames.fechaLeitura();
        arrayRMI=new AClientesRMI();

        new ThreadRecepcaoImagens(arrayUsers,arrayLigacoes, aMensagens, arrayRMI);

        try {
            server = new ServerSocket(port);
        } catch (IOException e) {
             System.out.println("Could not listen on port"+ port);
             System.exit(-1);
        }

        try {
            h = new ConnectionHandler(arrayUsers,arrayLigacoes, aMensagens,arrayRMI,aSalasChat);
            Registry r = LocateRegistry.createRegistry(port+1);
            r.rebind("chat", h);

        } catch (RemoteException re) {
            System.out.println("Exception in HelloImpl.main: " + re);
        }

        handleConnection();   
    }

    public void handleConnection() throws ClassNotFoundException{
        System.out.println("Servidor activo.\nÀ espera de um cliente...");

        while (true) {
            try {

                Socket socket = server.accept();
                socket.setTcpNoDelay(true);
                new ConnectionHandler(socket,arrayUsers,arrayLigacoes,aMensagens,arrayRMI);
            } catch (IOException e) {
                System.out.println("Accept failed\n");
                System.exit(-1);
            }
        }
    }
}


class VerificaServerPrincipal implements Runnable{
    DatagramSocket aSocket;
    String texto;
    int serverPort;
    boolean principal;
    Thread t;
    int num=0;

    VerificaServerPrincipal(){
        texto = "ping";
        serverPort = 6789;
        principal=true;
        t=new Thread(this);
        t.start();

    }

    public void run(){
        try{
            aSocket = new DatagramSocket();
            aSocket.setSoTimeout(1000); 
            while(true){
                byte [] m = texto.getBytes();
                InetAddress aHost = InetAddress.getByName("localhost");                                                 
                DatagramPacket request = new DatagramPacket(m,m.length,aHost,serverPort);
                aSocket.send(request);                               
                byte[] buffer = new byte[1000];
                DatagramPacket reply = new DatagramPacket(buffer, buffer.length); 
                do{  
                    aSocket.receive(reply);
                }while((new String(reply.getData(), 0, reply.getLength())).compareTo(texto+num)==0);
                num++;
                principal=false;
            }
    
        }catch (IOException e){}
       
    }

}

class VerificaServerBackup implements Runnable{
    DatagramSocket aSocket;
    String texto;
    int serverPort;
    boolean principal;
    Thread t;
    int num=0;

    VerificaServerBackup(){
        texto = "ping";
        serverPort = 6789;
        principal=true;
        t=new Thread(this);
        t.start();

    }

    public void run(){
        try{
            aSocket = new DatagramSocket(serverPort);
            String s;
            while(true){
                byte[] buffer = new byte[4000];             
                DatagramPacket request = new DatagramPacket(buffer, buffer.length);
                aSocket.receive(request);
                s=new String(request.getData(), 0, request.getLength());    
                DatagramPacket reply = new DatagramPacket(request.getData(),request.getLength(), request.getAddress(), request.getPort());
                aSocket.send(reply);
            }
    
        }catch (IOException e){
            System.out.println("Servidor activo!");
            System.out.println(e.getMessage());
        }
       
    }

}

class ThreadFacebook implements Runnable{
    
    GerirLigacoes arrayLigacoes;
    MensagensUtilizador aMensagens;
    AClientesRMI arrayRMI;
    Thread t;
    FacebookREST fb;

    public ThreadFacebook(GerirLigacoes arrayLigacoes,MensagensUtilizador aMensagens, AClientesRMI arrayRMI,DadosRMI cliente) throws RemoteException{
        this.arrayLigacoes=arrayLigacoes;
        this.aMensagens=aMensagens;
        this.arrayRMI=arrayRMI;
        this.aMensagens=aMensagens;
        this.fb=cliente.getFB();

        t = new Thread(this);
        t.start();        
    }

    public void run() {
        MensagensUtilizador aMensagens1;
        Mensagem mensagem;
        FicheiroDeObjectos fEscrita=new FicheiroDeObjectos();
        while(true){
            try{
                //System.out.println("entrou");
                aMensagens1=fb.get(aMensagens);
                if(aMensagens1!=null){
                    for(int i=0;i<aMensagens1.getSize();i++){
                        mensagem=aMensagens1.getMensagem(i);
                        mensagem.setId(aMensagens.novoId());
                        aMensagens.setMensagem(mensagem);
                        fEscrita.abreEscrita("mensagens.txt");
                        fEscrita.escreveObjecto(aMensagens);
                        fEscrita.fechaEscrita();
                        new EnviarMensagem(arrayRMI,arrayLigacoes,mensagem,0,aMensagens);
                    }
                }
                Thread.sleep(5000);
            }catch(IOException e){
                System.out.println(e.getMessage());
            }
            catch(InterruptedException e){
                System.out.println(e.getMessage());
            }
        }
    }

}


class ConnectionHandler extends UnicastRemoteObject implements Runnable,ChatClient{
    Socket socket;
    ARegistos arrayUsers;
    GerirLigacoes arrayLigacoes;
    ObjectOutputStream  dos;
    ObjectInputStream dis;
    MensagensUtilizador aMensagens;
    AClientesRMI arrayRMI;
    Utilizadores user;
    String username;
    ASalasChat aSalasChat;


    public ConnectionHandler(ARegistos arrayUsers,GerirLigacoes arrayLigacoes,MensagensUtilizador aMensagens, AClientesRMI arrayRMI,ASalasChat aSalasChat) throws RemoteException{
        System.getProperties().put("java.security.policy", "policy.all");
        System.setSecurityManager(new RMISecurityManager());
        this.arrayUsers=arrayUsers;
        this.arrayLigacoes=arrayLigacoes;
        this.aMensagens=aMensagens;
        this.arrayRMI=arrayRMI;
        this.aSalasChat=aSalasChat;
     
        username=new String();
    }


    public ConnectionHandler(Socket socket, ARegistos arrayUsers,GerirLigacoes arrayLigacoes,MensagensUtilizador aMensagens,AClientesRMI arrayRMI) throws ClassNotFoundException, RemoteException{
       try{
            this.socket = socket;
            this.arrayUsers=arrayUsers;
            this.arrayLigacoes=arrayLigacoes;
            this.aMensagens=aMensagens;
            this.arrayRMI=arrayRMI;
    
            dis = new ObjectInputStream(socket.getInputStream());
            dos = new ObjectOutputStream (socket.getOutputStream());
            Thread t = new Thread(this);
            t.start();
        }catch(IOException e){
            System.out.println(e.getMessage());
        }
    }

    public void run() {
        int opcao;
        try
        {
            do{
                opcao = Integer.parseInt((String)dis.readObject());
                // System.out.println(opcao);
                switch(opcao){
                    case 1:login();break;
                    case 2:registar();break;
                    case 11:criarPost(false);break;
                    case 12:criarPost(true);break;
                    case 13:comentarPost();break;
                    case 14:mensagensPessoais(username,false);editarPost();break;
                    case 15:mensagensPessoais(username,false);eliminarMensagem();break;
                   // case 16:criarImagem();break;
                    case 18:mensagensTodas(false,user.getUsername());break;
                    case 19:mostrarUsersTcp(false);mostrarUsersRMI(false);break;
                    case 99:retoma();break;
                }
                
            }while(true);
            
    

        } catch (IOException e) {arrayLigacoes.remLigacao(user);
        }catch (ClassNotFoundException e){
          e.printStackTrace();
        }
        catch(InterruptedException e){};
    }

    public ArrayList<String> mostrarUsersTcp(boolean rmi) throws RemoteException,IOException{
        int i;
        ArrayList<String> aNomes=new ArrayList<String>();

        for(i=0;i<arrayLigacoes.getListaSocketsSize();i++){
            aNomes.add(arrayLigacoes.getLigacoes(i).getUsername());
            if(!rmi)
                dos.writeObject(new Mensagem("",arrayLigacoes.getLigacoes(i).getUsername(),"",0));
        }
        return aNomes;
    }

    public ArrayList<String> mostrarUsersRMI(boolean rmi) throws RemoteException,IOException{
        int i;
        ArrayList<String> aNomes= new ArrayList<String>();

        for(i=0;i<arrayRMI.getSize();i++){
            aNomes.add(arrayRMI.getCliente(i).getUsername());
            if(!rmi)
                dos.writeObject(new Mensagem("",arrayRMI.getCliente(i).getUsername(),"",0));
        }
        return aNomes;
    }

    public void retoma() throws IOException,ClassNotFoundException,InterruptedException{
         Registo registo=(Registo)dis.readObject();
         arrayLigacoes.setLigacoes(user=(new Utilizadores(socket,registo.getUsername(), dos)));

    }

    public void login() throws IOException,ClassNotFoundException,InterruptedException{
        Registo registo;
        try{
        registo=(Registo)dis.readObject();
        login1(registo,false);
        }catch(IOException e){}
        catch(ClassNotFoundException e){}
    }

    public boolean login1(Registo registo,boolean rmi) throws RemoteException,InterruptedException{
        int i=0,a;
        boolean check=false;
        MensagensUtilizador aMensagensReal=new MensagensUtilizador();

        try{
            while(i<arrayUsers.getSize())
            {
            
              if(registo.getUsername().equals(arrayUsers.getRegisto(i).getUsername()) && convertHash(registo.getPassword()).equals(arrayUsers.getRegisto(i).getPassword()))
              {
          
                check=true; 
                if(!rmi){
                    dos.writeObject(true);
                    dos.reset();
                    dos.writeObject(true);
                    arrayLigacoes.setLigacoes(user=(new Utilizadores(socket,registo.getUsername(), dos)));
                    mensagensTodas(false,user.getUsername());
                    username=registo.getUsername();
                }
                    return true;
              }
              i++;
            }
            if(!rmi){
                //System.out.println("Enviou isto");
                dos.writeObject(false);
            }
        }catch (IOException e){
          System.err.println("Error: " + e.getMessage());
        }
        return false;
  }

  boolean procurarUsername(String username){
    int i;
    for(i=0;i<arrayUsers.getSize();i++){
        if(arrayUsers.getRegisto(i).getUsername().compareTo(username)==0)
            return false;
    }
    return true;
  }

  public MensagensUtilizador mensagensPessoais(String username,boolean rmi) throws RemoteException,IOException{
        int a;
        MensagensUtilizador aMensagensReal=new MensagensUtilizador();
        System.out.println("Mensagens Pessoais");
        for(a=0;a<aMensagens.getSize();a++){
            if(aMensagens.getMensagem(a).getUsername().compareTo(username)==0)
                if(rmi)
                    aMensagensReal.setMensagem(aMensagens.getMensagem(a));
                else{
                    dos.writeObject(aMensagens.getMensagem(a));
                    dos.reset();
                }
        }

        return aMensagensReal;
  }

  public void comentarPost() throws RemoteException,ClassNotFoundException,IOException,InterruptedException{
    int id;
    String texto;

    mensagensTodas(false,user.getUsername());
    dos.writeObject(new Mensagem("","Insere o id da mensagem a comentar","",0));
    id=dis.readInt();
    texto=(String)dis.readObject();
    comentarPost1(id,null,texto,user.getUsername());
  }

  public void comentarPost1(int id,String postId,String texto,String username) throws RemoteException,IOException{
    int i;
    Mensagem mensagem;
    FicheiroDeObjectos fEscrita=new FicheiroDeObjectos();
    FacebookREST fb;

    if((fb=(arrayRMI.getClienteNome(username)).getFB())!=null){
            System.out.println("entrou no comentario");
            fb.comment(texto,postId);
    }
    mensagem=aMensagens.getMensagemId(id);
    mensagem.setComentario(new Comentario(username,texto));
    new EnviarMensagem(arrayRMI,arrayLigacoes,new Mensagem("","Mensagem comentada com id="+mensagem.getId(),"",0),0,aMensagens);
    fEscrita.abreEscrita("mensagens.txt");
    fEscrita.escreveObjecto(aMensagens);
    fEscrita.fechaEscrita();
  }

  public MensagensUtilizador mensagensTodas(boolean rmi,String username) throws RemoteException,InterruptedException{
    int a;
    MensagensUtilizador aMensagens1=new MensagensUtilizador();
    Mensagem mensagem;
    LoadImagem loadImagem;
    try{
       for(a=0;a<aMensagens.getSize();a++){

            if((mensagem=aMensagens.getMensagem(a)).getDestinatario().compareTo("")==0 || mensagem.getDestinatario().compareTo(username)==0 || mensagem.getUsername().compareTo(username)==0) {
                if(!rmi){
         
                    if(mensagem.getDirectoria()==null){
                        dos.reset();
                        dos.writeObject(mensagem);
                    }
                    else{
                        loadImagem=new LoadImagem(mensagem,user.getUsername(),arrayRMI,arrayLigacoes,aMensagens);
                        loadImagem.t.join();
                        dos.writeObject(loadImagem.mensagem1);
                    }
                }else{ 
                    if(mensagem.getDirectoria()==null)
                        aMensagens1.setMensagem(mensagem);
                    else{
                        loadImagem=new LoadImagem(mensagem,username,arrayRMI,arrayLigacoes,aMensagens);
                        loadImagem.t.join();
                        aMensagens1.setMensagem(loadImagem.mensagem1);
                    }
                }
            }
        }
    //System.out.println("Mensagens Todas:");
    }catch(IOException e){System.out.println("Erro "+ e.getMessage());}
    return aMensagens1;
  }

  public void enviarImagem(Mensagem mensagem,String username) throws IOException,ClassNotFoundException{

    LoadImagem loadImagem;
    FicheiroDeObjectos fEscrita=new FicheiroDeObjectos();

    try{
        loadImagem=new LoadImagem(mensagem,username,arrayRMI,arrayLigacoes,aMensagens);
        loadImagem.t.join(); 

        aMensagens.setMensagem(mensagem);
        fEscrita.abreEscrita("mensagens.txt");
        fEscrita.escreveObjecto(aMensagens);
        fEscrita.fechaEscrita();
        new EnviarMensagem(arrayRMI,arrayLigacoes,loadImagem.mensagem1,0,aMensagens);
    }catch(InterruptedException e){
        System.out.println(e.getMessage());
    }
  }

  public void registar() throws IOException,ClassNotFoundException{

        try{
            Registo registo=(Registo)dis.readObject();
            registar1(registo,false);
        }catch(IOException e){}
        catch(ClassNotFoundException e){}
  }

  public boolean registar1(Registo registo,boolean rmi) throws IOException, RemoteException{
    boolean resposta;
    try{
        FicheiroDeObjectos fEscrita=new FicheiroDeObjectos();
        resposta=procurarUsername(registo.getUsername());
        if(resposta){
            registo.setPassword(convertHash(registo.getPassword()));
            fEscrita.abreEscrita("utilizadores.txt");
            arrayUsers.setRegisto(registo);
            fEscrita.escreveObjecto(arrayUsers);
            fEscrita.fechaEscrita();
            if(!rmi)
                dos.writeObject(true);
            else
                return true;
        }else{
            if(!rmi)
                dos.writeObject(false);
            else
                return false;
        }

      }catch (Exception e){//Catch exception if any
        System.err.println("Error: " + e.getMessage());
      }
      return false;
  }
      

    public ASalasChat getSalasChat() throws RemoteException{
        return aSalasChat;
    }

    public void criarPost(boolean agendado) throws ClassNotFoundException, RemoteException{
        // System.out.println("a receber mensagem");
         try{
            Mensagem mensagem=(Mensagem)dis.readObject();
            criarPost1(agendado,mensagem);
         }catch(IOException e){
              System.out.println("Erro no envio da mensagem2");
        }

    }

    public void removeRMI(DadosRMI cliente) throws RemoteException{
        int i;
        DadosRMI cliente1;

        for(i=0;i<arrayRMI.getSize();i++){
            if((cliente1=arrayRMI.getCliente(i)).getUsername().compareTo(cliente.getUsername())==0)
                arrayRMI.remCliente(cliente1);
        }
    }

    public void subscribe(DadosRMI cliente,String accessCode) throws RemoteException{
        System.out.println("Subscribe1");
        if(accessCode!=null){
            FacebookREST fb=new FacebookREST(accessCode);
            cliente.setFB(fb);
            new ThreadFacebook(arrayLigacoes,aMensagens,arrayRMI,cliente);
        }

        arrayRMI.setCliente(cliente);
    }

    public void criarPost1(boolean agendado,Mensagem mensagem) throws ClassNotFoundException, RemoteException{
        FicheiroDeObjectos fEscrita=new FicheiroDeObjectos();
        String texto;
        ObjectOutputStream socketEnvio=null;
        int i;
        Calendar cal = Calendar.getInstance();
        Date data1,data=new Date();
        long resData;
        EnviarMensagem novaImagem;
        String postId="";
        FacebookREST fb;
        //System.out.println(codigoAcesso);
        if(mensagem.getDestinatario().compareTo("")==0 && (fb=(arrayRMI.getClienteNome(mensagem.getUsername())).getFB())!=null){
            postId=fb.publish(mensagem);
        }
        //System.out.println("a criar post");
        try{
            mensagem.setId(aMensagens.novoId());
            mensagem.setIdPost(postId);
            aMensagens.setMensagem(mensagem);
            fEscrita.abreEscrita("mensagens.txt");
            fEscrita.escreveObjecto(aMensagens);
            fEscrita.fechaEscrita();
            
            new EnviarMensagem(arrayRMI,arrayLigacoes,mensagem,mensagem.getData()-data.getTime(),aMensagens);
        }catch(IOException e){
              System.out.println("Erro no envio da mensagem1");
            }
        
    }

    public boolean criarSalaChat(SalaChat salaChat) throws ClassNotFoundException, RemoteException{
        FicheiroDeObjectos fEscrita=new FicheiroDeObjectos();

        try{
            salaChat.setId(aSalasChat.novoId());
            if(verificaTema(salaChat.getTema())){
                aSalasChat.setSalaChat(salaChat);
                fEscrita.abreEscrita("salasChat.txt");
                fEscrita.escreveObjecto(aSalasChat);
                fEscrita.fechaEscrita();

                return true;
            }
            return false;
        }catch(IOException e){
            System.out.println("Erro ao criar sala de chat");
            return false;
        }
        
    }

    public ASalasChat getSalasP(String user) throws RemoteException{
        int i;
        ASalasChat aSalasChatP=new ASalasChat();
        for(i=0;i<aSalasChat.getSize();i++){
            if(aSalasChat.getSalaChat(i).getCriador().compareTo(user)==0)
                aSalasChatP.setSalaChat(aSalasChat.getSalaChat(i));
        }

        return aSalasChatP;
    }

    public void remSala(int id) throws RemoteException{
        int i;
        FicheiroDeObjectos fEscrita=new FicheiroDeObjectos();
        for(i=0;i<aSalasChat.getSize();i++){
            if(aSalasChat.getSalaChat(i).getId()==id)
                aSalasChat.remSala(aSalasChat.getSalaChat(i));
        }
        try{
            fEscrita.abreEscrita("salasChat.txt");
            fEscrita.escreveObjecto(aSalasChat);
            fEscrita.fechaEscrita();
        }catch(IOException e){}
    }

    public boolean verificaTema(String tema){
        int i;

        for(i=0;i<aSalasChat.getSize();i++){
            if(aSalasChat.getSalaChat(i).getTema().compareTo(tema)==0)
                return false;
        }
        return true;
    }

    public void editarPost() throws RemoteException{
        int opcao=0;
        String texto="";

        try{
            dos.writeObject(new Mensagem("","Insere o id da mensagem a editar","",0));
            opcao =Integer.parseInt((String)dis.readObject());
            texto =(String)dis.readObject();
            
        }catch(IOException e){}
        catch(ClassNotFoundException e){}
        editarPost1(opcao,texto);
    }

    public void editarPost1(int opcao,String texto) throws RemoteException{
        int i=0;
        FicheiroDeObjectos fEscrita=new FicheiroDeObjectos();
        Mensagem mensagem=null;
        try{

            while(i<aMensagens.getSize()){
                if((mensagem=aMensagens.getMensagem(i)).getId()==opcao){
                    mensagem.setTexto(texto);
                    break;
                }
                i++;
            }
            mensagem=new Mensagem("","Mensagem alterada com id="+mensagem.getId(),"",0);
            fEscrita.abreEscrita("mensagens.txt");
            fEscrita.escreveObjecto(aMensagens);
            fEscrita.fechaEscrita();
            new EnviarMensagem(arrayRMI,arrayLigacoes,mensagem,0,aMensagens);
        }catch(IOException e){}
     }

    public void eliminarMensagem(){
        int opcao;
        try{
             dos.writeObject(new Mensagem("","Insere o id da mensagem a eliminar","",0));
             opcao =Integer.parseInt((String)dis.readObject());
             eliminarMensagem1(opcao,null);
        }catch(IOException e){}
        catch(ClassNotFoundException e){}
    }

    public void eliminarMensagem1(int opcao,String username) throws RemoteException{
        int i=0;
        FicheiroDeObjectos fEscrita=new FicheiroDeObjectos();
        Mensagem mensagem=null;
        int id=0;
        FacebookREST fb;

        try{
            while(i<aMensagens.getSize()){
                // System.out.println((mensagem=aMensagens.getMensagem(i)).getId()==opcao);
                if((mensagem=aMensagens.getMensagem(i)).getId()==opcao){
                    if(username!=null && (fb=(arrayRMI.getClienteNome(username)).getFB())!=null){
                        //System.out.println("entrou");
                        fb.delete(mensagem.getIdPost());
                    }
                    id=mensagem.getId();
                    aMensagens.remMensagem(mensagem);
                    break;
                }
                i++;
            }
            mensagem=new Mensagem("","Mensagem eliminada com o id:"+id,"",0);
            new EnviarMensagem(arrayRMI,arrayLigacoes,mensagem,0,aMensagens);
            fEscrita.abreEscrita("mensagens.txt");
            fEscrita.escreveObjecto(aMensagens);
            fEscrita.fechaEscrita();
        }catch(IOException e){}
     }


    String convertHash(String password){
        String password1=null;
        try{
           
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(password.getBytes());
            byte[] hashMd5 = md.digest();
            password1=stringHexa(hashMd5);
        }catch(Exception SecurityException){
            System.out.println("Fail no hash");
            System.exit(-1);
        }
        return password1;
    }
    
    private static String stringHexa(byte[] bytes) {
       StringBuilder s = new StringBuilder();
       for (int i = 0; i < bytes.length; i++) {
           int parteAlta = ((bytes[i] >> 4) & 0xf) << 4;
           int parteBaixa = bytes[i] & 0xf;
           if (parteAlta == 0) s.append('0');
           s.append(Integer.toHexString(parteAlta | parteBaixa));
       }
       return s.toString();
    }
    
}

class LoadImagem implements Runnable{
    Mensagem mensagem;
    String directoria;
    String destino;
    MensagensUtilizador aMensagens;
    AClientesRMI arrayRMI;
    GerirLigacoes arrayLigacoes;
    Thread t;
    Socket socket;
    Mensagem mensagem1;

    LoadImagem(Mensagem mensagem,String destino,AClientesRMI arrayRMI, GerirLigacoes arrayLigacoes,MensagensUtilizador aMensagens){
        this.mensagem=mensagem;
        System.out.println(mensagem.getDirectoria());
        this.directoria=mensagem.getDirectoria();
        this.destino=destino;
        this.arrayRMI=arrayRMI;
        this.arrayLigacoes=arrayLigacoes;
        this.aMensagens=aMensagens;
        this.socket=socket;
        t = new Thread(this);
        t.start();
    }

    public void run(){
        try{
            ASCIIConverter conv=new ASCIIConverter();
            File myFile = new File(directoria);
            BufferedImage image = ImageIO.read(myFile);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(image, "jpg", baos);

            //String texto=byteArray.toString();
            mensagem1= new Mensagem (mensagem.getUsername(),"",destino,mensagem.getData());
            mensagem1.setId(mensagem.getId());
            mensagem1.setDestinatario("");
            System.out.println("/Users/josericardoramos/Faculdade/3Ano/1Semestre/SD/Projecto1/src/"+directoria);
            mensagem1.setTexto(conv.convertAndResize("/Users/josericardoramos/Faculdade/3Ano/1Semestre/SD/Projecto1/src/"+directoria));
            mensagem1.setImagem(Base64.encode(baos.toByteArray()));
            mensagem1.setArrayComentario(mensagem.getArrayComentario());
            //new EnviarMensagem(arrayRMI,arrayLigacoes,mensagem1,0,aMensagens);
        }catch(FileNotFoundException e){System.out.println("Não encontrou o ficheiro");}
        catch(IOException e){System.out.println(e.getMessage()); }
    }
}

class RecebeImagemServer implements Runnable{
    GerirLigacoes arrayLigacoes;
    Mensagem mensagem;
    MensagensUtilizador aMensagens;
    AClientesRMI arrayRMI;
    String nomeImagem;
    DataInputStream dis;
    int porta;
    Socket socket;
    String username;

    RecebeImagemServer(AClientesRMI arrayRMI, GerirLigacoes arrayLigacoes,MensagensUtilizador aMensagens,Socket socket) throws IOException{
        this.arrayLigacoes=arrayLigacoes;
        this.aMensagens=aMensagens;
        this.arrayRMI=arrayRMI;
        this.porta=porta;
        this.socket=socket;
        this.aMensagens=aMensagens;
        dis=new DataInputStream(socket.getInputStream());
        Thread t=new Thread(this);
        t.start();
    }

    public void run(){
        Imagem imagem;
        Mensagem mensagem,mensagem1;
        Date data=new Date();
        String texto;
        FicheiroDeObjectos fEscrita=new FicheiroDeObjectos();
        int conta=0,i,novoId;
        ASCIIConverter conv = new ASCIIConverter();
  
        try{
            System.out.println("A receber imagem");
            byte[] mybytearray = new byte[1024];
            byte[] ficheiro= new byte[1000000];
            novoId=aMensagens.novoId();
            String nome="Server/"+novoId;
            FileOutputStream fos = new FileOutputStream(nome);           

            int count;

            username=dis.readUTF();
            //System.out.println(username);
            while ((count = dis.read(mybytearray)) > 0 ) {
                // System.out.println(count);
                fos.write(mybytearray, 0, count);
                for(i=conta;i<count+conta;i++)
                    ficheiro[i]=mybytearray[i-conta];
                conta+=count;
            }

            //texto=ficheiro.toString();
            // System.out.println(texto);
            mensagem=new Mensagem(username,nome,data.getTime());
            mensagem.setId(novoId);
            aMensagens.setMensagem(mensagem);
            mensagem1=new Mensagem(username,"","",data.getTime());
            mensagem1.setId(novoId);
            mensagem1.setTexto(conv.convertAndResize("/Users/josericardoramos/Faculdade/3Ano/1Semestre/SD/Projecto1/src/"+nome));
            mensagem1.setImagem(Base64.encode(ficheiro));
            fEscrita.abreEscrita("mensagens.txt");
            fEscrita.escreveObjecto(aMensagens);
            fEscrita.fechaEscrita();
            new EnviarMensagem(arrayRMI,arrayLigacoes,mensagem1,0,aMensagens);
        }catch(IOException e){System.out.println("Erro na thread receber os ficheiros1");}
    }
}

class ThreadRecepcaoImagens implements Runnable{
    ServerSocket server;
    Thread t;
    ARegistos arrayUsers;
    GerirLigacoes arrayLigacoes;
    MensagensUtilizador aMensagens;
    AClientesRMI arrayRMI;

    ThreadRecepcaoImagens(ARegistos arrayUsers,GerirLigacoes arrayLigacoes,MensagensUtilizador aMensagens, AClientesRMI arrayRMI) throws IOException{
        //System.out.println("a espera de ficheiros");
        this.arrayUsers=arrayUsers;
        this.arrayLigacoes=arrayLigacoes;
        this.aMensagens=aMensagens;
        this.arrayRMI=arrayRMI;
        server=new ServerSocket(7000);
        t=new Thread(this);
        t.start();
    }

    public void run(){
        while (true) {
            try {
                Socket socket = server.accept();
                socket.setTcpNoDelay(true);
                new RecebeImagemServer(arrayRMI,arrayLigacoes,aMensagens,socket);
            } catch (IOException e) {
                System.out.println("Accept failed\n");
                System.exit(-1);
            }
        }
    }
}

class EnviarMensagem implements Runnable{
    GerirLigacoes arrayLigacoes;
    Mensagem mensagem;
    long data;
    MensagensUtilizador aMensagens;
    AClientesRMI arrayRMI;

    EnviarMensagem(AClientesRMI arrayRMI, GerirLigacoes arrayLigacoes,Mensagem mensagem,long data,MensagensUtilizador aMensagens){
        this.arrayLigacoes=arrayLigacoes;
        this.mensagem=mensagem;
        this.data=data;
        this.aMensagens=aMensagens;
        this.arrayRMI=arrayRMI;
        Thread t = new Thread(this);
        t.start();
    }  

    public void run(){
        int i;
        ChatServer clienteRMI;

     //   System.out.println(arrayLigacoes.getLigacoes(0).getOutput()==null);
        try{
            if(data>0){
                Thread.sleep(data);
                aMensagens.remMensagem(mensagem);
                aMensagens.setMensagem(mensagem);
            }

            if(mensagem.getDestinatario().compareTo("")==0){
                    for(i=0;i<arrayLigacoes.getSize();i++)
                        (arrayLigacoes.getLigacoes(i).getOutput()).writeObject(mensagem);
                    for(i=0;i<arrayRMI.getSize();i++){
                        try{
                        arrayRMI.getCliente(i).getChatServer().printMensagem(mensagem);
                        }catch(IOException e){
                            System.out.println("Remover user");
                            arrayRMI.remCliente(arrayRMI.getCliente(i));
                        }
                    }
            }else{
                for(i=0;i<arrayLigacoes.getSize();i++){
                          if(arrayLigacoes.getLigacoes(i).getUsername().compareTo(mensagem.getDestinatario())==0 || arrayLigacoes.getLigacoes(i).getUsername().compareTo(mensagem.getUsername())==0){
                            (arrayLigacoes.getLigacoes(i).getOutput()).writeObject(mensagem);
                          }
                }
                for(i=0;i<arrayRMI.getSize();i++){
                    try{
                     if(arrayRMI.getCliente(i).getUsername().compareTo(mensagem.getDestinatario())==0 || arrayRMI.getCliente(i).getUsername().compareTo(mensagem.getUsername())==0)
                        arrayRMI.getCliente(i).getChatServer().printMensagem(mensagem);
                    }catch(IOException e){
                        System.out.println("Remover user");
                        arrayRMI.remCliente(arrayRMI.getCliente(i));
                    }
                }
            }

        }catch(IOException e){
            System.out.println("Erro ao enviar");
                    
        }catch(InterruptedException e){
            System.out.println("Erro ao enviar");
            System.out.println(e.getMessage());
        }
    }
}