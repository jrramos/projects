import java.util.*;
import java.lang.*;
import java.io.*;

import rmi.*;

public class ARegistos implements Serializable{
	ArrayList<Registo> aRegistos;

	public ARegistos(){
		aRegistos=new ArrayList<Registo>();
	}

	public synchronized void setRegisto(Registo registo){
		aRegistos.add(registo);
	}

	public synchronized Registo getRegisto(int i){
		return  aRegistos.get(i);
	}

	public synchronized int getSize(){
		return aRegistos.size();
	}

}