package rmi;

import java.io.*;
import java.util.*;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

public class Mensagem implements Serializable
{
	String username;
	String texto;
	int id;
	String idPost;
	String destinatario;
	long data;
	String imagem;
	String directoria;
	ArrayList<Comentario> comentarios;

	public Mensagem(String username,String texto,String destinatario,long data){
		this.username=username;
		this.texto=texto;
		this.destinatario=destinatario;
		this.data=data;
		this.imagem=null;
		this.idPost="";
		comentarios=new ArrayList<Comentario>();
	}

	public Mensagem(){
		username=new String();
		texto=new String();
		destinatario=new String();
		directoria=new String();
		comentarios=new ArrayList<Comentario>();
		this.idPost="";
		imagem=null;
	}
	public Mensagem(String username,String directoria,long data){
		this.username=username;
		this.directoria=directoria;
		this.data=data;
		this.destinatario="";
		this.idPost="";
		comentarios=new ArrayList<Comentario>();
	}

	public String getDestinatario(){
		return destinatario;
	}

	public void setDestinatario(String destinatario){
		this.destinatario=destinatario;
	}

	public String getImagem(){
		return imagem;
	}

	public void setImagem(String imagem){
		this.imagem=imagem;
	}

	public String getTexto(){
		synchronized(comentarios){
			return texto;
		}
	}

	public void setArrayComentario(ArrayList<Comentario> comentarios){
		synchronized(comentarios){
			this.comentarios=comentarios;
		}
	}

	public ArrayList<Comentario> getArrayComentario(){
	 	synchronized(comentarios){
			return comentarios;
		}
	}

	public void setTexto(String texto){
		synchronized(texto){
			this.texto=texto;
		}
	}

	public String getUsername(){
		return username;
	}

	public String getDirectoria(){
		return directoria;
	}

	public void setComentario(Comentario comentario){
		synchronized(comentarios){
			comentarios.add(comentario);
		}
	}

	public int getId(){
		return id;
	}

	public void setId(int id){
		this.id=id;
	}

	public String getIdPost(){
		return idPost;
	}

	public void setIdPost(String idPost){
		this.idPost=idPost;
	}

	public int getComentariosSize(){
		synchronized(comentarios){
			return comentarios.size();
		}
	}

	public String getComentarios(){
		String comments=new String();
		int i;

		for(i=0;i<getComentariosSize();i++)
	 		comments+=comentarios.get(i)+"<br>";

	 	return comments;
	}
	
	public String toString(){
	 	String destinatario1=new String(),comments=new String();
	 	int i;

	 	if(destinatario.compareTo("")!=0)
	 		destinatario1=" -> "+destinatario;
	 	destinatario1+='\n';
	 	if(getComentariosSize()>0)
	 		comments+='\n';
	 	for(i=0;i<getComentariosSize();i++)
	 		comments+=comentarios.get(i);
	 	
	 	return username+": "+destinatario1+texto+comments;
	}

	public String toStringWeb(){
		String destinatario1=new String(),comments=new String();
		int i;

	 	if(destinatario.compareTo("")!=0)
	 		destinatario1=" -> "+destinatario;
	 	destinatario1+='\n';
	 	if(getComentariosSize()>0)
	 		comments+='\n';
	 	for(i=0;i<getComentariosSize();i++)
	 		comments+=comentarios.get(i);
		return "<b><a href='comentarMensagem.jsp?id="+id+"'>"+username+"</a></b>:<br> "+destinatario1+texto+comments;
	}

	public String imagens(){
	
		return "<b>"+username+"</b>:<br>"+" "+"<img  src='data:image/jpg;base64,"+imagem+"' alt='IMG DESC'>";
	}

	public long getData(){
		return data;
	}

	public void setData(long data){
		this.data=data;
	}

	public void killMensagem(){
		data=-1;
	}
} 