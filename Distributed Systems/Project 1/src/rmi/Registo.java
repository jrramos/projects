package rmi;

import java.io.*;
public class Registo implements Serializable{
	private String username;
	private String password;

	public Registo(String username,String password){
		this.username=username;
		this.password=password;
	}

	public String getUsername(){
		return username;
	}

	public String getPassword(){
		return password;
	}

	public void setPassword(String password){
		this.password=password;
	}

}