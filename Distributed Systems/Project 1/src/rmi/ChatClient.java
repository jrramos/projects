package rmi;

import java.rmi.*;
import java.io.*;

import java.util.ArrayList;
public interface ChatClient extends Remote {
	public void eliminarMensagem1(int opcao,String username) throws RemoteException;
	public void editarPost1(int opcao,String texto) throws RemoteException;
	public void criarPost1(boolean agendado,Mensagem mensagem) throws ClassNotFoundException, RemoteException;
	public boolean registar1(Registo registo,boolean rmi) throws IOException, RemoteException;
	public boolean login1(Registo registo,boolean rmi) throws RemoteException,InterruptedException;
	public MensagensUtilizador mensagensPessoais(String username,boolean rmi) throws RemoteException,IOException;
	public void subscribe(DadosRMI cliente,String accessCode) throws RemoteException;
	public MensagensUtilizador mensagensTodas(boolean rmi,String username) throws RemoteException,InterruptedException;
	public void comentarPost1(int id,String postId,String texto,String username) throws RemoteException,IOException;
	public ArrayList<String> mostrarUsersTcp(boolean rmi) throws RemoteException,IOException;
	public ArrayList<String> mostrarUsersRMI(boolean rmi) throws RemoteException,IOException;
	public void removeRMI(DadosRMI cliente) throws RemoteException;
	public boolean criarSalaChat(SalaChat salaChat) throws ClassNotFoundException, RemoteException;
	public ASalasChat getSalasChat() throws RemoteException;
	public ASalasChat getSalasP(String user) throws RemoteException;
	public void remSala(int id) throws RemoteException;
	public void enviarImagem(Mensagem mensagem,String username) throws IOException,ClassNotFoundException;
}