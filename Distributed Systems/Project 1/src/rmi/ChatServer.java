package rmi;

import java.rmi.*;
import java.io.*;

public interface ChatServer extends Remote {
	public void printMensagem(Mensagem mensagem) throws RemoteException;
}