package rmi;

import java.io.*;
import java.util.*;

public class ASalasChat implements Serializable{
	private ArrayList<SalaChat> salasChat;
	private int id;

	public ASalasChat(){
		salasChat=new ArrayList<SalaChat>();
	}

	public synchronized void setSalaChat(SalaChat sala){
		salasChat.add(sala);
	}

	public synchronized SalaChat getSalaChat(int i){
		return salasChat.get(i);
	}

	public synchronized int novoId(){
		id++;
		return id;
	}

	public synchronized SalaChat getSalaId(int id){
		int i;

		for(i=0;i<salasChat.size();i++){
			if(salasChat.get(i).getId()==id)
				return salasChat.get(i);
		}
		return null;
	}

	public synchronized int getSize(){
		return salasChat.size();
	}

	public synchronized int getId(){
		return id;
	}

	public synchronized void remSala(SalaChat sala){
		salasChat.remove(sala);
	}
}