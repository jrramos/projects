package rmi;

import java.util.*;

import org.scribe.builder.*;
import org.scribe.builder.api.*;
import org.scribe.model.*;
import org.scribe.oauth.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import rmi.*;

public class FacebookREST
{
  private static final Token EMPTY_TOKEN = null;
  private OAuthService service;
  private Token accessToken;
  private String uId;

  public FacebookREST(String codigoAcesso)
  {
    // Replace these with your own api key and secret
    String apiKey = "195487357242798";
    String apiSecret = "4ef6a13e293b3d3523da67972f2b5005";
    
    
    service = new ServiceBuilder()
                                  .provider(FacebookApi.class)
                                  .apiKey(apiKey)
                                  .apiSecret(apiSecret)
                                  .callback("http://eden.dei.uc.pt/~amaf/echo.php") // Do not change this.
                                  .scope("publish_stream,read_stream")
                                  .build();

    Verifier verifier = new Verifier(codigoAcesso);
    //System.out.println(verifier);
    accessToken = service.getAccessToken(EMPTY_TOKEN, verifier);
    uId=getIdUser();
  }


  synchronized public String publish(Mensagem mensagem){
    String body;
    String id;
    
    String texto=stringToHTMLString(mensagem.getTexto());
    String pedido="https://api.facebook.com/method/stream.publish?message="+texto+"&uid="+uId+"&privacy={'value':'ALL_FRIENDS'}&format=json";
    OAuthRequest request = new OAuthRequest(Verb.GET, pedido);
    service.signRequest(accessToken, request);
    Response response = request.send();
    //System.out.println(response.getCode());
    id=(response.getBody()).toString().substring(1, ((response.getBody()).toString()).length()-1) ;
    //System.out.println(id);
    return id;
  }

  synchronized public MensagensUtilizador get(MensagensUtilizador aMensagens){
  
    MensagensUtilizador aMensagens1=new MensagensUtilizador();
    String pedido="https://api.facebook.com/method/stream.get?viewer_id="+uId+"&format=json";
    OAuthRequest request = new OAuthRequest(Verb.GET, pedido);
    service.signRequest(accessToken, request);
    Response response = request.send();
    HashMap hm = new HashMap(); 
    JSONObject o= (JSONObject )JSONValue.parse(response.getBody());
    JSONArray arrPosts= (JSONArray) o.get("posts");
    JSONArray arrUsers= (JSONArray) o.get("profiles");
    Mensagem mensagem;
    boolean check;

    for(int i=0; i< arrPosts.size(); i++) {
      System.out.println("entrou");
      JSONObject item = (JSONObject) arrPosts.get(i);
  
      if((item.get("attribution")==null || (item.get("attribution").toString()).compareTo("Social More")!=0) && (item.get("message").toString()).compareTo("")!=0){
        check=false;
        if(hm.get(item.get("actor_id"))!=null){
          for(int e=0;e<aMensagens.getSize();e++){
            if((aMensagens.getMensagem(e).getIdPost()).compareTo((String)item.get("post_id"))==0)
              check=true;
          }
          if(!check){
            mensagem=new Mensagem((String)hm.get(item.get("actor_id")),(String)item.get("message"),"",Integer.parseInt(item.get("created_time").toString()));
            mensagem.setIdPost((String)item.get("post_id"));
            System.out.println("nova mensagem "+mensagem);
            aMensagens1.setMensagem(mensagem);
          }
        }else{
          //System.out.println("entrou");
          for(int a=0;a<arrUsers.size();a++){
            JSONObject profile = (JSONObject) arrUsers.get(a);
            //System.out.println(profile.get("id")+" "+item.get("actor_id"));
            if((profile.get("id").toString()).compareTo(item.get("actor_id").toString())==0){
              hm.put(profile.get("id"),profile.get("name"));
            }
          }
          for(int e=0;e<aMensagens.getSize();e++){
            //System.out.println((aMensagens.getMensagem(e).getIdPost()));
            //System.out.println((String)item.get("post_id"));
            if((aMensagens.getMensagem(e).getIdPost()).compareTo((String)item.get("post_id"))==0)
              check=true;
          }
          if(!check){
            mensagem=new Mensagem((String)hm.get(item.get("actor_id")),(String)item.get("message"),"",Integer.parseInt(item.get("created_time").toString()));
            mensagem.setIdPost((String)item.get("post_id"));
            System.out.println("nova mensagem "+mensagem);
            aMensagens1.setMensagem(mensagem);
          }
      
        }
      }
    }

    return aMensagens1;
  }

  synchronized public String getIdUser(){
    String pedido="https://graph.facebook.com/me&format=json";
    OAuthRequest request = new OAuthRequest(Verb.GET, pedido);
    service.signRequest(accessToken, request);
    Response response = request.send();
    JSONObject o= (JSONObject) JSONValue.parse(response.getBody());
    //System.out.println(response.getBody());
    String id = (o.get("id")).toString();
    System.out.println(response.getCode());

    return id;
  }

  synchronized public void comment(String texto,String id){

    System.out.println("comment id :"+id);
    texto=stringToHTMLString(texto);
    String pedido="https://api.facebook.com/method/stream.addComment?comment="+texto+"&uid="+uId+"&post_id="+id;
    OAuthRequest request = new OAuthRequest(Verb.GET, pedido);
    service.signRequest(accessToken, request);
    Response response = request.send();
    System.out.println("comentar "+response.getCode());
    System.out.println("comentar "+response.getBody());
    // System.out.println(response.getCode());
    // System.out.println(response.getBody());
  }

  synchronized public void delete(String post_id){
    // System.out.println(post_id);

    String pedido="https://api.facebook.com/method/stream.remove?post_id="+post_id+"&uid="+uId;
    System.out.println("delete id:"+post_id);
    OAuthRequest request = new OAuthRequest(Verb.GET, pedido);
    service.signRequest(accessToken, request);
    Response response = request.send();
    System.out.println("apagou "+response.getCode());
    System.out.println("apagou "+response.getBody());

  }

  synchronized public String stringToHTMLString(String string) {
    int size=string.length();
    String resp=new String();
    char c;
    for (int i = 0; i < size; i++)
    {
        c = string.charAt(i);
        if (c == ' ') 
            resp+="+";
        else if (c == '"')
          resp+="&quot;";
        else if (c == '&')
          resp+="&amp;";
        else if (c == '<')
          resp+="&lt;";
        else if (c == '>')
          resp+="&gt;";
        else if (c == '\n')
          resp+="&lt;br/&gt;";
        else
          resp+=c;
            
    }
    System.out.println("conversao "+resp);
    return resp;
  }
}