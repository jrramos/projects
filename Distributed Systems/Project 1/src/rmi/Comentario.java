package rmi;

import java.util.*;
import java.lang.*;
import java.io.*;

public class Comentario implements Serializable{
	String username;
	String texto;

	public Comentario(String username, String texto){
		this.username=username;
		this.texto=texto;
	}

	public String toString(){
		return "*"+username+": "+texto;
	}
}