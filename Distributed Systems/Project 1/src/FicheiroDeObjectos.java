import java.io.*;
import java.lang.*;

public class FicheiroDeObjectos
{
    private ObjectInputStream iS;
    private ObjectOutputStream oS;
    
    public void abreLeitura(String nomeDoFicheiro) throws IOException {
        iS = new ObjectInputStream(new FileInputStream(nomeDoFicheiro));
    }
    
    public void abreEscrita(String nomeDoFicheiro) throws IOException {
        oS = new ObjectOutputStream(new FileOutputStream(nomeDoFicheiro)); 
    }
    
    public void addEscrita(String nomeDoFicheiro) throws IOException {
        oS = new ObjectOutputStream(new FileOutputStream(nomeDoFicheiro,true)); 
    }
    
    public Object leObjecto() throws IOException,ClassNotFoundException
    {
        return iS.readObject(); 
    }
    
    public void escreveObjecto(Object o) throws IOException {
        oS.writeObject(o); 
    }
    
    public void fechaLeitura() throws IOException
    {
        iS.close();
    }

    public void fechaEscrita() throws IOException
    {
        oS.close();
    }
}
