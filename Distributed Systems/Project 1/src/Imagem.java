class Imagem{
	String nome;
	byte[] imagem;

	Imagem(String nome,byte[] imagem){
		this.nome=nome;
		this.imagem=imagem;
	}

	String getNome(){
		return nome;
	}

	byte[] getImagem(){
		return imagem;
	}
}