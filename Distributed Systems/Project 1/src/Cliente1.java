import java.net.*;
import java.io.*;
import java.lang.*;
import java.util.*;
import java.rmi.*;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.*;

import rmi.*;

public class Cliente1 extends UnicastRemoteObject implements ChatServer{

	ChatClient ligacao=null;
	MensagensUtilizador aMensagens;
	InputStreamReader input= new InputStreamReader(System.in);
	BufferedReader reader= new BufferedReader(input);
	String usernameSessao;
	Cliente1 novoCliente=null;
	DadosRMI clienteRMI;
	Mensagem ultimaMensagem;
	int[] portas;
	int porta=0;
	Boolean logado=false;
	int numTemps;

	public static void main(String args[]){
		try{
			if(args.length!=2){
	            System.out.println("O número de dados introduzidos nao e valido");
	            System.exit(-1);
        	}
        
			new Cliente1(args);
		}catch(InterruptedException e){
			System.out.println("Erroo1");
		}
		catch(NotBoundException e){
			System.out.println("Erroo2");
		}
		catch(RemoteException e){
			System.out.println("Erroo3");
		}
	}

	public void  printMensagem(Mensagem mensagem){
		System.out.println(mensagem);
	}


	Cliente1() throws RemoteException{
			super();
	}

	Cliente1(String args[]) throws InterruptedException,NotBoundException,RemoteException{
		int opcao;

		while(true){
			try {
				if(!logado){
					portas=new int[2];
		        	portas[0]=Integer.parseInt(args[0]);
		        	portas[1]=Integer.parseInt(args[1]);
					ligacao = (ChatClient) LocateRegistry.getRegistry(portas[porta]).lookup("chat");
		       		novoCliente= new Cliente1();
		      
					input = new InputStreamReader(System.in);
					reader = new BufferedReader(input);
					do{
						System.out.println("(1)Login\n(2)Registar\n(0)Sair\n");
						opcao=Integer.parseInt(reader.readLine());

						switch(opcao){
							case 1: loginRegisto(true);break;
							case 2: loginRegisto(false);break;
							case 3: System.exit(1);
			            			break;
						}
					}while(opcao!=0);

					ligacao.removeRMI(clienteRMI);
					System.exit(0);
				}else{
					Thread.sleep(2000);
					ligacao = (ChatClient) LocateRegistry.getRegistry(portas[porta]).lookup("chat");
	       			novoCliente= new Cliente1();
				}
			} catch (IOException e) {
				porta++;
	       		porta%=2;
			}
	
	       	catch(NotBoundException e){
	       		System.out.println("Erro2");
	       	}
			
		}
	}

	public void recoonectar(){
		int opcao;
		while(true){
			try {
				System.out.println(logado);
				if(!logado){
					ligacao = (ChatClient) LocateRegistry.getRegistry(portas[porta]).lookup("chat");
		       		novoCliente= new Cliente1();
		      
					input = new InputStreamReader(System.in);
					reader = new BufferedReader(input);
					do{
						System.out.println("(1)Login\n(2)Registar\n(0)Sair\n");
						opcao=Integer.parseInt(reader.readLine());

						switch(opcao){
							case 1: loginRegisto(true);break;
							case 2: loginRegisto(false);break;
							case 3: System.exit(1);
			            			break;
						}
					}while(opcao!=0);

					ligacao.removeRMI(clienteRMI);
					System.exit(0);
				}else{
					Thread.sleep(2000);
					ligacao = (ChatClient) LocateRegistry.getRegistry(portas[porta]).lookup("chat");
	       			novoCliente= new Cliente1();
	       			ligacao.subscribe(clienteRMI=new DadosRMI((ChatServer) novoCliente,usernameSessao),null);
	       			break;

				}
			} catch (IOException e) {
				porta++;
	       		porta%=2;
				System.out.println("Erro1R");
			}
	
	       	catch(NotBoundException e){
	       		System.out.println("Erro2R");
	       	}
			catch(InterruptedException e){
				System.out.println("errooo");
			}
		}
	}

	public void loginRegisto(Boolean login){
		String username="";
		String password;
		Boolean resposta=false;
		Registo registo=null;

		try{
			System.out.println("Insere o username");
			username=reader.readLine();
			System.out.println("Insere a password");
			password=reader.readLine();

			registo=new Registo(username,password);
			if(login)
				resposta=(Boolean)ligacao.login1(registo,true);
			else
				resposta=(Boolean)ligacao.registar1(registo,true);

			loginRegisto1(login,resposta,username);
		}catch(IOException e){
			try{
				recoonectar();
				resposta=(Boolean)ligacao.login1(registo,true);
				loginRegisto1(login,resposta,username);
			}catch(InterruptedException a){}
			catch(IOException a){}
		}
		catch(InterruptedException e){
			System.out.println("Erro5");
		}
	}


	public void loginRegisto1(Boolean login,Boolean resposta,String username){
		try{
			if(login){
				if(resposta){
					System.out.println("\nLogin com sucesso!\n");
					logado=true;
					aMensagens=(MensagensUtilizador)ligacao.mensagensPessoais(username,true);
					usernameSessao=username;
					ligacao.subscribe(clienteRMI=new DadosRMI((ChatServer) novoCliente,usernameSessao),null);
					System.out.println("(1)Criar post imediato\n(2)Criar post agendado\n(3)Comentar post\n(4)Editar post\n(5)Eliminar post\n(6)Voltar a mostrar o menu\n(7)Ver mensagens\n(8)Ver users online\n(0)Sair da area de cliente\n");
					verMensagens();
					areaUtilizador();
				}else
					System.out.println("\nLogin invalido!\n");
			}else{
				if(resposta)
					System.out.println("\nRegistado com sucesso!\n");
				else
					System.out.println("\nRegisto sem sucesso!\nMude de username\n");
				}
		}catch(IOException e){
			recoonectar();
			loginRegisto1(login,resposta,username);

		}catch(NotBoundException e){
			System.out.println("Erro4");
		}
		catch(InterruptedException e){
			System.out.println("Erro5");
		}
		
		
	}

	public void verMensagens() {
		int i=0;
		MensagensUtilizador mensagens;
		try{
			mensagens=ligacao.mensagensTodas(true,usernameSessao);

			for(i=0;i<mensagens.getSize();i++){
				System.out.println(mensagens.getMensagem(i));
			}
		}catch(RemoteException e){
			
		}catch(IOException e){}
		catch(InterruptedException e){}
	}

	public void areaUtilizador() throws IOException,InterruptedException,NotBoundException{
		int opcao,i;

		try{
			do{
				opcao=Integer.parseInt(reader.readLine());
				switch (opcao) {
					case 1:criarPost(false);break;
	                case 2:criarPost(true);break;
	                case 3:comentarPost();break;
	                case 4:editarPost();break;
	                case 5:eliminarPost();break;
					case 6: System.out.println("(1)Criar post imediato\n(2)Criar post agendado\n(3)Comentar post\n(4)Editar post\n(5)Eliminar post\n(6)Voltar a mostrar o menu\n(8)Ver users online\n(7)Ver mensagens\n(0)Sair da area de cliente\n");break;
					case 7: verMensagens();break;
					case 8: mostrarUsers();break;
				}
			}while(opcao!=0);
		}
		catch(IOException e){
			System.out.println("Erro6");
		}

	}

	public void mostrarUsers() throws IOException{
		ArrayList<String> aMensagens;
		int i=0;

		try{
			aMensagens=ligacao.mostrarUsersTcp(true);
			for(i=0;i<aMensagens.size();i++)
				System.out.println(aMensagens.get(i));
			aMensagens=ligacao.mostrarUsersRMI(true);
			for(i=0;i<aMensagens.size();i++)
				System.out.println(aMensagens.get(i));
		}catch(RemoteException e){
			recoonectar();
		}
	}

	public void criarPost(Boolean agendado){
      String texto,destinatario="",dataString;
      String opcao;
      Mensagem mensagem=new Mensagem();
      int i=0;
      Boolean check=false;
      Date data1,data=new Date();
      long resData;
      Scanner sc;

      ultimaMensagem=new Mensagem();
      try{
        System.out.println("Insira texto:");
        texto=reader.readLine();
        do{
          System.out.println("Esta mensagem é privada?(s/n)");
          opcao=reader.readLine();
        }while(opcao.compareTo("s")!=0 && opcao.compareTo("n")!=0);

        if(opcao.compareTo("s")==0){
        	System.out.println("Insira o destinatario:");
        	destinatario=reader.readLine();
        }

       if(agendado){
	    	System.out.println("Insere a data de envio(yyyy MM dd HH mm)");
	    	dataString=reader.readLine();
	    	sc=new Scanner(dataString);
	    	sc.useDelimiter(" ");
            data1 = new GregorianCalendar(sc.nextInt(),sc.nextInt()-1,sc.nextInt(), sc.nextInt(),sc.nextInt()).getTime();
            resData=data1.getTime();
	    }else
	    	resData=data.getTime();

	   	mensagem=new Mensagem(usernameSessao,texto,destinatario,resData);
	   	ultimaMensagem=mensagem;
	    ligacao.criarPost1(agendado,mensagem);
	    aMensagens.setMensagem(mensagem);
      }catch(IOException e){
      	try{
	  		recoonectar();
	  		ligacao.criarPost1(agendado,ultimaMensagem);
	  		aMensagens.setMensagem(mensagem);
	  	}catch(RemoteException a){}
	  	catch(ClassNotFoundException a){}
  	  }catch(ClassNotFoundException e){}

    }

    public void editarPost(){
    	int id=0;
    	int i;
    	Mensagem mensagem=null;
    	String opcao="";

    	try{
    		ultimaMensagem=new Mensagem();
    		aMensagens=ligacao. mensagensPessoais(usernameSessao,true);
	    	for(i=0;i<aMensagens.getSize();i++){
	    		if((mensagem=aMensagens.getMensagem(i)).getUsername().compareTo(usernameSessao)==0)
	    			System.out.println("Id mensagem: "+mensagem.getId()+"\nTexto: "+mensagem.getTexto());
	    	}

	    	System.out.println("Insere o id da mensagem a alterar");
	    	id=Integer.parseInt(reader.readLine());
	    	System.out.println("Insere o novo texto");
	    	opcao=reader.readLine();
	    	mensagem.setTexto(opcao);
	    	ultimaMensagem=mensagem;
	    	ligacao.editarPost1(id,opcao);
	    }catch(IOException e){
	    	try{
	    		recoonectar();
	    		ligacao.editarPost1(id,opcao);
	    	}catch(RemoteException a){}
	    }
    }

    public void comentarPost(){
    	int id=0;
    	String texto="";
    	Mensagem mensagem;

    	try{
    		ultimaMensagem=new Mensagem();
	    	verMensagens();
	    	System.out.println("Insere o id da mensagem");
	    	id=Integer.parseInt(reader.readLine());
	    	ultimaMensagem.setId(id);
	    	System.out.println("Insere o comentario");
	    	texto=reader.readLine();
	    	ultimaMensagem.setTexto("comentar");
	    	ultimaMensagem.setComentario(new Comentario(usernameSessao,texto));
	    	ligacao.comentarPost1(id,null,texto,usernameSessao);
	    }catch(RemoteException e){
			
		}catch(IOException e){
			try{
				recoonectar();
				ligacao.comentarPost1(id,null,texto,usernameSessao);
			}catch(IOException a){}
		}

    }

    public void eliminarPost(){
    	int i=0;
    	int id=0;
    	Mensagem mensagem=null;

    	try{
    		aMensagens=ligacao. mensagensPessoais(usernameSessao,true);
	    	for(i=0;i<aMensagens.getSize();i++){
	    		if((mensagem=aMensagens.getMensagem(i)).getUsername().compareTo(usernameSessao)==0)
	    			System.out.println("Id mensagem: "+mensagem.getId()+"\nTexto: "+mensagem.getTexto()+"\n");
	    	}

	    	System.out.println("Insere o id da mensagem a eliminar");
	    	id=Integer.parseInt(reader.readLine());
	    	ultimaMensagem.setId(id);
	    	ultimaMensagem.setTexto("eliminar");
	    	ligacao.eliminarMensagem1(id,null);
	    }catch(IOException e){
	    	try{
		    	recoonectar();
		    	ligacao.eliminarMensagem1(id,null);
		    }catch(RemoteException a){}
	    
    	}
    }
    public void getMenu(){
    	System.out.println("(1)Criar post imediato\n(2)Criar post agendado\n(3)Editar post\n(4)Eliminar post\n(5)Criar imagem\n(6)Eliminar imagem\n(7)Mandar mensagem privada\n(0)Sair da area de cliente\n");
    }

}
