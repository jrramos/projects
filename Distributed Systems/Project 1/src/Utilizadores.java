import java.net.Socket;
import java.io.*;

class Utilizadores{
    private Socket socket;
    private String username;
    private ObjectOutputStream outputO;

    Utilizadores(Socket socket,String username, ObjectOutputStream outputO){
    	this.socket=socket;
    	this.username=username;
        this.outputO=outputO;
    }

    Socket getSocket(){
    	return socket;
    }

    ObjectOutputStream getOutput(){
        return outputO;
    }

    String getUsername(){
    	return username;
    }

}