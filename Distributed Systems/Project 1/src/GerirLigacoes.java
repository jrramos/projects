import java.util.*;
import java.lang.*;
import java.io.*;

class GerirLigacoes implements Serializable{
    ArrayList<Utilizadores> listaSockets;

    GerirLigacoes(){
        listaSockets=new ArrayList<Utilizadores>();
    }

    void setLigacoes(Utilizadores ligacao){
        synchronized(listaSockets){
            listaSockets.add(ligacao);
        }
    }

    Utilizadores getLigacoes(int i){
        synchronized(listaSockets){
            return listaSockets.get(i);
        }
    }

   int getSize(){
        synchronized(listaSockets){
            return listaSockets.size();
        }
    }

    void remLigacao(Utilizadores user){
        listaSockets.remove(user);
    }

    int getListaSocketsSize(){
        return listaSockets.size();
    }
}