
import java.util.*;

import rmi.*;

public class AClientesRMI{
	ArrayList<DadosRMI> arrayUsers;
	

	public AClientesRMI(){
		arrayUsers=new ArrayList<DadosRMI>();
	}

	public void setCliente(DadosRMI cliente){
		arrayUsers.add(cliente);
	}

	public DadosRMI getCliente(int i){
		return arrayUsers.get(i);
	}

	public int getSize(){
		return arrayUsers.size();
	}

	public DadosRMI getClienteNome(String nome){

		for(int i=0;i<arrayUsers.size();i++){
			try{
				if(arrayUsers.get(i).getUsername().compareTo(nome)==0){
					return arrayUsers.get(i);
				}
			}catch(NullPointerException e){
				arrayUsers.remove(arrayUsers.get(i));
				i--;
			}
		}

		return null;

	}

	public void remCliente(DadosRMI cliente){
		arrayUsers.remove(cliente);
	}

}