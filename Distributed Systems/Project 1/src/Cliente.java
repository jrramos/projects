import java.net.*;
import java.io.*;
import java.lang.*;
import java.util.*;

import rmi.*;

public class Cliente{

	public static void main(String[] args) throws UnknownHostException, InterruptedException{
		Socket s=null;
		ObjectInputStream dis=null;
		Sessao sessao;
		String resposta;
		Object objecto;
		int i=0;
		Mensagem mensagem,mensagem1;
		Menu menu=null;

        //argumentos =1
        if(args.length!=3){
            System.out.println("O número de dados introduzidos nao e valido");
            System.exit(-1);
        }
        int[] portas={Integer.parseInt(args[1]),Integer.parseInt(args[2])};
        int porta=1;
        sessao=new Sessao();
        while(true){
			try {
			    // 1o passo
			    porta=portas[i];
			    s = new Socket(args[0], porta);
			    s.setTcpNoDelay(true);
			    // 2o passo
			    if(!sessao.logado){
				    menu=new Menu(s,sessao);
				    System.out.println("Online no servidor "+s.getLocalPort());
				    String texto = "";
				    dis=new ObjectInputStream(s.getInputStream());
				    sessao.quebra=false;
				}else{
		        	menu.serverSocket=s;
		        	menu.dos=new ObjectOutputStream(s.getOutputStream());
		        	System.out.println("mudou de servidor");
		        	menu.dos.writeObject(99+"");
		        	menu.dos.writeObject(new Registo(sessao.username,sessao.password));
		        	dis=new ObjectInputStream(s.getInputStream());
		        	Recupera recupera=sessao.ultimaMensagem;

		        	if(recupera!=null){
		        		recupera=sessao.getMensagem();
		        		System.out.println(recupera.getMenu());

			    		menu.dos.writeObject(recupera.getMenu());
			    		if(recupera.getOpcao()!=-1)
			    			menu.dos.writeObject(recupera.getOpcao());
			    		if(sessao.ultimaMensagem.getTexto().compareTo("")!=0)
			    			menu.dos.writeObject(recupera.getTexto());
			    		if(recupera.getMensagem()!=null)
			    			menu.dos.writeObject(recupera.getMensagem());
				   }
				    if(sessao.loadingFicheiro!=null){
				    	recupera=sessao.getFicheiro();
				    	menu.criarImagem(recupera.getTexto());
				  	}
		    		
		    		sessao.quebra=false;
		
				}
			    // 3o passo
			   while(true){
					if(!sessao.logado){
					
						objecto=dis.readObject();
						sessao.put(objecto);
					}else{	
					
						mensagem=(Mensagem)dis.readObject();
						System.out.println(mensagem);
					}
				}
			       
			//mesma coisa de tentar
			} catch (UnknownHostException e) {
				sessao.quebra=true;
				i++;
		    	i%=2; 
		    	
			} catch (EOFException e) {
			    sessao.quebra=true;
			} catch (IOException e) {
				sessao.quebra=true;
				i++;
		    	i%=2; 

			} catch (ClassNotFoundException e) {
				System.out.println("Erro");
			}
		}
    }

}

class Menu extends Thread {
	ObjectOutputStream dos;

    Socket serverSocket;
	Sessao sessao;
	InputStreamReader input;
	BufferedReader reader;
	String usernameSessao;
	String menu;

	public Menu(Socket socket,Sessao sessao){
		try{
			input = new InputStreamReader(System.in);
			reader = new BufferedReader(input);
			this.sessao=sessao;
			serverSocket = socket;
            dos= new ObjectOutputStream(serverSocket.getOutputStream());
          
            this.start();
        }catch(IOException e){System.out.println("con"); System.exit(0);}
    }

    public void run(){
    	String data;
		String opcao="";
		int opcao1;
		if(!sessao.logado){
		
			try{

				do{
					System.out.println("(1)Login\n(2)Registar\n(3)Sair\n");
					opcao=reader.readLine();
					dos.writeObject(opcao);
					opcao1=Integer.parseInt(opcao);
					switch(opcao1){
					 	case 1: loginRegisto(true);break;
					 	case 2: loginRegisto(false);break;
					 	case 3: System.exit(1); break;
					}
				}while(opcao1!=3);
				System.exit(0);
			}catch(IOException e){
				System.out.println("erro");
			}
		}
	}

	public void loginRegisto(Boolean login){
		String username;
		String password;
		Boolean resposta;

		try{
			System.out.println("Insere o username");
			username=reader.readLine();
			System.out.println("Insere a password");
			password=reader.readLine();
			sessao.registo=new Registo(username,password);
			dos.writeObject(new Registo(username,password));

			resposta=(Boolean)sessao.get();

			if(login){
				if(resposta){
					sessao.logado=resposta;
					sessao.setUsername(username);
					System.out.println("\nLogin com sucesso!\n");
					resposta=(Boolean)sessao.get();
					usernameSessao=username;
					sessao.password=password;
					System.out.println("(1)Criar post imediato\n(2)Criar post agendado\n(3)Comentar Post\n(4)Editar post\n(5)Eliminar post\n(6)Criar imagem\n(7)Voltar a mostrar o menu\n(8)Voltar a mostrar mensagens\n(9)Mostrar users\n(0)Sair da area de cliente\n");
					areaUtilizador();
				}else
					System.out.println("\nLogin invalido!\n");
			}else{
				if(resposta)
					System.out.println("\nRegistado com sucesso!\n");
				else
					System.out.println("\nRegisto sem sucesso!\nMude de username\n");
				}
		}catch(IOException e){
			System.err.println("Error: " + e.getMessage());
		}
		
	}

	public void areaUtilizador() throws IOException{
		int i,opcao;
		String opcao1;

		try{
			do{
				opcao1=reader.readLine();
				opcao=Integer.parseInt(opcao1);
				menu=opcao1=""+(opcao+10);
				sessao.ultimaMensagem=new Recupera();
				sessao.ultimaMensagem.menu=opcao1;
				dos.writeObject(opcao1);
				switch (opcao) {
					case 1:criarPost(false);break;
	                case 2:criarPost(true);break;
	                case 3:comentarPost();break;
	                case 4:editarPost();break;
	                case 5:eliminarPost();break;
	                case 6:criarImagem();break;
					case 7: System.out.println("(1)Criar post imediato\n(2)Criar post agendado\n(3)Comentar Post\n(4)Editar post\n(5)Eliminar post\n(6)Criar imagem\n(7)Voltar a mostrar o menu\n(8)Voltar a mostrar mensagens\n(9)Mostrar users\n(0)Sair da area de cliente\n");break;
				}
			}while(opcao!=0);
		}catch(ClassNotFoundException e){
			System.err.println("Error: " + e.getMessage());
		}catch(IOException e){
			System.err.println("Error: " + e.getMessage());
		}

	}

	public void criarPost(Boolean agendado) throws ClassNotFoundException{
      String texto,destinatario="",dataString;
      String opcao="";
      Mensagem mensagem=null;
      int i;
      Boolean check=false;
      Date data1,data=new Date();
      long resData;
      Scanner sc;

      try{
  
        System.out.println("Insira texto:");
        texto=reader.readLine();
		do{
          System.out.println("Esta mensagem é privada?(s/n)");
          opcao=reader.readLine();
        }while(opcao.compareTo("s")!=0 && opcao.compareTo("n")!=0);

        if(opcao.compareTo("s")==0){
        	System.out.println("Insira o destinatario:");
        	destinatario=reader.readLine();
        }

       if(agendado){
	    	System.out.println("Insere a data de envio(yyyy MM dd HH mm)");
	    	dataString=reader.readLine();
	    	sc=new Scanner(dataString);
	    	sc.useDelimiter(" ");
            data1 = new GregorianCalendar(sc.nextInt(),sc.nextInt()-1,sc.nextInt(), sc.nextInt(),sc.nextInt()).getTime();
            resData=data1.getTime();
	    }else
	    	resData=data.getTime();

	   	mensagem=new Mensagem(usernameSessao,texto,destinatario,resData);

	    sessao.ultimaMensagem.setMensagem(mensagem);
	   // System.out.println(sessao.quebra);
	    if(!sessao.quebra){
			dos.writeObject(mensagem);
			sessao.ultimaMensagem=null;
		}else{
			sessao.putMensagem(sessao.ultimaMensagem);
		}
      }catch(IOException e){

        }
    
    }

    public void editarPost(){
    	int i;
    	int id=0;
    	String opcao="";
    	Mensagem mensagem=null;

    	try{
	    	opcao=reader.readLine();
	    	id=Integer.parseInt(opcao);
	    	sessao.ultimaMensagem.setOpcao(id);
	    	System.out.println("Insere o novo texto");
	    	opcao=reader.readLine();
	    	sessao.ultimaMensagem.setTexto(opcao);
	    	if(!sessao.quebra){
		    	dos.writeObject(id+"");
		    	dos.writeObject(opcao);
		    	sessao.ultimaMensagem=null;
	    	}else
	    		sessao.putMensagem(sessao.ultimaMensagem);
	    }catch(IOException e){
	    }
    }

    public void	 comentarPost() {
    	String opcao="";
    	int id=0;
    	try{
	    	opcao=reader.readLine();
	    	id=Integer.parseInt(opcao);
	    	//sessao.ultimaMensagem.setOpcao(id);
	    	System.out.println("Insere o cometario");
	    	opcao=reader.readLine();
	    	//sessao.ultimaMensagem.setMensagem(new Mensagem(usernameSessao,opcao,"",0));
	    	if(!sessao.quebra){
		    	dos.writeInt(id);
		  
		    	dos.writeObject(opcao);
		    	sessao.ultimaMensagem=null;
		    }else{
		    	sessao.putMensagem(sessao.ultimaMensagem);
		    }
	    }catch(IOException e){
	    	// sessao.ultimaMensagem=new Recupera(menu,id,new Mensagem(usernameSessao,opcao,"",0),"");
	    	// sessao.putMensagem(sessao.ultimaMensagem);
	    }
    }

    public void criarImagem(){
    	try{
	    	String dir;
	    	int porta;
	    	sessao.loadingFicheiro=new Recupera();
	    	sessao.ultimaMensagem=null;
	    	System.out.println("Insere a directoria da imagem");
	    	dir=reader.readLine();
       		sessao.loadingFicheiro.texto=dir;
       		sessao.putFicheiro(sessao.loadingFicheiro);
       		if(!sessao.quebra)
	    		new EnviarImagemClient(usernameSessao,dir,sessao);
	    }catch(IOException e){

	    }
    }

     public void criarImagem(String directoria){
     	try{
	     	sessao.loadingFicheiro=new Recupera();
	     	sessao.loadingFicheiro.texto=directoria;
       		sessao.putFicheiro(sessao.loadingFicheiro);
	     	new EnviarImagemClient(usernameSessao,directoria,sessao);
     	}catch(IOException e){
	    	sessao.putMensagem(sessao.ultimaMensagem);
	    }
     }

    public void mostrarMensagens() {
    	try{
    		dos.writeInt(20);

    	}catch(IOException e){
    		sessao.putMensagem(sessao.ultimaMensagem);
    	}

    }


    public void eliminarPost(){
    	int i;
    	Mensagem mensagem=null;
    	String opcao="";

    	try{

	    	opcao=reader.readLine();
	    	sessao.ultimaMensagem.setOpcao(Integer.parseInt(opcao));
	    	if(!sessao.quebra){
		    	dos.writeObject(opcao);
		    	sessao.ultimaMensagem=null;
		    }else
		    	sessao.putMensagem(sessao.ultimaMensagem);
	    }catch(IOException e){
	    	
	    }
    }

    public void getMenu(){
    	System.out.println("(1)Criar post imediato\n(2)Criar post agendado\n(3)Editar post\n(4)Eliminar post\n(5)Criar imagem\n(6)Eliminar imagem\n(7)Mandar mensagem privada\n(0)Sair da area de cliente\n");
    }

}

class EnviarImagemClient implements Runnable{

    Mensagem mensagem;
    long data;
    String nomeImagem;
    DataOutputStream dos;
    String username;
    int porta;
    Socket socket;
    Sessao sessao;

    EnviarImagemClient(String username,String nomeImagem,Sessao sessao) throws IOException{
    	porta=7000;
        this.dos=dos;
        this.sessao=sessao;
        this.nomeImagem=nomeImagem;
        this.username=username;
        socket=new Socket("localhost", porta);
        socket.setTcpNoDelay(true);
        dos=new DataOutputStream(socket.getOutputStream());
        Thread t = new Thread(this);
        t.start();
    }

    public void run(){
        int i;
        Date data=new Date();
        String texto;
        try{
	        File myFile = new File("Cliente/"+nomeImagem);
	        byte[] mybytearray = new byte[1024];
	        FileInputStream fis = new FileInputStream(myFile);
	        
	        
            int count;
            dos.writeUTF(username);

   			dos.flush();
            while ((count = fis.read(mybytearray)) > 0) {
                dos.write(mybytearray, 0, count);
            }
            dos.close();
            sessao.loadingFicheiro=null;
            sessao.getFicheiro();
	       texto=new String(mybytearray);
	      }catch(IOException e){System.out.println("Erro ao enviar");}

    }
}
