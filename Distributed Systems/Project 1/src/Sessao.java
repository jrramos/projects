import java.util.*;
import java.io.*;

import rmi.*;

class Sessao{
	Object objecto;
	boolean valueSet=false;
	boolean valueSet1=false;
	boolean valueSet2=false;
	int nMensagens;
	boolean logado;
	Recupera ultimaMensagem;
	String password;
	boolean quebra;
	Registo registo;
	String username;
	Recupera loadingFicheiro;

	Sessao(){
		quebra=false;
		registo=null;
	}

	synchronized Object get() {
	    while(!valueSet){
	      try {
	        wait();
	      } catch(InterruptedException e) {
	        System.out.println("interruptedException caught");
	      }
	    }
	      valueSet = false;
	      notify();
	      return objecto;
  	}

	synchronized void put(Object n) {

	    while(valueSet){
	      try {
	        wait();
	      } catch(InterruptedException e) {
	        System.out.println("interruptedException caught");
	      }
	    }

	      this.objecto = n;
	      valueSet = true;
	      notify();
	 }

	 synchronized Recupera getMensagem() {
	    while(!valueSet1){
	      try {
	        wait();
	      } catch(InterruptedException e) {
	        System.out.println("interruptedException caught");
	      }
	    }
	      valueSet = false;
	      notify();
	      return ultimaMensagem;
  	}

	synchronized void putMensagem(Recupera recupera) {

	    while(valueSet1){
	      try {
	        wait();
	      } catch(InterruptedException e) {
	        System.out.println("interruptedException caught");
	      }
	    }

	      this.ultimaMensagem = recupera;
	      valueSet1 = true;
	      notify();
	 }

	 synchronized Recupera getFicheiro() {
	    while(!valueSet2){
	      try {
	        wait();
	      } catch(InterruptedException e) {
	        System.out.println("interruptedException caught");
	      }
	    }
	      valueSet2 = false;
	      notify();
	      return ultimaMensagem;
  	}

	synchronized void putFicheiro(Recupera recupera) {

	    while(valueSet2){
	      try {
	        wait();
	      } catch(InterruptedException e) {
	        System.out.println("interruptedException caught");
	      }
	    }

	      this.ultimaMensagem = recupera;
	      valueSet2 = true;
	      notify();
	 }

	 void setUsername(String username){
	 	this.username=username;
	 }

	 String getUsername(){
	 	return username;
	 }




}