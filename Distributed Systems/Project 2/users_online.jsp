<html>
<head>
    <style type="text/css">
		#usersDiv {
	position:absolute;
	width:127px;
	height:392px;
	z-index:1;
	left: 7px;
	top: 34px;
		}
	</style>
    <script type="text/javascript">
        var websocket1;

        window.onload = function() { // execute once the page loads
            initialize1();
         
        }

        function initialize1() { // URI = ws://10.16.0.165:8080/chat/chat
            connect1('ws://' + window.location.host + '/Projeto/servlets.UsersOnlineServlet');
        }

        function connect1(host) { // connect to the host websocket servlet
            if ('WebSocket' in window)
                websocket1 = new WebSocket(host);
            else if ('MozWebSocket' in window)
                websocket1 = new MozWebSocket(host);
            else {
                writeToHistory1('Get a real browser which supports WebSocket.');
                return;
            }

            websocket1.onclose   = onClose1;
            websocket1.onmessage = onMessage1;
            websocket1.onerror   = onError1;
        }
        
        function onClose1(event) {
            writeToHistory1('WebSocket closed.');
            
        }
        
        function onMessage1(message) { // print the received message
            writeToHistory1(message.data);

        }
        
        function onError1(event) {
            writeToHistory1('WebSocket error (' + event.data + ').');
     
        }

        function writeToHistory1(text) {
            var history1 = document.getElementById('usersDiv');
            var p1 = document.createElement('p');
            p1.style.wordWrap = 'break-word';
            p1.innerHTML = text;
            while(history1.childNodes.length >= 1) {
    			history1.removeChild(history1.firstChild);
  			}
            history1.appendChild(p1);
            while (history1.childNodes.length > 25)
                history1.removeChild(console.firstChild);
            history1.scrollTop = history1.scrollHeight;
        }

    </script>
</head>
<body>
<noscript>JavaScript must be enabled for WebSockets to work.</noscript>
<div>
    <div id="usersDiv"></div>
</div>
</body>
</html>