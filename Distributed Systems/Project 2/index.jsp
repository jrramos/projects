<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@page import="servlets.User;"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>.::SocialMore::.</title>
<script src="SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<style type="text/css">
#apDiv1 {
	position:absolute;
	width:500px;
	height:160px;
	z-index:1;
	left: 13px;
	top: 113px;
}
</style>
</head>

<body>
  <h1>SocialMore</h1>
  <ul id="MenuBar1" class="MenuBarHorizontal">
  <li><a class="MenuBarItem" href="index.jsp">Login</a></li>
  <li><a class="MenuBarItem" href="index.jsp?pagina=registo">Registo</a></li>
  </ul>
  <div id="apDiv1">
  <% if(request.getParameter("nota")!=null){
       out.println(request.getParameter("nota"));
    }
    if(request.getParameter("sessao")!=null)
      session.removeAttribute("user");
    User user;
    if((user = (User)session.getAttribute("user"))!=null && user.getUserName()!=null){
%>
      <jsp:forward page="/main.jsp"></jsp:forward>
<%
    }
    if(request.getParameter("pagina")!=null){
      if(request.getParameter("pagina").compareTo("registo")==0){
  %>
<jsp:include page="registo.jsp"></jsp:include>
  <%
      }
    }else{
  %>
      <jsp:include page="login.jsp"></jsp:include>
  <%
    }
  %>
</div>
</body>
</html>