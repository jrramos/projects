package servlets;

import org.apache.catalina.websocket.WebSocketServlet;
import org.apache.catalina.websocket.MessageInbound;
import org.apache.catalina.websocket.StreamInbound;
import org.apache.catalina.websocket.WsOutbound;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicInteger;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SalasChatWS extends WebSocketServlet {

	private final Set<ChatMessageInbound> connections =
			new CopyOnWriteArraySet<ChatMessageInbound>();

	protected StreamInbound createWebSocketInbound(String subProtocol,
			HttpServletRequest request) {
		HttpSession session=request.getSession();
		return new ChatMessageInbound(((User) session.getAttribute("user")),request.getParameter("tema"));
	}

	private final class ChatMessageInbound extends MessageInbound {

		private final String nickname;
		private final String tema;

		private ChatMessageInbound(User user,String tema) {
			this.nickname = user.getUserName();
			this.tema=tema;
		}

		protected void onOpen(WsOutbound outbound) {
			connections.add(this);
			broadcast("* " + nickname + " entrou na sala");
		}

		protected void onClose(int status) {
			connections.remove(this);
			broadcast("* " + nickname + " desconectou");
		}

		protected void onTextMessage(CharBuffer message) throws IOException {
			// never trust the client
			String filteredMessage = filter(message.toString());
			broadcast("&lt;" + nickname + "&gt; "+filteredMessage);
		}

		private void broadcast(String message) { // send message to all
			for (ChatMessageInbound connection : connections) {
				try {
					if(connection.getTema().compareTo(tema)==0){
						CharBuffer buffer = CharBuffer.wrap(message);
						connection.getWsOutbound().writeTextMessage(buffer);
					}
				} catch (IOException ignore) {}
			}
		}

		public String getTema(){
			return tema;
		}

		public String filter(String message) {
			if (message == null)
				return (null);
			// filter characters that are sensitive in HTML
			char content[] = new char[message.length()];
			message.getChars(0, message.length(), content, 0);
			StringBuilder result = new StringBuilder(content.length + 50);
			for (int i = 0; i < content.length; i++) {
				switch (content[i]) {
				case '<':
					result.append("&lt;");
					break;
				case '>':
					result.append("&gt;");
					break;
				case '&':
					result.append("&amp;");
					break;
				case '"':
					result.append("&quot;");
					break;
				default:
					result.append(content[i]);
				}
			}
			return (result.toString());
		}



		protected void onBinaryMessage(ByteBuffer message) throws IOException {
			throw new UnsupportedOperationException("Binary messages not supported.");
		}
	}
}