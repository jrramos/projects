package rmi;

import java.util.*;
import java.lang.*;
import java.io.*;

public class MensagensUtilizador implements Serializable{
	ArrayList<Mensagem> aMensagens;
	int id;

	public MensagensUtilizador(){
		aMensagens=new ArrayList<Mensagem>();
	}

	public synchronized Mensagem getMensagem(int i){
		return aMensagens.get(i);
	}

	public synchronized void setMensagem(Mensagem mensagem){
		aMensagens.add(mensagem);
	}

	public synchronized int novoId(){
		id++;
		return id;
	}

	public synchronized Mensagem getMensagemId(int id){
		int i;

		for(i=0;i<aMensagens.size();i++){
			if(aMensagens.get(i).getId()==id)
				return aMensagens.get(i);
		}
		return null;
	}

	public synchronized void setId(int id){
		this.id=id;
	}	

	public synchronized int getSize(){
		return aMensagens.size();
	}

	public synchronized int getId(){
		return id;
	}

	public synchronized void remMensagem(Mensagem mensagem){
		aMensagens.remove(mensagem);
	}
} 