package servlets;

import java.util.Date;

import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.lang.String;
import org.apache.catalina.websocket.WsOutbound;
import java.net.*;
import java.io.*;
import java.lang.*;
import java.util.*;
import java.rmi.*;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.*;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;

import rmi.*;

public class User extends UnicastRemoteObject implements ChatServer
{
  
  private String _userName;
    
  private String _loginDate;

  private WsOutbound outBound;

  private ChatClient _ligacao=null;

  private String _tema;

  private int _porta;

  private String accessCode;


  public User(int i) throws RemoteException{
      super();
      
  }
  
  public User() throws RemoteException
  {
    
    _loginDate = (new Date()).toString();
    _userName="";
  }

  public void  printMensagem(Mensagem mensagem){
    try {
        CharBuffer buffer=null;
        String users;
        System.out.println(mensagem.toStringWeb());
        if(mensagem.getImagem()==null)
          users=mensagem.toStringWeb();
        else
          users=mensagem.imagens();
        buffer = CharBuffer.wrap(users);
        outBound.writeTextMessage(buffer);

    } catch (IOException ignore) {}
  }

  public void setUserName(String userName)
  {
    _userName = userName;
  }

  public void setAccessCode(String accessCode)
  {
    this.accessCode = accessCode;
  }

  public String getAccessCode()
  {
    return accessCode;
  }
    
  public String getUserName()
  {
    return _userName;
  }  

  public String getTema()
  {
    return _tema;
  }   
  
  public String getLoginDate()
  {
    return _loginDate;
  }
 
  public void setLigacao(ChatClient ligacao)
  {
    _ligacao = ligacao;
  }  

  public void setOutbound(WsOutbound outbound)
  {
    this.outBound = outbound;
  }

   public ChatClient getLigacao()
  {
    return _ligacao;
  }  

  public void setPorta(int porta)
  {
    _porta = porta;
  }  

  public int getPorta()
  {
    return _porta;
  }  

  public MensagensUtilizador getMensagens()
  {
    try{
      return _ligacao.mensagensTodas(true,_userName);
    }catch(RemoteException e){
      
    }catch(InterruptedException e){}
    return null;
  }  

  public ASalasChat getSalasP()
  {
    try{
      return _ligacao.getSalasP(_userName);
    }catch(RemoteException e){}

    return null;
  }

  public void eliminarSala(int id)
  {
    try{
      _ligacao.remSala(id);
    }catch(RemoteException e){}

  }

  public MensagensUtilizador getMensagensP()
  {
    try{
      return _ligacao.mensagensPessoais(_userName,true);
    }catch(RemoteException e){
      checkLigacao();
      getMensagensP();
    }catch(IOException e){}

    return null;
  }  

  public Boolean login(Registo registo){
    try{
      return _ligacao.login1(registo,true);
    }catch(RemoteException e){
      checkLigacao();
      login(registo);
    }catch(InterruptedException e){}
    return false;
  }

  public void eliminar(int opcao){
    try{
      _ligacao.eliminarMensagem1(opcao,_userName);
    }catch(RemoteException e){
      checkLigacao();
      eliminar(opcao);
    }
  }  

  public void novoPost(boolean agendado,Mensagem mensagem){
    try{
      _ligacao.criarPost1(agendado,mensagem);
    }catch(RemoteException e){
      checkLigacao();
      novoPost(agendado,mensagem);
    }catch(ClassNotFoundException e){}
  } 

  public void editar(int opcao,String texto){
    try{
      _ligacao.editarPost1(opcao,texto);
    }catch(RemoteException e){
      checkLigacao();
      editar(opcao,texto);
    }
  } 

  public void enviarImagem(Mensagem mensagem){
    try{
      _ligacao.enviarImagem(mensagem,_userName);
    }catch(RemoteException e){
      checkLigacao();
      enviarImagem(mensagem);
    }catch(IOException e){}
    catch(ClassNotFoundException e){}
  } 

  public Boolean registar(Registo registo){
    try{
      return _ligacao.registar1(registo,true);
    }catch(RemoteException e){
      checkLigacao();
      registar(registo);
    }catch(IOException e){}
    return false;
  } 

  public void comentar(int id,String postId,String texto,String username){
    try{
      _ligacao.comentarPost1(id,postId,texto,username);
    }catch(RemoteException e){
      System.out.println(e.getMessage());
    }catch(IOException e){}

  }  

  public boolean criarSalaChat(SalaChat sala){
    try{
      return _ligacao.criarSalaChat(sala);
    }catch(RemoteException e){
      //checkLigacao();
      //criarSalaChat(sala);
      return false;
    }catch(IOException e){}
    catch(ClassNotFoundException e){}
    return false;
  }

  public ASalasChat getSalasChat(){
    try{
      
      return _ligacao.getSalasChat();
    }catch(IOException e){
      return null;
    }
  }

  public void checkLigacao()
  {
      int[] portas={6001,6006};


        if(_porta==0)
            _porta=6001;

        while(true){
        
          try
          {

            Registry registry = LocateRegistry.getRegistry(_porta);
            _ligacao = (ChatClient) registry.lookup("chat");

            _ligacao.subscribe(new DadosRMI((ChatServer) this, _userName),accessCode);
            return;
          }
      
          catch (RemoteException e)
          {
            
            _porta%=2;
            _porta=portas[_porta];
            
          }
          catch (NotBoundException e)
          {
            
            _porta%=2;
            _porta=portas[_porta];
          }
      }

  }


}