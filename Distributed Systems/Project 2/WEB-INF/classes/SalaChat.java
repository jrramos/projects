package rmi;
import java.sql.Date;
import java.io.*;

public class SalaChat implements Serializable{
	String tema;
	String criador;
	int id;

	public SalaChat(String tema,String criador){
		this.tema=tema;
		this.criador=criador;
	}

	public String getTema(){
		return tema;
	}

	public String getCriador(){
		return criador;
	}

	public void setId(int id){
		this.id=id;
	}

	public int getId(){
		return id;
	}

	public String toString(){
		return "<b>Sala: "+tema+"</b>, criado por:"+criador;
	}
}