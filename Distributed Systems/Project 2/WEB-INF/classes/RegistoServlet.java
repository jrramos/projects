package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.lang.String;

import rmi.*;

public class RegistoServlet extends HttpServlet
{


	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
	{
		//DadosRMI clienteRMI;
		HttpSession session=request.getSession();
		User user = (User) session.getAttribute("user");

		if(user==null){
			user=new User();
			user.checkLigacao();
			session.setAttribute("user",user);
		}

		try{
	    	Boolean respRegisto;
			Registo registo=new Registo(request.getParameter("userName"),request.getParameter("passWord"));
			respRegisto=user.registar(registo);

			// String verifiedPass;
			RequestDispatcher dispatcher;
			// if ((verifiedPass = (String) _users.get(user)) != null && verifiedPass.equals(pass))
			// {
			if(respRegisto){
			    dispatcher = request.getRequestDispatcher("/index.jsp?nota=<p>Registado com sucesso</p>");
			}
			else
			{
			    dispatcher = request.getRequestDispatcher("/index.jsp?nota=<p>Erro no registo. Mude de username</p>");
			}
			
			dispatcher.forward(request, response);
		}catch(RemoteException e){}
	}

	@Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
		doGet(request, response);
    }
}