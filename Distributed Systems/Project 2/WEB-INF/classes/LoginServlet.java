package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.lang.String;

import rmi.*;

public class LoginServlet extends HttpServlet
{
    

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {

		// String user = request.getParameter("userName");
		// String pass = request.getParameter("passWord");
		//String nick = request.getParameter("nickName");
		//DadosRMI clienteRMI;
		HttpSession session=request.getSession();
		User user = (User) session.getAttribute("user");
		user=new User();
		user.setAccessCode(request.getParameter("accessCode"));
		Registo registo=new Registo(request.getParameter("userName"),request.getParameter("passWord"));
		user.setUserName(registo.getUsername());
		user.checkLigacao();
		session.setAttribute("user",user);
		try{
	    	Boolean respLogin;
			respLogin=user.login(registo);

			// String verifiedPass;
			RequestDispatcher dispatcher;
			// if ((verifiedPass = (String) _users.get(user)) != null && verifiedPass.equals(pass))
			// {
			if(respLogin){
			
			    //user.setUserName(registo.getUsername());
			    user.setAccessCode(request.getParameter("accessCode"));

				//ligacao.subscribe(clienteRMI=new DadosRMI((ChatServer) novoCliente,usernameSessao)) 
			    dispatcher = request.getRequestDispatcher("/wall.jsp");
	
			}
			else
			{
			    dispatcher = request.getRequestDispatcher("/index.jsp?nota=<p>O utilizador/password nao coincidem. Por favor, faca de novo o login ou registe-se</p>");
			}
			
			dispatcher.forward(request, response);
		}catch(RemoteException e){
			RequestDispatcher dispatcher;
			dispatcher = request.getRequestDispatcher("/erros.jsp?erro="+e.getMessage());
			dispatcher.forward(request, response);
		}
		catch(NullPointerException e){
			RequestDispatcher dispatcher;
			dispatcher = request.getRequestDispatcher("/erros.jsp?erro="+e.getMessage());
			dispatcher.forward(request, response);
		}

	}

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
	doGet(request, response);
    }

}
