package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.lang.String;

import rmi.*;

public class ComentarMensagem extends HttpServlet
{
    
   
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {

		// String user = request.getParameter("userName");
		// String pass = request.getParameter("passWord");
		//String nick = request.getParameter("nickName");

		ChatClient ligacao;
		servlets.User user;
		String nome;
		Mensagem mensagem;
    	HttpSession session=request.getSession();
    	String texto;
    	int opcao;
    	String postId;

		user = (User) session.getAttribute("user");
		
		texto=request.getParameter("texto");
		opcao=Integer.parseInt(request.getParameter("opcao"));
		postId=request.getParameter("postId");
		try{

			user.comentar(opcao,postId,texto,user.getUserName());
	
			RequestDispatcher dispatcher;

				
			dispatcher = request.getRequestDispatcher("/wall.jsp?nota=Mensagem com id:"+opcao+" comentada");
			
			dispatcher.forward(request, response);
		}catch(RemoteException e){
			RequestDispatcher dispatcher;

				
			dispatcher = request.getRequestDispatcher("/erros.jsp?erro="+e.getMessage());
			
			dispatcher.forward(request, response);
		}


	}

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
	doGet(request, response);
    }

}