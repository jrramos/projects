package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.lang.String;
import java.util.*;

import rmi.*;

public class MensagemServlet extends HttpServlet
{
    
   
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {

		// String user = request.getParameter("userName");
		// String pass = request.getParameter("passWord");
		//String nick = request.getParameter("nickName");

		ChatClient ligacao;
		servlets.User user;
		String nome;
		Mensagem mensagem;
		Boolean agendado=false;
    	HttpSession session=request.getSession();
    	Date data1,data=new Date();
    	long resData;
    	String destinatario;

		user = (User) session.getAttribute("user");
		
		if(request.getParameter("ano")!=null){
			agendado=true;
            data1 = new GregorianCalendar(Integer.parseInt(request.getParameter("ano")),Integer.parseInt(request.getParameter("mes"))-1,Integer.parseInt(request.getParameter("dia")), Integer.parseInt(request.getParameter("hora")),Integer.parseInt(request.getParameter("minuto"))).getTime();
            resData=data1.getTime();
		}else
			resData=data.getTime();

		if((destinatario=request.getParameter("destinatario"))==null){
			destinatario="";
		}

		mensagem=new Mensagem(user.getUserName(),request.getParameter("texto"),destinatario,resData);
		try{
			user.novoPost(agendado,mensagem);
	
			RequestDispatcher dispatcher;

				
			dispatcher = request.getRequestDispatcher("/wall.jsp");
			
			dispatcher.forward(request, response);
		}catch(RemoteException e){
			RequestDispatcher dispatcher;

				
			dispatcher = request.getRequestDispatcher("/erros.jsp?erro="+e.getMessage());
			
			dispatcher.forward(request, response);
		}

	}

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
	doGet(request, response);
    }

}