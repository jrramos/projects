package servlets;

import org.apache.catalina.websocket.WebSocketServlet;
import org.apache.catalina.websocket.MessageInbound;
import org.apache.catalina.websocket.StreamInbound;
import org.apache.catalina.websocket.WsOutbound;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicInteger;
import java.io.IOException;
import rmi.*;

public class UsersOnlineServlet extends WebSocketServlet {

	//private final AtomicInteger sequence = new AtomicInteger(0);
	private final Set<ChatMessageInbound> connections =
			new CopyOnWriteArraySet<ChatMessageInbound>();
	private ArrayList<String> listaUsers=new ArrayList<String>();

	protected StreamInbound createWebSocketInbound(String subProtocol,
			HttpServletRequest request) {
		HttpSession session=request.getSession();
		return new ChatMessageInbound(((User) session.getAttribute("user")).getUserName());
	}

	private final class ChatMessageInbound extends MessageInbound {

		private final String nickname;

		private ChatMessageInbound(String username) {
			this.nickname = username;
		}

		protected void onOpen(WsOutbound outbound) {
			connections.add(this);
			listaUsers.add(nickname);
			broadcast();
		}

		protected void onClose(int status) {
			connections.remove(this);
			remover(nickname);
			broadcast();
		}

		protected void onTextMessage(CharBuffer message) throws IOException {}

		private void remover(String username){
			int i;

			for(i=0;i<listaUsers.size();i++){
				if(listaUsers.get(i).compareTo(nickname)==0)
					listaUsers.remove(i);
			}
		}

		private void broadcast() { // send message to all
		
			for (ChatMessageInbound connection : connections) {
				try {
					CharBuffer buffer=null;
					String users=new String();
					for(int i=0;i<listaUsers.size();i++){
						users+=listaUsers.get(i)+"<br>";
					}
					buffer = CharBuffer.wrap(users);
					connection.getWsOutbound().writeTextMessage(buffer);
				} catch (IOException ignore) {}
			}
		}

		protected void onBinaryMessage(ByteBuffer message) throws IOException {
			throw new UnsupportedOperationException("Binary messages not supported.");
		}
	}
}