package rmi;

import java.util.*;
import java.io.*;

public class DadosRMI implements Serializable{
	ChatServer chatServer;
	String username;
	FacebookREST fb;

	public DadosRMI(ChatServer chatServer,String username){
		this.chatServer=chatServer;
		this.username=username;
	}

	public ChatServer getChatServer(){
		return chatServer;
	}

	public String getUsername(){
		return username;
	}

	public void setFB(FacebookREST fb){
		this.fb=fb;
	}

	public FacebookREST getFB(){
		return fb;
	}
}