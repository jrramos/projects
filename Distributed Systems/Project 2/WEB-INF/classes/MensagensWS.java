package servlets;

import org.apache.catalina.websocket.WebSocketServlet;
import org.apache.catalina.websocket.MessageInbound;
import org.apache.catalina.websocket.StreamInbound;
import org.apache.catalina.websocket.WsOutbound;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicInteger;
import java.io.IOException;
import rmi.*;

public class MensagensWS extends WebSocketServlet {

	private final Set<ChatMessageInbound> connections =
			new CopyOnWriteArraySet<ChatMessageInbound>();

	protected StreamInbound createWebSocketInbound(String subProtocol,
			HttpServletRequest request) {
		HttpSession session=request.getSession();
		return new ChatMessageInbound(((User) session.getAttribute("user")));
	}

	private final class ChatMessageInbound extends MessageInbound {

		private final String nickname;
		private final User user;

		private ChatMessageInbound(User user) {
			this.nickname = user.getUserName();
			this.user=user;
		}

		protected void onOpen(WsOutbound outbound) {
			connections.add(this);
			user.setOutbound(outbound);
		}

		protected void onClose(int status) {
			connections.remove(this);
	
		}

		protected void onTextMessage(CharBuffer message) throws IOException {

		}


		protected void onBinaryMessage(ByteBuffer message) throws IOException {
			throw new UnsupportedOperationException("Binary messages not supported.");
		}
	}
}