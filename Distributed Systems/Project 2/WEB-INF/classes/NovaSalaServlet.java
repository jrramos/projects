package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.lang.String;

import rmi.*;

public class NovaSalaServlet extends HttpServlet
{
    

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {


		HttpSession session=request.getSession();
		User user = (User) session.getAttribute("user");
		
		try{
	    	Boolean respSala;
			//Registo registo=new Registo(request.getParameter("userName"),request.getParameter("passWord"));
			respSala=user.criarSalaChat(new SalaChat(request.getParameter("tema"),user.getUserName()));

			// String verifiedPass;
			RequestDispatcher dispatcher;
			// if ((verifiedPass = (String) _users.get(user)) != null && verifiedPass.equals(pass))
			// {
			if(respSala){
			
				//ligacao.subscribe(clienteRMI=new DadosRMI((ChatServer) novoCliente,usernameSessao)) 
			    dispatcher = request.getRequestDispatcher("/wall.jsp?nota=<p>Nova sala inserida com sucesso</p>");
	
			}
			else
			{
			    dispatcher = request.getRequestDispatcher("/wall.jsp?nota=<p>O tema ja existe</p>");
			}
			
			dispatcher.forward(request, response);
		}catch(RemoteException e){
			RequestDispatcher dispatcher;
			dispatcher = request.getRequestDispatcher("/erros.jsp?erro="+e.getMessage());
			dispatcher.forward(request, response);
		}
		catch(NullPointerException e){
			RequestDispatcher dispatcher;
			dispatcher = request.getRequestDispatcher("/erros.jsp?erro="+e.getMessage());
			dispatcher.forward(request, response);
		}

	}

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
	doGet(request, response);
    }

}
