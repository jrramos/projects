package servlets;

import java.io.File;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.lang.String;
import java.util.*;

import rmi.*;

// A anotation MultipartConfig permite configurar onde colocar o ficheiro, e o tamanho maximo
// de Upload.

@SuppressWarnings("serial")


@MultipartConfig(location = "/Users/josericardoramos/Faculdade/3Ano/1Semestre/SD/Projecto1/src/Server", maxFileSize = 10485760L) // 10MB.

@WebServlet(urlPatterns = { "/FileUploadServlet" })
public class FileUploadServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        request.getRequestDispatcher("/upload.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException
    {
        MultipartMap map = new MultipartMap(request, this); // Processa os ficheiros de upload
        String text = map.getParameter("name");
        File file = map.getFile("file"); // Devolve o ficheiro depois de feito o upload.
        HttpSession session=request.getSession();
        servlets.User user;
        Date data=new Date();
        long resData;

        resData=data.getTime();
        // Now do your thing with the obtained input.
        user = (User) session.getAttribute("user");

        Mensagem mensagem=new Mensagem(user.getUserName(),"Server/"+(file.toString()).substring((file.toString()).lastIndexOf('/')+1),resData);
        // System.out.println("Text: " + text);
        // System.out.println("File: " + file);
        try{
            user.enviarImagem(mensagem);
    
            RequestDispatcher dispatcher;

                
            dispatcher = request.getRequestDispatcher("/wall.jsp");
            
            dispatcher.forward(request, response);
        }catch(RemoteException e){
            RequestDispatcher dispatcher;

                
            dispatcher = request.getRequestDispatcher("/erros.jsp?erro="+e.getMessage());
            
            dispatcher.forward(request, response);
        }
    }

}