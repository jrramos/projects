<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@page import="java.util.*" %>
<jsp:include page="main.jsp"></jsp:include>
<html>
<head>
	<style type="text/css">
    #wall {
        position:absolute;
        width:651px;
        height:392px;
        z-index:1;
    }

    </style>
    <script type="text/javascript">

        var websocket;

        window.onload = function() { 

            <%
			servlets.User user = (servlets.User) session.getAttribute("user");
			rmi.MensagensUtilizador mensagens = user.getMensagens();
                
			for (int i=0;i<mensagens.getSize();i++)
			{

				if(mensagens.getMensagem(i).getImagem()!=null){

			%>
					writeImagens('<%=mensagens.getMensagem(i).getUsername()%>','<%=mensagens.getMensagem(i).getImagem()%>');
			<%
		
				}else{

                    if((mensagens.getMensagem(i).getIdPost())!=null){      
			%>
					   writeToHistory("<b><a href='comentarMensagem.jsp?id=<%=mensagens.getMensagem(i).getId()%>&postId=<%=mensagens.getMensagem(i).getIdPost()%>' > <%= mensagens.getMensagem(i).getUsername() %> </a></b> <%= mensagens.getMensagem(i).getDestinatario() %><br><%= mensagens.getMensagem(i).getTexto() %><br><%= mensagens.getMensagem(i).getComentarios() %>");
			<%
                    }else{
            %>
                        writeToHistory("<b><a href='comentarMensagem.jsp?id=<%=mensagens.getMensagem(i).getId()%>' > <%= mensagens.getMensagem(i).getUsername() %> </a></b> <%= mensagens.getMensagem(i).getDestinatario() %><br><%= mensagens.getMensagem(i).getTexto() %><br><%= mensagens.getMensagem(i).getComentarios() %>");

            <%
                    }
				}
			}
        
		%>


            initialize1();
			initialize();
            
        }

        function initialize() { // URI = ws://10.16.0.165:8080/chat/chat
            connect('ws://' + window.location.host + '/Projeto/servlets.MensagensWS');
        }

        function connect(host) { // connect to the host websocket servlet
            if ('WebSocket' in window)
                websocket = new WebSocket(host);
            else if ('MozWebSocket' in window)
                websocket = new MozWebSocket(host);
            else {
                writeToHistory('Get a real browser which supports WebSocket.');
                return;
            }

            websocket.onopen    = onOpen; // set the event listeners below
            websocket.onclose   = onClose;
            websocket.onmessage = onMessage;
            websocket.onerror   = onError;


        }

        function onOpen(event) {
            
        }
        
        function onClose(event) {
            writeToHistory('WebSocket closed.');
            
        }
        
        function onMessage(message) { // print the received message
            writeToHistory(message.data);
            document.getElementById('chat').onkeydown = null;
        }
        
        function onError(event) {
            writeToHistory('WebSocket error (' + event.data + ').');
            document.getElementById('chat').onkeydown = null;
            
        }
        
        function doSend() {
            var message = document.getElementById('chat').value;
            document.getElementById('chat').value = '';
            if (message != '')
                websocket.send(message); // send the message
        }

        function writeImagens(user,imagem){
        	var history = document.getElementById('wall');
            var p = document.createElement('p');
            p.style.wordWrap = 'break-word';
            p.innerHTML = '<b>'+user+'</b>:<br>'+"<img  src='data:image/jpg;base64,"+imagem+"' alt='IMG DESC'>";
            history.appendChild(p);
            while (history.childNodes.length > 25)
                history.removeChild(console.firstChild);
            history.scrollTop = history.scrollHeight;
        }

        function writeToHistory(mensagem) {
            var history = document.getElementById('wall');
            var p = document.createElement('p');
            p.style.wordWrap = 'break-word';
            p.innerHTML = mensagem;
            history.appendChild(p);
            while (history.childNodes.length > 25)
                history.removeChild(console.firstChild);
            history.scrollTop = history.scrollHeight;
        }

    </script>
</head>
<body>

<br>
<div id="wall">

</div>
</form>
</body>
</html>