<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@page import="java.util.*" %>
<jsp:include page="main.jsp"></jsp:include>
<html>
<head>

    <link rel="stylesheet" type="text/css" href="style.css">
    <script type="text/javascript">

        var websocket;
        var tema = getUrlVars()["tema"];

        window.onload = function() { // execute once the page loads
            initialize();
            initialize1();
            document.getElementById("chat").focus();
        }

        function getUrlVars() {
            var vars = {};
            var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
                vars[key] = value;
            });
            return vars;
        }


        function initialize() { // URI = ws://10.16.0.165:8080/chat/chat
            connect('ws://' + window.location.host + '/Projeto/servlets.SalasChatWS?tema='+tema);
        }

        function connect(host) { // connect to the host websocket servlet
            if ('WebSocket' in window)
                websocket = new WebSocket(host);
            else if ('MozWebSocket' in window)
                websocket = new MozWebSocket(host);
            else {
                writeToHistory('Get a real browser which supports WebSocket.');
                return;
            }

            websocket.onopen    = onOpen; // set the event listeners below
            websocket.onclose   = onClose;
            websocket.onmessage = onMessage;
            websocket.onerror   = onError;
        }

        function onOpen(event) {
            writeToHistory('Connected to ' + window.location.host + '.');
            document.getElementById('chat').onkeydown = function(key) {
                if (key.keyCode == 13)
                    doSend(); // call doSend() on enter key
            };
        }
        
        function onClose(event) {
            writeToHistory('WebSocket closed.');
            document.getElementById('chat').onkeydown = null;
        }
        
        function onMessage(message) { // print the received message
            writeToHistory(message.data);
        }
        
        function onError(event) {
            writeToHistory('WebSocket error (' + event.data + ').');
            document.getElementById('chat').onkeydown = null;
        }
        
        function doSend() {
            var message = document.getElementById('chat').value;
            document.getElementById('chat').value = '';
            if (message != '')
                websocket.send(message); // send the message
        }

        function writeToHistory(text) {
            var history = document.getElementById('history');
            var p = document.createElement('p');
            p.style.wordWrap = 'break-word';
            p.innerHTML = text;
            history.appendChild(p);
            while (history.childNodes.length > 25)
                history.removeChild(console.firstChild);
            history.scrollTop = history.scrollHeight;
        }

    </script>
</head>
<body>
<h3>Sala de chat : <%= request.getParameter("tema") %></h3>
<div>
    <div id="container"> <div id="history"></div> </div>
    <p> <input type="text" placeholder="type to chat" id="chat"> </p>
</div>
</body>
</html>