<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@page import="servlets.User;"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <style type="text/css">
    #usersonline1 {
        position:absolute;
        width:127px;
        height:392px;
        z-index:1;
        left: 1000px;
        top: 145px;
    }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>.::SocialMore::.</title>
    <script src="SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
    <link href="SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />

  </head>

  <body>
    <h1>SocialMore</h1>
    <jsp:include page="jsp/auth_verification.jsp"></jsp:include>
    <p></p>
    <ul id="MenuBar1" class="MenuBarHorizontal">
      <li><a href="wall.jsp">Mensagens</a></li>
      <li><a class="MenuBarItemSubmenu" href="#">Mensagem Pub</a>
        <ul>
          <li><a href="imediata.jsp">Imediata</a></li>
          <li><a href="agendada.jsp">Agendada</a></li>
        </ul>
      </li>
      <li><a href="privada.jsp">Mensagem Privada</a></li>
      <li><a href="upload.jsp">Enviar Imagem</a></li>
      <li><a class="MenuBarItemSubmenu" href="#">Gerir Mensagens</a>
        <ul>
          <li><a href="editar.jsp">Editar</a></li>
          <li><a href="apagar.jsp">Apagar</a></li>
        </ul>
      </li>
      <li><a class="MenuBarItemSubmenu" href="#">Salas Chat</a>
        <ul>
          <li><a href="criarSalaChat.jsp">Criar</a></li>
          <li><a href="entrarSalasChat.jsp">Entrar</a></li>
          <li><a href="apagarSala.jsp">Apagar</a></li>
        </ul>
      </li>

    </ul>
    <script type="text/javascript">
      var MenuBar1 = new Spry.Widget.MenuBar("MenuBar1", {imgDown:"SpryAssets/SpryMenuBarDownHover.gif", imgRight:"SpryAssets/SpryMenuBarRightHover.gif"});
    </script>
    <p><br /><br />
    <%
      if(request.getParameter("nota")!=null)
        out.println("<b>"+request.getParameter("nota")+"</b><br>");
    %>
    <br /><br />
    <div id="usersonline1"><h3>Users Online</h3><jsp:include page="users_online.jsp"></jsp:include></div>
  </body>
</html>