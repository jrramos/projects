package tdb_test;

import info_extraction.DescriptionExtraction;

import java.util.ArrayList;
import java.util.Iterator;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ReadWrite;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.tdb.TDBFactory;

public class Test {
	Dataset dataset;
	String nameSpace;
	String ns;
	String directory ="/Users/josericardoramos/Faculdade/4Ano/2Semestre/SIGC/Projeto/TDBDatabase";
	
	public static void main(String[] args) {
	    	new Test();
	}
	 
    public Test(){
		ns="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
		nameSpace="http://www.owl-ontologies.com/creationOwl#";
		dataset = TDBFactory.createDataset(directory) ;
		dataset.begin(ReadWrite.READ) ;
		
		getRecomendation(28);
    }
    
	public void getRecomendation(int id){
		
	   String queryString ="prefix nameSpace: <"+nameSpace+"> "
		 		+"prefix ns: <"+ns+"> "
		 		+"SELECT DISTINCT ?name ?type WHERE {{?x nameSpace:hasName ?name . FILTER (regex(?name,\"(^|((.)* ))toy($| ((.)*))\",'i'))} UNION {?x nameSpace:hasName ?name . ?x nameSpace:hasTopic ?topic . FILTER (regex(?topic,\"(^|((.)* ))marji($| ((.)*))\",'i'))}"
	 				+ ". ?x ns:type ?type}";
	    
	    QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset) ;
	    ResultSet rs = qExec.execSelect() ;

	    while(rs.hasNext()){
	    	int res=0;
			QuerySolution row= rs.next();
			Iterator columns=row.varNames();

			while(columns.hasNext()){
				String name;
				RDFNode cell = row.get((name=columns.next().toString()));
				
				if(name.compareTo("name")==0){
					String campoAux=cell.asLiteral().toString();
		    	    System.out.println(campoAux.substring(0,campoAux.indexOf('^')));
	    	    }else if(name.compareTo("type")==0){
	    	    	Node campoAux=cell.asNode();
		    		name=campoAux.getURI();

	    	    	System.out.println(name);
	    	    }

	    	 
    		}
    	    
			     
				 
		} 

	}
}
