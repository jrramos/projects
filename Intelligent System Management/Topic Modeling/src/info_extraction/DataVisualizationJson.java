package info_extraction;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import info_extraction.entities.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class DataVisualizationJson {
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("SIGC");
	EntityManager em = emf.createEntityManager();
	List<Genre> genresList;
	List<Movie>	moviesList;
	
	public static void main(String[] args) {
    	new DataVisualizationJson();
	}
	
	public DataVisualizationJson(){
		findGenres();
		findMovies();
		
		String content=makeJson();
		
		saveToFile(content);
	}
	
	public void findGenres(){
		 String queryGenreNames="SELECT s FROM Genre s";
		 
		 Query query=(Query) em.createQuery(queryGenreNames,Genre.class);
		 
		 genresList=query.getResultList();
	 }
	
	public void findMovies(){
		 String queryGenreNames="SELECT s FROM Movie s";
		 
		 Query query=(Query) em.createQuery(queryGenreNames,Movie.class);
		 
		 moviesList=query.getResultList();
	 }
	
	public String makeJson(){
		ArrayList<Topic> topics;
		String content="{\n \"name\": \"\", \"children\": [ ";
		
		for(Genre genre:genresList){
			content+="{\n \"name\": \""+genre.getName()+"\",\n \"children\": [";
			topics=getTopicWords(genre);
			int topicNumber=1;
			
			for(Topic topic:topics){
				content+="{\n \"name\": \"Topic "+topicNumber+"\",\n \"children\": [\n";
				for(int word=0;word<topic.words.size();word++){
					content+="{\n \"name\": \""+topic.words.get(word)+"\",\"size\": 12300\n },";
				}
				content=content.substring(0, content.length()-1);
				content+="]},";
				topicNumber++;
			}
			content=content.substring(0, content.length()-1);
			
			content+="]},";
		}
		
		content=content.substring(0, content.length()-1);
		content+=" ]}";
	
		
		return content;
	}
	
	public ArrayList<Topic> getTopicWords(Genre genre){
		List<Movie> movies=checkMovies(genre);
		
		return (new TopicModellingVisualization(movies)).topicsTerms;
	}
	
	public List<Movie> checkMovies(Genre genre){
		List<Movie> movies=new ArrayList<Movie>();
		
		for(Movie movie:moviesList){
			if(gotGenre(movie,genre))
				movies.add(movie);
		}
		
		return movies;
	}
	
	public boolean gotGenre(Movie movie,Genre genre){
		
		for(int i=0;i<movie.getGenresSize();i++){
			if(genre.getName().compareTo(movie.getGenre(i).getName())==0){
				return true;
			}
		}
		
		return false;
	}
	
	public void saveToFile(String content){
	
	        String folder="/Users/josericardoramos/Faculdade/4Ano/2Semestre/SIGC/teste/D3JS/web/area.json";
	        
	        
	        try
	        {
	            FileWriter writer = new FileWriter(folder);
	            
	            writer.append(content);
	            writer.flush();
	            writer.close();
	        }catch(IOException e)
	        {
	             e.printStackTrace();
	        } 
	    
	}
}
