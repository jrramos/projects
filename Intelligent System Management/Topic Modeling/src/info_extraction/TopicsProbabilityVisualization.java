package info_extraction;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import info_extraction.entities.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class TopicsProbabilityVisualization {
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("SIGC");
	EntityManager em = emf.createEntityManager();
	List<Genre> genresList;
	List<Movie>	moviesList;
	int[] topicFrequency;
	ArrayList<Topic> topicsTerms;
	String contentTopicWords;
	
	public static void main(String[] args) {
    	new TopicsProbabilityVisualization();
	}
	
	public TopicsProbabilityVisualization(){
		findGenres();
		findMovies();
		contentTopicWords="";
		String content=makeJson();

		saveToFile(content);
	}
	
	public void findGenres(){
		 String queryGenreNames="SELECT s FROM Genre s";
		 
		 Query query=(Query) em.createQuery(queryGenreNames,Genre.class);
		 
		 genresList=query.getResultList();
	 }
	
	public void findMovies(){
		 String queryGenreNames="SELECT s FROM Movie s";
		 
		 Query query=(Query) em.createQuery(queryGenreNames,Movie.class);
		 
		 moviesList=query.getResultList();
	 }
	
	public String makeJson(){
		int[] topics;
		String headerGraph="age,population\n";
		String contentGraph;
		String contentBubbles="{\n \"name\": \"\", \"children\": [ ";
		
		for(Genre genre:genresList){
			contentTopicWords="";
			int topicNumber=1;
			contentBubbles+="{\n \"name\": \""+genre.getName()+"\",\n \"children\": [";
			contentGraph=headerGraph;
			getTopicWords(genre);
			
			for(int topic=0;topic<20;topic++){
				contentGraph+="Topic"+topic+","+topicFrequency[topic]+"\n";
			}
			
			saveToFile(contentGraph,genre.getName().replace(" ","_"));
			

			
			for(Topic topic:topicsTerms){
				contentBubbles+="{\n \"name\": \"Topic "+topicNumber+"\",\n \"children\": [\n";
				for(int word=0;word<topic.words.size();word++){
					contentBubbles+="{\n \"name\": \""+topic.words.get(word)+"\",\"size\": 12300\n },";
					contentTopicWords+=topic.words.get(word)+",";
				}
				contentTopicWords=contentTopicWords.substring(0, contentTopicWords.length()-1);
				contentTopicWords+="\n";
				
				contentBubbles=contentBubbles.substring(0, contentBubbles.length()-1);
				contentBubbles+="]},";
				topicNumber++;
			}
			
			saveTopicWords(genre.getName(),contentTopicWords);
			
			contentBubbles=contentBubbles.substring(0, contentBubbles.length()-1);
			
			contentBubbles+="]},";
			
		}
		
		contentBubbles=contentBubbles.substring(0, contentBubbles.length()-1);
		contentBubbles+=" ]}";
	
		
		return contentBubbles;
	
	}
	
	public void getTopicWords(Genre genre){
		List<Movie> movies=checkMovies(genre);
		TopicsDistribution topicDistribution=new TopicsDistribution(movies);
		
		topicsTerms=topicDistribution.topicsTerms;
		topicFrequency=topicDistribution.topicFrequency;
	}
	
	public List<Movie> checkMovies(Genre genre){
		List<Movie> movies=new ArrayList<Movie>();
		
		for(Movie movie:moviesList){
			if(gotGenre(movie,genre))
				movies.add(movie);
		}
		
		return movies;
	}
	
	public boolean gotGenre(Movie movie,Genre genre){
		
		for(int i=0;i<movie.getGenresSize();i++){
			if(genre.getName().compareTo(movie.getGenre(i).getName())==0){
				return true;
			}
		}
		
		return false;
	}
	
	public void saveToFile(String content,String name){
	
	        String folder="/Users/josericardoramos/Faculdade/4Ano/2Semestre/SIGC/teste/chart/"+name+".csv";
	        
	        
	        try
	        {
	            FileWriter writer = new FileWriter(folder);
	            
	            writer.append(content);
	            writer.flush();
	            writer.close();
	        }catch(IOException e)
	        {
	             e.printStackTrace();
	        } 
	    
	}
	
	public void saveToFile(String content){
		
        String folder="/Users/josericardoramos/Faculdade/4Ano/2Semestre/SIGC/teste/all_topics/web/area.json";
        
        
        try
        {
            FileWriter writer = new FileWriter(folder);
            
            writer.append(content);
            writer.flush();
            writer.close();
        }catch(IOException e)
        {
             e.printStackTrace();
        } 
    
	}
	
	public void saveTopicWords(String genre,String content){
		
        String folder="/usr/local/apache-tomcat-7.0.32/webapps/SiteSIGC/words_by_topic/"+genre+".txt";
        
        
        try
        {
            FileWriter writer = new FileWriter(folder);
            
            writer.append(content);
            writer.flush();
            writer.close();
        }catch(IOException e)
        {
             e.printStackTrace();
        } 
    
	}
}
