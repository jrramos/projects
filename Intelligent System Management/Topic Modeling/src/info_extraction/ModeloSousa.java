package info_extraction;

import java.util.List;

import info_extraction.entities.Movie;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class ModeloSousa {
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("SIGC");
	EntityManager em = emf.createEntityManager();
	List<Movie>	moviesList;
	
	public void findMovies(){
		 String queryGenreNames="SELECT s FROM Movie s";
		 
		 Query query=(Query) em.createQuery(queryGenreNames,Movie.class);
		 
		 moviesList=query.getResultList();
	 }
	
	public static void main(String[] args) {
    	new ModeloSousa();
	}
	
	public ModeloSousa(){
		findMovies();
		
		new ModeloSousaTopic(moviesList);
	}
}
