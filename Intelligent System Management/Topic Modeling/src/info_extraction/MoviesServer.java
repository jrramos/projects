package info_extraction;

import java.io.*;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.net.URL;
import java.nio.charset.Charset;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.json.JSONException;

import info_extraction.entities.*;
import info_extraction.entities.Character;

public class MoviesServer{

	EntityManagerFactory emf = Persistence .createEntityManagerFactory("SIGC");
	EntityManager em = emf.createEntityManager();

    public static void main(String[] args) {
    	new MoviesServer();
    }

    MoviesServer(){
    	JSONObject jsonObjectMovieDB=null,jsonObjectRootenTomatoes=null,jsonReviewsRootenTomatoes=null,jsonObjectMovieDBCasts=null;
    	int id=36,a,totalReviews;
    	int sucess=0;
    	String title="",description;
    	JSONArray arrayGenres;
    	Double rank;
    	int year;
    	JSONObject idsObject;
    	String imdbID;
    	JSONObject aux;
    	Movie movie;
    	Review review;
    	String movieName,movieNameURL;
    	//int newChar,newActor,newJob,newCrew,newGenre,newCompany,newLanguage,newCountry;
    	
    	while(sucess<1100){
    		System.out.println("id "+id);
	 		try {
	 				People people=null;
	 				Character caracter=null;
	 				Cast cast=null;
	 				Genre novoGenero=null;
	 				Job job=null;
	 				Language language=null;
	 				Crew crew=null;
	 				Cast actor=null;
	 				
	 				InputStream isMovieDB = new URL("http://api.themoviedb.org/3/movie/"+id+"?api_key=6dfc1f26ede76fb7a23736bedbc69e26").openStream();
			      	BufferedReader rd = new BufferedReader(new InputStreamReader(isMovieDB, Charset.forName("UTF-8")));
			     	String jsonText = readAll(rd);
			     	jsonObjectMovieDB = new JSONObject(jsonText);
			     	isMovieDB.close();
			      	
			      	movieName=jsonObjectMovieDB.getString("original_title");
			      	movieNameURL=movieName.replaceAll(" ","+");
			      	//System.out.println("nome:"+movieNameURL);
			      	
			      	InputStream isRottenTomatoes = new URL("http://api.rottentomatoes.com/api/public/v1.0/movies.json?apikey=ev3qdev39ttrjea2883vzjbk&q="+movieNameURL+"&page_limit=1").openStream();
			      	rd = new BufferedReader(new InputStreamReader(isRottenTomatoes, Charset.forName("UTF-8")));
			     	jsonText = readAll(rd);
			     	jsonObjectRootenTomatoes = new JSONObject(jsonText);
			     	isRottenTomatoes.close();
			     	
			     	InputStream isMovieDBCasts = new URL("http://api.themoviedb.org/3/movie/"+id+"/casts?api_key=6dfc1f26ede76fb7a23736bedbc69e26").openStream();
			      	rd = new BufferedReader(new InputStreamReader(isMovieDBCasts, Charset.forName("UTF-8")));
			     	jsonText = readAll(rd);
			     	jsonObjectMovieDBCasts = new JSONObject(jsonText);
			     	isMovieDBCasts.close();
			     	
			     	String linkToReviews;
			     	
			     	
			      	if(jsonObjectRootenTomatoes.getJSONArray("movies").length()!=1){
			      		id++;
			      		System.out.println("Falhou Total busca:"+jsonObjectRootenTomatoes.getInt("total"));
			      		continue;
			      	}
			      	
			      	
		      		JSONArray rottenTomatoesArray=jsonObjectRootenTomatoes.getJSONArray("movies");
		      		JSONObject movieRottenTomatoes=(JSONObject)rottenTomatoesArray.get(0);
		      		JSONObject reviewsArrayLink=movieRottenTomatoes.getJSONObject("links");
		      		linkToReviews=reviewsArrayLink.getString("reviews");
			      	

			      	InputStream isRottenTomatoesReviews = new URL(linkToReviews+"?apikey=ev3qdev39ttrjea2883vzjbk").openStream();
			      	rd = new BufferedReader(new InputStreamReader(isRottenTomatoesReviews, Charset.forName("UTF-8")));
			     	jsonText = readAll(rd);
			     	jsonReviewsRootenTomatoes = new JSONObject(jsonText);
			     	isRottenTomatoesReviews.close();
			      	
			     	
					if(jsonReviewsRootenTomatoes.getInt("total")<1){
						id++;
						//System.out.println("Falhou Total reviews:"+jsonReviewsRootenTomatoes.getInt("total"));
			      		continue;
					}
					
					
					arrayGenres = jsonObjectMovieDB.getJSONArray("genres");

		 			if(arrayGenres.length()==0){
		 				id++;
		 				//System.out.println("Falhou Total genres:"+arrayGenres.length());
		 				continue;
		 			}
					
					title=jsonObjectMovieDB.getString("title");
					description=jsonObjectMovieDB.getString("overview");
					rank=jsonObjectMovieDB.getDouble("vote_average");
					rank=jsonObjectMovieDB.getDouble("vote_average");
					year=Integer.parseInt(jsonObjectMovieDB.getString("release_date").split("-")[0]);
							
					movie=new Movie(id,title,rank,description,year);
					
					JSONArray languagesArray=jsonObjectMovieDB.getJSONArray("spoken_languages");
					
					if(languagesArray.length()==0){
						id++;
						//System.out.println("Falhou languages:"+languagesArray.length());
						continue;
					}
					
					for(int i=0;i<languagesArray.length();i++){
						if(((JSONObject)languagesArray.get(i)).getString("name").compareTo("")!=0 && ((JSONObject)languagesArray.get(i)).getString("name").compareTo("''")!=0){
							if((language=findLanguage(((JSONObject)languagesArray.get(i)).getString("name")))==null){
				 				language=new Language(((JSONObject)languagesArray.get(i)).getString("name"));
				 				EntityTransaction tx = em.getTransaction();
				 				tx.begin();
				 				em.persist(language);
				 				tx.commit();
				 			}
							//System.out.println((language==null)+" "+(movie==null));
				 			movie.setLanguage(language);
						}
					}
					
					if(movie.getLanguageSize()==0){
						id++;
						//System.out.println("Falhou languages:"+languagesArray.length());
						continue;
					}
					
					JSONArray reviewsArray=jsonReviewsRootenTomatoes.getJSONArray("reviews");
					
					for(int i=0;i<reviewsArray.length();i++){
						JSONObject jsonReview=(JSONObject)reviewsArray.get(i);
						
						if(!jsonReview.has("original_score"))
							continue;
						
						String criticAuthor=jsonReview.getString("critic");
						String criticReview=jsonReview.getString("quote");
						String rankReview=jsonReview.getString("original_score");
						
						
						if(criticReview.compareTo("")==0){
				      		continue;
						}
						
						review=new Review();
						
						review.setComment(criticReview);
						review.setAuthor(criticAuthor);	
						review.setRank(rankReview);
						
						EntityTransaction tx = em.getTransaction();
						tx.begin();
		 				em.persist(review);
		 				tx.commit();
						
		 				movie.setReview(review);
					}
				
					if(movie.getReviewsSize()==0){
						id++;
						//System.out.println("Falhou Reviews:"+movie.getReviewsSize());
						continue;
					}

					
					
			 		for(int i=0;i<arrayGenres.length();i++){
						 if((novoGenero=findGenre(((JSONObject)arrayGenres.get(i)).getInt("id")))==null){
			 				novoGenero=new Genre(((JSONObject)arrayGenres.get(i)).getInt("id"),((JSONObject)arrayGenres.get(i)).getString("name"));
			 				EntityTransaction tx = em.getTransaction();
			 				tx.begin();
			 				em.persist(novoGenero);
			 				tx.commit();
			 			}
			 			movie.setGenre(novoGenero);
			 		}
			 		
			 		JSONArray castArray=jsonObjectMovieDBCasts.getJSONArray("cast");

			 		for(int i=0;i<castArray.length();i++){
			 			
			 			if((people=findPeople(((JSONObject)castArray.get(i)).getInt("id")))==null){
			 				people=new People(((JSONObject)castArray.get(i)).getInt("id"),((JSONObject)castArray.get(i)).getString("name"));
			 				EntityTransaction tx = em.getTransaction();
			 				tx.begin();
			 				em.persist(people);
			 				tx.commit();
			 			}
			 			
			 			if((caracter=findCharacter(((JSONObject)castArray.get(i)).getString("character")))==null){
			 				caracter=new Character(((JSONObject)castArray.get(i)).getString("character"));
			 				EntityTransaction tx = em.getTransaction();
			 				tx.begin();
			 				//System.out.println(((JSONObject)castArray.get(i)).getString("character"));
			 				em.persist(caracter);
			 				tx.commit();
			 			}
			 		
			 			if((actor=findActor(caracter,people))==null){
			 				actor=new Cast(people,caracter);
			 				EntityTransaction tx = em.getTransaction();
			 				tx.begin();
			 				em.persist(actor);
			 				tx.commit();
			 			}
			 			movie.setActor(actor);
			 		
			 		}

			 		JSONArray crewtArray=jsonObjectMovieDBCasts.getJSONArray("crew");

			 		for(int i=0;i<crewtArray.length();i++){
			 			if((people=findPeople(((JSONObject)crewtArray.get(i)).getInt("id")))==null){
			 				people=new People(((JSONObject)crewtArray.get(i)).getInt("id"),((JSONObject)crewtArray.get(i)).getString("name"));
			 				EntityTransaction tx = em.getTransaction();
			 				tx.begin();
			 				em.persist(people);
			 				tx.commit();
			 			}


			 			if((job=findJob(((JSONObject)crewtArray.get(i)).getString("job")))==null){
		
			 				job= new Job(((JSONObject)crewtArray.get(i)).getString("job"));
			 				EntityTransaction tx = em.getTransaction();
			 				tx.begin();
			 				em.persist(job);
			 				tx.commit();
			 			}
			 			
			 			if((crew=findCrew(job,people))==null){
			 				crew=new Crew(job,people);
			 				EntityTransaction tx = em.getTransaction();
			 				tx.begin();
			 				em.persist(crew);
			 				tx.commit();
			 			}
			 				
			 			movie.setCrew(crew);
			 		}
			 		
		 			id++;
		 			sucess++;
		 			
		 			EntityTransaction tx = em.getTransaction();
	 				tx.begin();
			 		em.persist(movie);
			 		tx.commit();
			 		
			} catch (FileNotFoundException w) {
				System.out.println("FileNotFoundException");
				w.printStackTrace();
				id++;
			} catch (IOException w) {
				System.out.println("IOException");
				w.printStackTrace();
				id++;
			}catch (JSONException e){
				System.out.println("JSONException");
				e.printStackTrace();
			}
		}
    }

    String readAll(Reader rd) throws IOException {
	    StringBuilder sb = new StringBuilder();
	    int cp;
	    while ((cp = rd.read()) != -1) {
	      sb.append((char) cp);
	    }
	    return sb.toString();
 	}
   
 
  public Genre findGenre(int id){
	  	
		String query="SELECT s FROM Genre s WHERE s.id=:id";
	   	Query query1=(Query) em.createQuery(query,Genre.class);
	    query1.setParameter("id",id);

	    List<Genre> lista=query1.getResultList();
	   	if(lista.size()>0){
	   		return lista.get(0);
	   	}else
	   		return null;

}

  
  public Character findCharacter(String name){
  	
  	String query="SELECT s FROM Character s WHERE s.name=:name";
	   	Query query1=(Query) em.createQuery(query,Character.class);
	    query1.setParameter("name",name);

	    List<Character> lista=query1.getResultList();
	   	if(lista.size()>0){
	   		return lista.get(0);
	   	}else
	   		return null;
      
  }
  
  public Cast findActor(Character character,People people){
  	String query="SELECT s FROM Cast s JOIN s.character c  JOIN s.people p WHERE c.id=:character AND p.id=:people";
	   	Query query1=(Query) em.createQuery(query,Cast.class);
	    query1.setParameter("character",character.getId());
	    query1.setParameter("people",people.getId());
	    List<Cast> lista=query1.getResultList();
	   	if(lista.size()>0)
	   		return lista.get(0);
	   	else
	   		return null;
  }
  
  public Crew findCrew(Job job,People people){
  	String query="SELECT s FROM Crew s JOIN s.people p JOIN s.job j WHERE p.id=:people AND j.id=:job";
	   	Query query1=(Query) em.createQuery(query,Crew.class);
	    query1.setParameter("people",people.getId());
	    query1.setParameter("job",job.getId());
	    List<Crew> lista=query1.getResultList();
	   	if(lista.size()>0)
	   		return lista.get(0);
	   	else
	   		return null;
  }
  
  public People findPeople(int id){
  	
  	String query="SELECT s FROM People s WHERE s.id=:id";
	   	Query query1=(Query) em.createQuery(query,People.class);
	    query1.setParameter("id",id);
	    List<People> lista=query1.getResultList();
	   	if(lista.size()>0){
	   		return lista.get(0);
	   	}else
	   		return null;
      
  }
  
  public Language findLanguage(String name){
	  	
		String query="SELECT s FROM Language s WHERE s.name=:name";
	   	Query query1=(Query) em.createQuery(query,Language.class);
	    query1.setParameter("name",name);

	    List<Language> lista=query1.getResultList();
	   	if(lista.size()>0){
	   		return lista.get(0);
	   	}else
	   		return null;
    
}
  
public Job findJob(String job){
  	
  	String query="SELECT s FROM Job s WHERE s.name=:name";
	   	Query query1=(Query) em.createQuery(query,Job.class);
	    query1.setParameter("name",job);

	    List<Job> lista=query1.getResultList();
	   	if(lista.size()>0){
	   		return lista.get(0);
	   	}else
	   		return null;
      
  }
    
}