package info_extraction;

import info_extraction.entities.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import opennlp.tools.namefind.RegexNameFinder;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.Span;

import org.hibernate.ejb.QueryImpl;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Document;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

public class PopulateLucene {
	
	private IndexWriter indexWriter = null;
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("SIGC");
	EntityManager em= emf.createEntityManager();
	List<Movie> movies;
	List<People> persons;
	List<Genre> genres;
	List<Language> languages;
	private IndexSearcher searcher; 
	private QueryParser titleQueryParser; 
	private QueryParser contentQueryParser; 
	ArrayList<String> descriptions;
	
	
    public IndexWriter getIndexWriter() throws IOException {
    	Analyzer analyzer = new StandardAnalyzer(null);
    	boolean recreateIndexIfExists = true;
        if (indexWriter == null) {
            indexWriter = new IndexWriter(FSDirectory.open(new File("/Users/josericardoramos/Faculdade/4Ano/2Semestre/SIGC/Projeto/LuceneIndexing/")), new IndexWriterConfig(Version.LUCENE_36, new StandardAnalyzer(Version.LUCENE_36)));
        }
        return indexWriter;
   }  
    
   public void getMovies(){
		String query="SELECT s FROM Movie s ";
		org.hibernate.ejb.QueryImpl query1=(org.hibernate.ejb.QueryImpl) em.createQuery(query,Movie.class);
	    movies=((TypedQuery<Movie>) query1).getResultList();
	   
	}

	public void getPersons(){
		String query="SELECT s FROM People s ";
		org.hibernate.ejb.QueryImpl query1=(org.hibernate.ejb.QueryImpl) em.createQuery(query,People.class);
		persons=((TypedQuery<People>) query1).getResultList();
	    
	}
	
	public void getGenres(){
		String query="SELECT s FROM Genre s ";
		org.hibernate.ejb.QueryImpl query1=(org.hibernate.ejb.QueryImpl) em.createQuery(query,Genre.class);
		genres=((TypedQuery<Genre>) query1).getResultList();   
	}
	
	public void getLanguages(){
		String query="SELECT s FROM Language s ";
		org.hibernate.ejb.QueryImpl query1=(org.hibernate.ejb.QueryImpl) em.createQuery(query,Language.class);
		languages=((TypedQuery<Language>) query1).getResultList();   
	}
  
    public void closeIndexWriter() throws IOException {
        if (indexWriter != null) {
            indexWriter.close();
        }
    }
    
    
    public void findTopics(List<Movie> moviesByGenre){
    	descriptions=(new TopicModelling(moviesByGenre)).topicsTerms;
    }
    
    public void writer(){
		
		try{
			getIndexWriter();
		}catch(IOException e){
			
		}
		
		getPersons();
		getMovies();
		getGenres();
		getLanguages();
		
		//findTopics(movies);
		populateLucene();
	}
    
    public PopulateLucene(){
		
		writer();
		//searchPeople("person:'Brad Pitt' AND person:'Angelina Jolie");
		
	}
    
	public static void main(String[] args) {
		new PopulateLucene();
	}


	
/*	
	public List<String> searchPeople(String name){
		List<String> res=new ArrayList<String>();
		try {
			searcher = new IndexSearcher(IndexReader.open(FSDirectory.open(new File("/Users/josericardoramos/Faculdade/4Ano/2Semestre/SIGC/Projeto/LuceneIndexing/")))); 
			StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_36); // defining the query parser to search items by title field. 
			titleQueryParser = new QueryParser(Version.LUCENE_36, "person", analyzer); // defining the query parser to search items by content field. 
			
			Query query;
			query = titleQueryParser.parse(name);

			ScoreDoc[] queryResults = searcher.search((org.apache.lucene.search.Query) query, 10).scoreDocs; 

			for (ScoreDoc scoreDoc : queryResults) { 
				Document doc = searcher.doc(scoreDoc.doc); 
				res.add(doc.get("id"));
				System.out.println("Search: "+doc.get("title"));
			} 
		}catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	}
		return res;
	}
	
	public List<String> searchCompanies(String name){
		List<String> res=new ArrayList<String>();
		try {
			searcher = new IndexSearcher(IndexReader.open(FSDirectory.open(new File("/Users/josericardoramos/Documents/workspace/ProjetoWS/LuceneIndexFile")))); 
			StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_36); // defining the query parser to search items by title field. 
			titleQueryParser = new QueryParser(Version.LUCENE_36, "companies", analyzer); // defining the query parser to search items by content field. 
			
			Query query;
			query = titleQueryParser.parse(name);

			ScoreDoc[] queryResults = searcher.search((org.apache.lucene.search.Query) query, 10).scoreDocs; 

			for (ScoreDoc scoreDoc : queryResults) { 
				Document doc = searcher.doc(scoreDoc.doc); 
				res.add(doc.get("id"));
				System.out.println(doc.get("title"));
			}
		}catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	}
		return res;
	}
	
	public List<String> searchMovies(String name){
		
		List<String> res=new ArrayList<String>();
		try {
			searcher = new IndexSearcher(IndexReader.open(FSDirectory.open(new File("/Users/josericardoramos/Faculdade/4Ano/2Semestre/SIGC/Projeto/LuceneIndexing/")))); 
			StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_36); // defining the query parser to search items by title field. 
			titleQueryParser = new QueryParser(Version.LUCENE_36, "title", analyzer); // defining the query parser to search items by content field. 
			
			Query query;
			query = titleQueryParser.parse(name);

			ScoreDoc[] queryResults = searcher.search((org.apache.lucene.search.Query) query, 10).scoreDocs; 

			for (ScoreDoc scoreDoc : queryResults) { 
				Document doc = searcher.doc(scoreDoc.doc); 
				res.add(doc.get("id"));
				System.out.println(doc.get("title"));
			} 
		}catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
	}
		return res;
	}
*/
	
	public void populateLucene(){
		int i=0;
		
		for(Movie movie:movies){
			Document doc = new Document(); 
			doc.add(new Field("type","movie", Field.Store.YES, Field.Index.NOT_ANALYZED));
			doc.add(new Field("id",""+ movie.getId(), Field.Store.YES, Field.Index.ANALYZED)); 
			doc.add(new Field("name", movie.getTitle(), Field.Store.YES, Field.Index.ANALYZED)); 
			/*
			String[] topicWords=descriptions.get(i).split(" ");
			
			System.out.println(movie.getDescription()+" \n"+descriptions.get(i)+"\n");
			for(int a=0;a<5;a++){
				doc.add(new Field("topicWords",topicWords[a],Field.Store.YES, Field.Index.ANALYZED)); 
			}
			*/
			try{
				indexWriter.addDocument(doc);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		for(People people:persons){
			Document doc = new Document(); 
			doc.add(new Field("type","people", Field.Store.YES, Field.Index.NOT_ANALYZED));
			doc.add(new Field("id",""+ people.getId(), Field.Store.YES, Field.Index.ANALYZED)); 
			doc.add(new Field("name", people.getName(), Field.Store.YES, Field.Index.ANALYZED)); 
			
			try{
				indexWriter.addDocument(doc);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		for(Genre genre:genres){
			Document doc = new Document(); 
			doc.add(new Field("type","genre", Field.Store.YES, Field.Index.NOT_ANALYZED));
			doc.add(new Field("id",""+ genre.getId(), Field.Store.YES, Field.Index.ANALYZED)); 
			doc.add(new Field("name", genre.getName(), Field.Store.YES, Field.Index.ANALYZED)); 
			
			try{
				indexWriter.addDocument(doc);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		for(Language language:languages){
			Document doc = new Document(); 
			doc.add(new Field("type","language", Field.Store.YES, Field.Index.NOT_ANALYZED));
			doc.add(new Field("id",""+ language.getId(), Field.Store.YES, Field.Index.ANALYZED)); 
			doc.add(new Field("name", language.getName(), Field.Store.YES, Field.Index.ANALYZED)); 
			
			try{
				indexWriter.addDocument(doc);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		String[] categories={"film","movie","character","company","actor","genre","job","country","language","rank","year","date"};
		for(String category:categories){
			Document doc = new Document(); 
			doc.add(new Field("type","category", Field.Store.YES, Field.Index.NOT_ANALYZED));
			doc.add(new Field("name",""+ category, Field.Store.YES, Field.Index.ANALYZED)); 
			
			try{
				indexWriter.addDocument(doc);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		try {
			indexWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

	}

}
