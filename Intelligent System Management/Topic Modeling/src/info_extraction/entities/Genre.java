package info_extraction.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "GENRES_TABLE")
public class Genre {
	@Id
	@Column(name = "genre_id")
	private int id;

	private String name;
	
	public Genre(){
		
	}
	public Genre(int id,String name){
		this.setId(id);
		this.setName(name);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
