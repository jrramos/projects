package info_extraction.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "CREWS_TABLE")
public class Crew implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "crew_id")
	private int id;
	
	@OneToOne
    @JoinColumn(name="job_id", nullable=false)
	@LazyCollection(LazyCollectionOption.FALSE)   
	private Job job;
	

	@OneToOne
    @JoinColumn(name="people_id",nullable=false)
	@LazyCollection(LazyCollectionOption.FALSE)   
	private People people;
	
	public Crew(){
		
	}
	
	public Crew(Job jobs,People people){
		this.people=people;
		this.job=jobs;

	}
	
	public Job getJob(){
		return job;
	}
	
	public People getPeople(){
		return people;
	}
}
