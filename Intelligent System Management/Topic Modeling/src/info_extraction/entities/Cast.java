package info_extraction.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "ACTORS_TABLE")
public class Cast implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "actor_id")
	private int id;
	
	@OneToOne
    @JoinColumn(name="people_id", nullable=false)
	@LazyCollection(LazyCollectionOption.FALSE)   
	People people;


	@OneToOne
    @JoinColumn(name="character_id", nullable=false)
	@LazyCollection(LazyCollectionOption.FALSE)   
	Character character;
	
	public Cast(){
		
	}
	
	public Cast(People people,Character character){
		this.people=people;
		this.character=character;
	}
	
	public Character getCharacter(){
		return character;
	}
	
	public People getPeople(){
		return people;
	}
}
