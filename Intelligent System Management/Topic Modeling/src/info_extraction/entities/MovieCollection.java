package info_extraction.entities;

import java.util.ArrayList;

public class MovieCollection {
	public String name;
	public ArrayList<Movie> movie;
	
	public MovieCollection(){
		movie=new ArrayList<Movie>();
	}
}
