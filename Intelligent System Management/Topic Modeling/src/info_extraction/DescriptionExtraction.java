package info_extraction;

import info_extraction.entities.Genre;
import info_extraction.entities.Movie;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import opennlp.tools.cmdline.postag.POSModelLoader;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.InvalidFormatException;

public class DescriptionExtraction {
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("SIGC");
	EntityManager em = emf.createEntityManager();
	List<Genre> genresList;
	List<Movie> moviesList;
	Tokenizer tokenizer;
	POSTaggerME tagger;
	
	 public static void main(String[] args) {
	    	new DescriptionExtraction();
	 }
	 
	 public void getModels(){
			try {
				//pos tagger
				POSModel model = new POSModelLoader().load(new File("/Users/josericardoramos/Documents/workspace/SIGC/libs/en-pos-maxent.bin"));
				tagger = new POSTaggerME(model);
				
				//tokenizer
				InputStream is = new FileInputStream("/Users/josericardoramos/Documents/workspace/SIGC/libs/en-token.bin");
				TokenizerModel model1 = new TokenizerModel(is);
				tokenizer = new TokenizerME(model1);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	 
	 public void findGenres(){
		 String queryGenreNames="SELECT s FROM Genre s";
		 
		 Query query=(Query) em.createQuery(queryGenreNames,Genre.class);
		 
		 genresList=query.getResultList();
	 }
	 
	 public void findMovies(){
		 String queryGenreNames="SELECT s FROM Movie s";
		 
		 Query query=(Query) em.createQuery(queryGenreNames,Movie.class);
		 
		 moviesList=query.getResultList();
	 }
	 
	 public DescriptionExtraction(){
		 getModels();
		 findMovies();
		 findGenres();
		 
		for(Genre genre: genresList){
			 //System.out.println("ta no :"+genre.getName());
			 int nMovies=findMoviePos(genre.getName());
			 findMovieNeg(genre.getName(),nMovies);
			 
		}
		 
	 }
	 
	 
	 public int findMoviePos(String name){
		 	int nMovies=0;
		 	
	    	for(Movie movie:moviesList){
	    			boolean res=false;
	    			try{
	    				int i=0;
	    				
	    				while(true){
	    					if(movie.getGenre(i).getName().compareTo(name)==0){
	    						res=true;
	    						break;
	    					}
	    					i++;
	    				}
		    			//System.out.println(movie.getGenre(i).getName());
	    				if(res){
	    					threatCase(name,movie,true);
	    					nMovies++;	
	    				}
	    			}catch(Exception e){
	    				
	    			}
	    	}

		     return nMovies; 
	 }
	 
	 public void findMovieNeg(String name, int nMovies){
		 	int check=0;
		 	
	    	for(Movie movie:moviesList){
	    			/*
	    			if(check>=nMovies){
	    				return;
	    			}
	    			*/
	    			boolean res=false;
	    			try{
	    				int i=0;
	    				
	    				while(true){
	    					if(movie.getGenre(i).getName().compareTo(name)==0){
	    						res=true;
	    						break;
	    					}
	    					i++;
	    				}
		    			//System.out.println(movie.getGenre(i).getName());
	    				
	    			}catch(IndexOutOfBoundsException e){
	    				if(!res){
	    					threatCase(name,movie,false);
	    					check++;
	    				}
	    			}
	    	}
	 }
	 
	 public void threatCase(String genreName,Movie movie, boolean pos){
		 String folder="";
		 
		 if(pos){
			 folder="/Users/josericardoramos/Faculdade/4Ano/2Semestre/SIGC/Projeto/movies_train_topic/"+genreName.replace(" ","_")+"/pos/"+movie.getId()+"_1.txt";
		 }else{
			 folder="/Users/josericardoramos/Faculdade/4Ano/2Semestre/SIGC/Projeto/movies_train_topic/"+genreName.replace(" ","_")+"/neg/"+movie.getId()+"_1.txt";
		 }
		 //String movieDescriptionContent=new OpenNLP(movie);
		 String content=(new OpenNLP(movie,tokenizer,tagger)).content;
		 
		 saveMovieDescription(folder,content);
	 }
	 
	 
	 private void saveMovieDescription(String folder,String content){	
			try
			{
				FileWriter writer = new FileWriter(folder);
			   	writer.append(content);
			    writer.flush();
			    writer.close();
			}
			catch(IOException e)
			{
			     e.printStackTrace();
			} 
	 }
	
}
