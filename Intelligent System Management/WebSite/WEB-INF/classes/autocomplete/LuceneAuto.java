/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package autocomplete;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.regex.Pattern;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.KeywordAnalyzer;
import org.apache.lucene.analysis.core.SimpleAnalyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.RegexpQuery;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.PhraseQuery;
import org.apache.lucene.search.PrefixQuery;
import org.apache.lucene.search.similarities.DefaultSimilarity;

/**
 *
 * @author c
 */
public class LuceneAuto {
    
    String path ="C:\\Users\\c\\Desktop\\SIGC\\Project\\Meta2\\";
    String indexDirectory = "/Users/josericardoramos/Faculdade/4Ano/2Semestre/SIGC/Projeto/AutoComplete";

    
    ArrayList<Pessoa> listaNomes = new ArrayList<Pessoa>();
    ArrayList<Pessoa> listaGeneros = new ArrayList<Pessoa>();
    ArrayList<Pessoa> listaMovies = new ArrayList<Pessoa>();
    ArrayList<Pessoa> listaJobs = new ArrayList<Pessoa>();

    public static void main(String[] args) throws FileNotFoundException, IOException, Exception {
        // TODO code application logic here
        new LuceneAuto();
    }
    
    public LuceneAuto() throws FileNotFoundException, IOException, Exception{
//        index();
//        addToIndex();
        String[] resultsAc = autoComplete("AC");
        int j = 0;
        for(int i = 0; i < resultsAc.length; i++) {
            if(resultsAc[i]!=null){
                System.out.println("--> "+resultsAc[i]);
                j++;
            }
        }
    }
    
    public String [][] autoQuery(String term  ,int page)throws Exception{
        int max = 150;
        term = term.toLowerCase();
        
        StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_43);
        String  [] field = {"name","movie","class","moviePesq","personPesq"};
        
        MultiFieldQueryParser queryParser = new MultiFieldQueryParser(Version.LUCENE_43, field, analyzer);
        Query query = queryParser.parse(term);
        IndexSearcher detective = createIndexSearcher();
        TopScoreDocCollector collector = TopScoreDocCollector.create( max*page, true);
        detective.search(query, collector);
        
        String pattern = "(.*)"+term+"(.*)";
        Pattern pat = Pattern.compile(pattern);
        
        ScoreDoc clues[]= collector.topDocs().scoreDocs;
        String [][] results =new String [clues.length][2];
        
        for(int i = 0;i< clues.length;i++){
            Document doc = detective.doc(clues[i].doc);
            results[i][0] = ""+(clues[i].score);
            if (doc.get("name")!=null){
                results[i][1] = doc.get("name");
            }
            
            if(doc.get("movie")!=null){
                results[i][1] = doc.get("movie");
            }
            if (doc.get("class")!=null){
                results[i][1] = doc.get("class");
            }
            System.out.println(results[i][1]);
            if(results[i][1]!=null)
                if (!pat.matcher(results[i][1].toLowerCase()).matches())
                    results[i][0] ="-1";
        }
        return results;
    }
    
    public String [] receiveAuto(String term) throws Exception{
        String [] retorn  = new String [20];
        
        IndexSearcher index = createIndexSearcher();
        
        String [][]results;
        ArrayList<String[]> retornResults = new ArrayList<String[]>();
        
        results = autoQuery(term, 1);
        int i;
        for( i = 0;i< results.length;i++){
            if (!results[i][0].equals("-1")){
                results[i][0] = ""+(Float.parseFloat(results[i][0])*80);
                retornResults.add(results[i]);
            }
        }

        //Testar Combinações
        if(term.charAt(term.length()-1)!=' '){
            results= autoQuery(term+"*", 1);
            for( i = 0;i< results.length;i++){
                if (!results[i][0].equals("-1")){
                    results[i][0] = ""+(Float.parseFloat(results[i][0])*50);
                    retornResults.add(results[i]);
                }
            }
        }
        StringBuilder tmp; 
        for(int j=1;j<term.length();j++){
            if(term.charAt(j)!= ' ' && term.charAt(j-1)!=' '){
                tmp = new StringBuilder (term);
                tmp.setCharAt(j,'?');
                results = autoQuery(tmp.toString(),1);
                for( i = 0;i< results.length;i++){
                    if (!results[i][0].equals("-1")){
                        results[i][0] = "" +(Float.parseFloat(results[i][0])*5);
                        retornResults.add(results[i]);
                    }
                }
            }
        }
        
        //eliminar repetidos
        for( i =0;i<retornResults.size()-1;i++){
            for(int j =i+1;j<retornResults.size();j++){
                if(retornResults.get(j)[1]!=null && retornResults.get(j)[1]!=null)
                    if(retornResults.get(j)[1].equals(retornResults.get(i)[1])) {
                        retornResults.get(i)[0] = "" + (Float.parseFloat(retornResults.get(i)[0]) + Float.parseFloat(retornResults.get(j)[0]));
                        retornResults.remove(j);
                        j--;
                    }   
            }
        }
        
        Collections.sort(retornResults, new AllComparator());
        
        for( i =0;i< 10 && i<retornResults.size();i++){
            retorn[i]= retornResults.get(i)[1];
        }
        return retorn;
        
    }
    public String [][]  checkQuery(String term  ,int page)throws Exception{
        int max = 150;
        term = term.toLowerCase();
        
        StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_43);
        String  [] field = {"name","movie","class"};
        
        MultiFieldQueryParser queryParser = new MultiFieldQueryParser(Version.LUCENE_43, field, analyzer);
        Query query = queryParser.parse(term);
        IndexSearcher detective = createIndexSearcher();
        TopScoreDocCollector collector = TopScoreDocCollector.create( max*page, true);
        detective.search(query, collector);
        
        String pattern = "(.*)"+term+"(.*)";

        Pattern pat = Pattern.compile(pattern);
        
        ScoreDoc clues[]= collector.topDocs().scoreDocs;
        String [][] results =new String [clues.length][3];
        
        for(int i = 0;i< clues.length;i++){
            Document doc = detective.doc(clues[i].doc);
            results[i][0] = ""+(clues[i].score);
           
            if (doc.get("name")!=null){
                results[i][1] = doc.get("name");               
                results[i][2] = "person";
            }
            
            if(doc.get("movie")!=null){
                results[i][1] = doc.get("movie");                
                results[i][2] = "movie";
            }
            
            if(doc.get("class")!=null){
                results[i][1] = doc.get("class");
                System.out.println("class");
                results[i][2] = "class";
            }
            if (!pat.matcher(results[i][1].toLowerCase()).matches())
                results[i][0] ="-1";
        }
        return results;
    }
    
    public String  check(String term) throws Exception{
        String [] retorn  = new String [20];
        IndexSearcher index = createIndexSearcher();
        String [][]results;
        ArrayList<String[]> retornResults = new ArrayList<String[]>();
        
        results = checkQuery(term, 1);
        return results[0][2];
    }
    
    public String [] recebeDe(String [] spliter){
        String [] array = new String[spliter.length];
        for(int i=0;i<spliter.length;i++){
            String nn ="";
            for(int j=0;j<i;j++)
                nn+= spliter[j]+" ";
            array[i] = nn;
        }
        return array;
    }
    
    public String [] autoComplete(String term) throws Exception{
        
        String [] result =receiveAuto(term);

        if (result[0]== null){
            String [] spliter = (term+" ").split(" ");
            String []array = recebeDe(spliter);
            int i = array.length-1;
            if (i>0){
                while(i>0){
                    String aa = check(array[i]);
                    
                    if (aa.equals("person")){
                        String []field = {"personPesq"};
                        String nn = term.replace(array[i],"");
                        System.out.println(nn);
                        String []cls = classes(nn, field);
                        for (int k=0;k<cls.length;k++){
                            if(cls[k]!= null){
                                String NN ="";

                                cls[k]= array[i] + cls[k];                            
                            }
                        }
                        return cls;
                    }
                    else if (aa.equals("movie")){
                        String []field ={"moviePesq"};
                        String nn = term.replace(array[i],""); 
                        System.out.println(nn);
                        String []cls = classes(nn, field);
                        for (int k=0;k<cls.length;k++){
                            if(cls[k]!= null){
                                cls[k]= array[i] + cls[k];                            
                            }
                        }
                        return cls;
                    }   
                    else if (aa.equals("class")){
                        String []field ={"class"};
                        String nn = term.replace(array[i],""); 
                        System.out.println(nn);
                        String []cls = classes(nn, field);
                        for (int k=0;k<cls.length;k++){
                            if(cls[k]!= null){
                                cls[k]= array + cls[k];                            
                            }
                        }
                        return cls;
                        
                    }
                    i--;
                }
            }
                
            }
        return result;
        
    }
    
    
    
    public String [] classes(String term, String []field) throws Exception{
        String [] retorn  = new String [20];
        
        IndexSearcher index = createIndexSearcher();
        
        String [][]results;
        ArrayList<String[]> retornResults = new ArrayList<String[]>();
        
        results= classesQuery(term, 1,field);
        int i;
        for( i = 0;i< results.length;i++){
            if (!results[i][0].equals("-1")){
                results[i][0] = ""+(Float.parseFloat(results[i][0])*80);
                retornResults.add(results[i]);
            }
        }
        //imcompleta
        if(term.charAt(term.length()-1)!=' '){
            results= classesQuery(term+"*", 1,field);
            for( i = 0;i< results.length;i++){
                if (!results[i][0].equals("-1")){
                    results[i][0] = ""+(Float.parseFloat(results[i][0])*50);
                    retornResults.add(results[i]);
                }
            }
        }
        
        StringBuilder tmp; 
        for(int j=1;j<term.length();j++){
            if(term.charAt(j)!= ' ' && term.charAt(j-1)!=' '){
                tmp = new StringBuilder (term);
                tmp.setCharAt(j,'?');
                results = classesQuery(tmp.toString(),1,field);
                for( i = 0;i< results.length;i++){
                    if (!results[i][0].equals("-1")){
                        results[i][0] = ""+(Float.parseFloat(results[i][0])*5);
                        retornResults.add(results[i]);
                    }
                }
            }
        }
        for( i =0;i<retornResults.size()-1;i++){
            for(int j =i+1;j<retornResults.size();j++){
                if(retornResults.get(j)[1].equals(retornResults.get(i)[1])) {
                    retornResults.get(i)[0] = ""+(Float.parseFloat(retornResults.get(i)[0]) + Float.parseFloat(retornResults.get(j)[0]));
                    retornResults.remove(j--);
                }
            }
        }
        
        Collections.sort(retornResults, new AllComparator());
        
        for( i =0;i< 10 && i<retornResults.size();i++){
            retorn[i]= retornResults.get(i)[1];
        }
        return retorn;
        
    }
    public String [][]  classesQuery(String term  ,int page, String []field )throws Exception{
        int max = 150;
        term = term.toLowerCase();
        
        StandardAnalyzer analyzer = new StandardAnalyzer(Version.LUCENE_43);
        
        MultiFieldQueryParser queryParser = new MultiFieldQueryParser(Version.LUCENE_43, field, analyzer);
        Query query = queryParser.parse(term);
        IndexSearcher detective = createIndexSearcher();
        TopScoreDocCollector collector = TopScoreDocCollector.create( max*page, true);
        detective.search(query, collector);
                
        String pattern = "(.*)"+term+"(.*)";

        Pattern pat = Pattern.compile(pattern);
        
        ScoreDoc clues[]= collector.topDocs().scoreDocs;
        String [][] results =new String [clues.length][2];
        
        for(int i = 0;i< clues.length;i++){
            Document doc = detective.doc(clues[i].doc);
            results[i][0] = ""+(clues[i].score);
            if (doc.get("personPesq")!=null){
                results[i][1] = doc.get("personPesq");
            }
            
            if(doc.get("moviePesq")!=null){
                results[i][1] = doc.get("moviePesq");
            }
            if (doc.get("class")!=null){
                results[i][1] = doc.get("class");
            }
            if (!pat.matcher(results[i][1].toLowerCase()).matches())
                results[i][0] ="-1";
        }
        return results;
    }
    
    public void index() throws IOException{
        boolean create = true;

        File indexDirFile = new File(this.indexDirectory);
  
        Directory dir = FSDirectory.open(indexDirFile);
        Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_43);
        IndexWriterConfig iwc = new IndexWriterConfig(Version.LUCENE_43, analyzer);
        
  
        IndexWriter writer = new IndexWriter(dir, iwc);
        writer.commit();
        writer.close();
    }
    
    public IndexWriter createIndexWriter() throws Exception {

        File indexDirFile = new File(this.indexDirectory);

        Directory dir = FSDirectory.open(indexDirFile);
        Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_43);
        IndexWriterConfig iwc = new IndexWriterConfig(Version.LUCENE_43, analyzer);
        iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
        IndexWriter writer = new IndexWriter(dir, iwc);

        return writer;
    }
    
    public void addToIndex()throws Exception{
        
        
        listaNomes = recebe("people.txt");
        listaGeneros = recebe("genre.txt");
        listaMovies = recebe("movies.txt");
        listaJobs = recebe("jobs.txt");
        
        
        IndexWriter writer = createIndexWriter();
        
        String []movies = {"directors", "producers", "actors","characters"};
        String []person = {"movies", "characters", "actors","job"};
        String []classes = {"characters", "actors","job","movies", "characters", "actors","job", "year","genres","action"};

        
        for(int i =0;i<listaJobs.size();i++){
            Document doc = new Document();
            doc.add(new TextField("moviePesq",listaJobs.get(i).nome,Field.Store.YES));
            writer.addDocument(doc);
        }
        
        for(int i =0;i<person.length;i++){
            Document doc = new Document();
            doc.add(new TextField("personPesq",person[i],Field.Store.YES));
            writer.addDocument(doc);
        }
        
        
        for(int i =0;i<listaGeneros.size();i++){
            Document doc = new Document();
            doc.add(new TextField("class",listaGeneros.get(i).nome,Field.Store.YES));
            writer.addDocument(doc);
        }
        
        System.out.println(listaNomes.size());
        for(int i = 0;i<listaNomes.size();i++){
            Document doc = new Document();
            doc.add(new TextField("name",listaNomes.get(i).nome,Field.Store.YES));
            writer.addDocument(doc);
        }
        
        for(int i = 0;i<listaMovies.size();i++){
            Document doc = new Document();
            doc.add(new TextField("movie",listaMovies.get(i).nome,Field.Store.YES));
            writer.addDocument(doc);
        }
        
        writer.commit();
        writer.close();
    }
    
    public IndexSearcher createIndexSearcher(){
        IndexReader indexReader = null;
        IndexSearcher indexSearcher = null;
        try {
            File indexDirFile = new File(this.indexDirectory);
            Directory dir = FSDirectory.open(indexDirFile);
            indexReader = DirectoryReader.open(dir);
            indexSearcher = new IndexSearcher(indexReader);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return indexSearcher;
        
    }
    
    public ArrayList<Pessoa>recebe(String nameFile) throws FileNotFoundException, IOException{
        BufferedReader br = new BufferedReader(new FileReader(path+nameFile));
        ArrayList<Pessoa> paragraph=new  ArrayList<Pessoa>();
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                    paragraph.add(new Pessoa(line));
                line = br.readLine();
            }
        } finally {
            br.close();
        }
        return paragraph;

        
    }
}


class AllComparator implements Comparator<String[]> {
    @Override
    public int compare(String[] x1, String[] x2) {
        if(Float.parseFloat(x1[0]) < Float.parseFloat(x2[0])) 
            return 1;
        if(Float.parseFloat(x1[0]) > Float.parseFloat(x2[0])) 
            return -1;
        return 0;
    }
}
