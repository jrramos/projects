package beans;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.hp.hpl.jena.graph.Node;
import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ReadWrite;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.sdb.store.Feature.Name;
import com.hp.hpl.jena.tdb.TDBFactory;

import entities.Genre;
import entities.Movie;


public class Query {
	int i;
	String ns="http://www.w3.org/1999/02/22-rdf-syntax-ns#";
	Dataset dataset;
	
	
	public Query(){
    	String directory = "/Users/josericardoramos/Faculdade/4Ano/2Semestre/SIGC/Projeto/TDBDatabase" ;
    	dataset = TDBFactory.createDataset(directory) ;
		dataset.begin(ReadWrite.READ) ;
	}
	public String seeDefault(ArrayList<Tuple1> tuples){
		int result=0;

		for(Tuple1 tuple:tuples){
	 			
	 			if(tuple.type.substring(tuple.type.indexOf('#')+1, tuple.type.length()).compareTo("Person")==0){
	 				result=1;
	 			}else if(tuple.type.substring(tuple.type.indexOf('#')+1, tuple.type.length()).compareTo("Company")==0){
	 				result=0;
	 			}else if(tuple.type.substring(tuple.type.indexOf('#')+1, tuple.type.length()).compareTo("Movie")==0){
	 				result=0;
	 				break;
	 			}
	 			
	 	}

	 	if(result==0){
	 		return "Movie";
	 	}else if(result==1){
	 		return "Character";
	 	}else{
	 		return "Movie";
	 	}
	}
	
	public ArrayList<ResultFinal> semanticSearch(ArrayList<String> classes,ArrayList<String> jobs,ArrayList<String> languages,ArrayList<String> rank,ArrayList<String> date,ArrayList<String> genres,ArrayList<String> other){

		int i,a;
		Movie movie=null;
		ArrayList<Movie> moviesList=null;
		String name="";
		String nameSpace="http://www.owl-ontologies.com/creationOwl#";
		ArrayList<Tuple1> tuples=getEntities(other);
		ArrayList<String> queries=new ArrayList<String>();
		ArrayList<String> types=new ArrayList<String>();
		String description="";

	   String queryString ="prefix nameSpace: <"+nameSpace+"> "+
	 			"prefix ns: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> ";
			   
	 	String querieAux="";
	 	String type="";
	 	ArrayList<Result> res=new ArrayList<Result>();
	 	
	 	if(classes==null || classes.size()==0 ){
	 		classes.add(seeDefault(tuples));
	 	}

	 	for(i=0;i<classes.size();i++){
	 		System.out.println("Ola!!"+classes.get(0));
	 		if(classes.get(i).matches("(m|M)ovie(s)?")){
	 			System.out.println("MATCHES FILMES");
	 			querieAux="SELECT DISTINCT ?id ?name WHERE {?movie nameSpace:hasName ?name. ?movie nameSpace:hasID ?id."
		 				+ "?movie ns:type nameSpace:Movie.";
	 			type="Movies";
	 			types.add(type);
	 		}else if(classes.get(i).matches("(c|C)haracter(s)?")){
	 			System.out.println("MATCHES Characters");
	 			querieAux="SELECT DISTINCT ?id ?name ?idCharacter ?nameCharacter ?personName ?personId WHERE {?movie nameSpace:hasName ?name. ?movie nameSpace:hasID ?id."
		 				+ "?movie ns:type nameSpace:Movie. ?movie nameSpace:hasCast ?cast. ?cast nameSpace:hasCharacter ?character. ?character nameSpace:hasID ?idCharacter."
		 				+ "?character nameSpace:hasName ?nameCharacter. ?cast nameSpace:hasPerson ?person. ?person nameSpace:hasName ?personName. ?person nameSpace:hasID ?personId.";
	 			type="Characters";
	 			types.add(type);
	 		}else if(classes.get(i).matches("(a|A)ctor(s)?")){
	 			System.out.println("MATCHES Actors");
	 			querieAux="SELECT DISTINCT ?id ?name ?idActor ?nameActor WHERE {?movie nameSpace:hasName ?name. ?movie nameSpace:hasID ?id."
		 				+ "?movie ns:type nameSpace:Movie. ?movie nameSpace:hasCast ?cast. ?cast nameSpace:hasPerson ?person. ?person nameSpace:hasID ?idActor."
		 				+ "?person nameSpace:hasName ?nameActor.";
	 			type="Actors";
	 			types.add(type);
	 		}else if(classes.get(i).matches("(g|G)enre(s)?")){
	 			System.out.println("MATCHES Genre");
	 			querieAux="SELECT DISTINCT ?id ?name ?idGenre ?nameGenre WHERE {?movie nameSpace:hasName ?name. ?movie nameSpace:hasID ?id."
		 				+ "?movie ns:type nameSpace:Movie. ?movie nameSpace:hasGenres ?genre. ?genre nameSpace:hasName ?nameGenre. ?genre nameSpace:hasID ?idGenre.";
	 			type="Genres";
	 			types.add(type);
	 		}else if(classes.get(i).matches("(((y|Y)ear)|((D|d)ate))(s)?")){
	 			System.out.println("MATCHES date");
	 			querieAux="SELECT DISTINCT ?id ?name ?idYear WHERE {?movie nameSpace:hasName ?name. ?movie nameSpace:hasID ?id."
		 				+ "?movie ns:type nameSpace:Movie. ?movie nameSpace:hasYear ?idYear.";
	 			type="Year";
	 			types.add(type);
	 		}else if(classes.get(i).matches("(r|R)ank(s)?")){
	 			System.out.println("MATCHES Rank");
	 			querieAux="SELECT DISTINCT ?id ?name ?nameRank WHERE {?movie nameSpace:hasName ?name. ?movie nameSpace:hasID ?id."
		 				+ "?movie ns:type nameSpace:Movie. ?movie nameSpace:hasClassification ?nameRank.";
	 			type="Rank";
	 			types.add(type);
	 		}else if(classes.get(i).matches("(c|C)ompan(y|ies)")){
	 			System.out.println("MATCHES company");
	 			querieAux="SELECT DISTINCT ?id ?name ?nameCompany WHERE {?movie nameSpace:hasName ?name. ?movie nameSpace:hasID ?id."
		 				+ "?movie ns:type nameSpace:Movie. ?movie nameSpace:hasCompany ?company. ?company nameSpace:hasName ?nameCompany. ";
	 			type="Companies";
	 			types.add(type);
	 		}else if(classes.get(i).matches("(j|J)ob(s)?")){
	 			System.out.println("MATCHES jobs");
	 			querieAux="SELECT DISTINCT ?id ?name ?idJob ?nameJob WHERE {"
		 				+ "?cast ns:type nameSpace:Crew. ?cast nameSpace:hasPerson ?person. ?person nameSpace:hasName ?name."
		 				+ "?cast nameSpace:hasJob ?job. ?job nameSpace:hasName ?nameJob . ?job nameSpace:hasID ?idJob ."
		 				+ "?person nameSpace:hasID ?id. ";
	 			int check=0;
	 			for(a=0;a<tuples.size();a++){
	 				System.out.println("MATCHES "+tuples.get(a).name+" "+tuples.get(a).type.substring(tuples.get(a).type.indexOf('#')+1, tuples.get(a).type.length()));
	 				if(tuples.get(a).type.substring(tuples.get(a).type.indexOf('#')+1, tuples.get(a).type.length()).compareTo("Person")==0){
		 				if(check==0){
		 					querieAux+="FILTER (regex(?name,'^"+tuples.get(a).name+"$','i')";
		 					check++;
		 				}else{
		 					querieAux+="|| regex(?name,'^"+tuples.get(a).name+"$','i')";
		 				}
		 			}
	 			}
	 			if(check>0){
 					querieAux+=").";
 				}
	 			System.out.println("!?!?!?"+querieAux);
	 			type="Jobs";
	 			types.add(type);
	 		}

	 		//description=type;
	 		//queries.add(queryString+querieAux);
	 		ArrayList<Result> queriesRes;
	 		queriesRes=makeQuery(queryString+querieAux,type,classes,jobs,languages,rank,date,genres,other,tuples,description);
	 		for(Result res1:queriesRes){
	 			res.add(res1);
	 		}
	 		
	 	}
	 	for(i=0;i<jobs.size();i++){
	 		System.out.println("???");
	 		//if(classes.get(i).matches("(((d|D)irector)|((p|P)roducer)|((o|O)riginal (m|M)usic (c|C)omposer)|((d|D)irector of (p|P)hotography)|((p|P)roduction (d|D)esign)|((c|C)asting)|((e|E)ditor)|((s|S)creenplay)|(Art Direction)|(Author)|(Executive Producer)|(Writer)|((M|m)usic)|((p|P)roduction Manager)|((N|n)ovel)|(Animation)|((S|s)pecial Effects)|((S|s)tory)|((M|m)usical)|((p|P)roduction Supervisor)|((O|o)riginal Story)|((c|C)reator))(s)?")){
	 			System.out.println("MATCHES Director");
	 			querieAux="SELECT DISTINCT ?id ?name ?idCrew ?nameCrew WHERE {?movie nameSpace:hasName ?name. ?movie nameSpace:hasID ?id ."
		 				+ " ?movie ns:type nameSpace:Movie . ?movie nameSpace:hasCrew ?crew . ?crew nameSpace:hasPerson ?person . ?person nameSpace:hasID ?idCrew ."
		 				+ " ?person nameSpace:hasName ?nameCrew . ?crew nameSpace:hasJob ?job . ?job nameSpace:hasName ?jobName . FILTER (regex(?jobName,'^"+jobs.get(i)+"$','i')) .";
	 		//}
	 		//queries.add(queryString+querieAux);
	 		type="Jobs";
	 		
	 		ArrayList<Result> queriesRes;
	 		queriesRes=makeQuery(queryString+querieAux,type,classes,jobs,languages,rank,date,genres,other,tuples,description);
	 		for(Result res1:queriesRes){
	 			res.add(res1);
	 		}
	 	}
	 	
	 	ArrayList<ResultFinal> resFinal=new ArrayList<ResultFinal>();
	 	int id,check;

	 	for(i=0;i<res.size();i++){
	 		System.out.println("->>>>>>!!"+res.get(i).type+" "+res.get(i).id);
	 		id=res.get(i).id;
	 		check=0;
	 		for(a=0;a<resFinal.size();a++){
	 			if(id==resFinal.get(a).id){
	 				check=1;
	 				break;
	 			}
	 		}
	 		if(check==1){
	 			continue;
	 		}

	 		ResultFinal resFinalAux=new ResultFinal();

	 		resFinalAux.id=res.get(i).id;
	 		resFinalAux.name=res.get(i).name;

	 		if(res.get(i).type.compareTo("Companies")==0){
	 			resFinalAux.type.add("Companies");
				resFinalAux.nameCompany.add(res.get(i).name1);
				resFinalAux.idCompany.add(res.get(i).id1);
	 		}else if(res.get(i).type.compareTo("Movies")==0){
				resFinalAux.type.add("Movies");
	 		}else if(res.get(i).type.compareTo("Characters")==0){
	 			resFinalAux.type.add("Characters");
	 			resFinalAux.nameCharacter.add(res.get(i).name1);
	 			resFinalAux.idCharacter.add(res.get(i).id1);
	 		}else if(res.get(i).type.compareTo("Genres")==0){
	 			resFinalAux.type.add("Genres");
	 			resFinalAux.genres.add(res.get(i).name1);
	 		}else if(res.get(i).type.compareTo("Actors")==0){
	 			resFinalAux.type.add("Actors");
	 			resFinalAux.nameActors.add(res.get(i).name1);
	 			resFinalAux.idActors.add(res.get(i).id1);
	 		}else if(res.get(i).type.compareTo("Year")==0){
	 			resFinalAux.type.add("Year");
	 			resFinalAux.year=res.get(i).id1+"";
	 		}else if(res.get(i).type.compareTo("Rank")==0){
	 			resFinalAux.type.add("Rank");
	 			resFinalAux.rank=res.get(i).name1;
	 		}else if(res.get(i).type.compareTo("Jobs")==0){
	 			resFinalAux.type.add("Jobs");
	 			resFinalAux.rank=res.get(i).name1;

	 		}

	 		resFinalAux.description=res.get(i).description;
	 		for(a=i+1;a<res.size();a++){
	 			if(res.get(a).id==id){
	 				check=0;
		 			if(res.get(a).type.compareTo("Companies")==0){
			 			resFinalAux.type.add("Companies");
						resFinalAux.nameCompany.add(res.get(a).name1);
						resFinalAux.idCompany.add(res.get(a).id1);
			 		}else if(res.get(a).type.compareTo("Movies")==0){
						resFinalAux.type.add("Movies");
			 		}else if(res.get(a).type.compareTo("Characters")==0){
			 			resFinalAux.type.add("Characters");
			 			resFinalAux.nameCharacter.add(res.get(a).name1);
			 			resFinalAux.idCharacter.add(res.get(a).id1);
			 		}else if(res.get(a).type.compareTo("Genres")==0){
			 			resFinalAux.type.add("Genres");
			 			resFinalAux.genres.add(res.get(a).name1);
			 		}else if(res.get(a).type.compareTo("Actors")==0){
			 			resFinalAux.type.add("Actors");
			 			resFinalAux.nameActors.add(res.get(a).name1);
			 			resFinalAux.idActors.add(res.get(a).id1);
			 		}else if(res.get(a).type.compareTo("Year")==0){
			 			resFinalAux.type.add("Year");
			 			resFinalAux.year=res.get(a).id1+"";
			 		}else if(res.get(a).type.compareTo("Rank")==0){
			 			resFinalAux.type.add("Rank");
			 			resFinalAux.rank=res.get(a).name1;
			 		}else if(res.get(a).type.compareTo("Jobs")==0){
			 			resFinalAux.type.add("Jobs");
			 			resFinalAux.nameJobs.add(res.get(a).name1);
			 			resFinalAux.idJobs.add(res.get(a).id1);
			 		}
		 		}
	 		}

	 		resFinal.add(resFinalAux);
	 	}

	 	return resFinal;
	}	
	 
	public ArrayList<Result> makeQuery(String query,String type,ArrayList<String> classes,ArrayList<String> jobs,ArrayList<String> languages,ArrayList<String> rank,ArrayList<String> date,ArrayList<String> genres,ArrayList<String> other,ArrayList<Tuple1> tuples,String description){
	 	String querieAux="";
	 	
	 		if(languages.size()>0){
	 			querieAux+="?movie nameSpace:hasSpokenLanguages ?language . ?language nameSpace:hasName ?nameLanguage  . FILTER (regex(?nameLanguage,'^"+languages.get(0)+"$','i')"; 
	 			description+="\nLanguage="+languages.get(0);
		 		for(int a=1;a<languages.size();a++){
		 			querieAux+=" || regex(?language,'^"+languages.get(a)+"$','i')";
		 			description+="\nLanguage="+languages.get(a);
		 		}
		 		querieAux+=").";
				
			}
	 		
	 		if(rank.size()>0){
	 			querieAux+="?movie nameSpace:hasClassification ?classification . FILTER (?classification = "+rank.get(0); 
	 			description+="\nRank="+rank.get(0);
		 		for(int a=1;a<rank.size();a++){
		 			querieAux+=" || ?rank = "+rank.get(a);
		 			description+="\nRank="+rank.get(a);
		 		}
		 		querieAux+=").";

			}
	 		
	 		if(date.size()>0){
	 			querieAux+="?movie nameSpace:hasYear ?year . FILTER (?year = "+date.get(0); 
	 			description+="\nDate="+date.get(0);
		 		for(int a=1;a<date.size();a++){
		 			querieAux+=" || ?year = "+date.get(a);
		 			description+="\nDate="+date.get(a);
		 		}
		 		querieAux+=").";
			}
	 		
	 		if(genres.size()>0){
	 			System.out.println("Entrou Genres");
	 			querieAux+="?movie nameSpace:hasGenres ?genre . ?genre  nameSpace:hasName ?nameGenres . FILTER (regex(?nameGenres,'^"+genres.get(0)+"$','i')"; 
	 			description+="\nGenres"+genres.get(0);
	 			for(int a=1;a<genres.size();a++){
	 				querieAux+=" || regex(?nameGenres,'^"+genres.get(a)+"$','i')";
	 				description+="\nGenres="+genres.get(a);
		 		}
	 			querieAux+="). ";
			}
	 		
	 		int numPerson=0;
	 		int numCompany=0;
	 		int numMovie=0;
	 		
	 		List<String> moviesIds=new ArrayList<String>();
	 		List<String> moviesIdsAux=new ArrayList<String>();
	 		PopulateLucene search=new PopulateLucene();
	 		
	 		String peopleQ="";
	 		String companyQ="";
	 		String movieQ="";
	 		int person=0;
	 		for(Tuple1 tuple:tuples){
	 			System.out.println("Entrou tuples "+tuple.name);
	 			List<Integer> res;
	 			if(tuple.type.substring(tuple.type.indexOf('#')+1, tuple.type.length()).compareTo("Person")==0 && !type.matches("(c|C)haracter(s)?") ){
	 				System.out.println("Entrou person!!!");

	 				if(numPerson==0){
	 					peopleQ+="person:'"+tuple.name+"' ";
	 					description+="\nPerson="+tuple.name;
	 				}else{
	 					peopleQ+="AND person:'"+tuple.name+"' ";
	 					description+=" && Person="+tuple.name;
	 				}
	 				numPerson++;

	 			}else if(tuple.type.substring(tuple.type.indexOf('#')+1, tuple.type.length()).compareTo("Company")==0){
	 				
	 				if(numCompany==0){
	 					companyQ+="company:'"+tuple.name+"' ";
	 					description+="\nCompany="+tuple.name;
	 				}else{
	 					companyQ+="AND company:'"+tuple.name+"' ";
	 					description+=" && Company="+tuple.name;
	 				}
	 				
	 				numCompany++;
	 			}else if(tuple.type.substring(tuple.type.indexOf('#')+1, tuple.type.length()).compareTo("Movie")==0){
	 				System.out.println("Entrou movie!!!!");
	 				if(numMovie==0){
	 					movieQ+="title:'"+tuple.name+"' ";
	 					description+="\nname="+tuple.name;
	 				}else{
	 					if(!tuple.name.matches("Transformers: The Movie")){
		 					movieQ+="OR title:'"+tuple.name+"' ";
		 					description+=" || name="+tuple.name;
	 					}	
	 				}
	 				
	 				numMovie++;
	 			}else if(tuple.type.substring(tuple.type.indexOf('#')+1, tuple.type.length()).compareTo("Person")==0 && type.matches("(c|C)haracter(s)?") ){
	 				System.out.println("Entrou!!!");
	 				if(person==0){
	 					querieAux+=" FILTER (regex(?personName,'^"+tuple.name+"$','i')";
	 					person++;
	 					description+="\nPerson="+tuple.name;
	 				}else{
	 					querieAux+=" || regex(?personName,'^"+tuple.name+"$','i')";
	 					description+=" || Person="+tuple.name;
	 				}
	 			}
	 			
	 		}
	 		if(person>0){
	 			querieAux+=")";
	 		}
	 		
	 		if(peopleQ.compareTo("")!=0){
	 			System.out.println(peopleQ);
		 		moviesIds=search.searchPeople(peopleQ);
	 		}if(companyQ.compareTo("")!=0){
		 		//moviesIdsAux=search.searchCompanies(companyQ);
		 		
		 		if(moviesIds!=null && moviesIds.size()!=0){
			 		for(int counter=0;counter<moviesIds.size();counter++){
			 			if(moviesIdsAux.contains(moviesIds.get(counter))){
			 				System.out.println("Ta a funcionar!");
			 			}else{
			 				System.out.println("A remover "+counter+" "+moviesIdsAux.get(0));
			 				moviesIds.remove(counter);
			 				counter--;
			 			}
			 		
			 		}
		 		}else{
		 			moviesIds=moviesIdsAux;
		 		}
	 		}if(movieQ.compareTo("")!=0){
	 			System.out.println(movieQ);
	 			moviesIdsAux=search.searchMovies(movieQ);
		 	
	 			if(moviesIds!=null && moviesIds.size()!=0){
	 				System.out.println(moviesIds.size());
			 		for(int counter=0;counter<moviesIds.size();counter++){
			 			if(moviesIdsAux.contains(moviesIds.get(counter))){
			 				System.out.println("Ta a funcionar!");
			 			}else{
			 				System.out.println("A remover "+counter+" "+moviesIds.get(0));
			 				moviesIds.remove(counter);
			 				counter--;
			 			}
			 		
			 		}
	 			}else{
		 			moviesIds=moviesIdsAux;
		 		}
	 		}
		 		
	 		
		 	if(moviesIds.size()>0){
		 		querieAux+=" {?movie nameSpace:hasID "+moviesIds.get(0)+"}";
		 	}else if(other.size()>0 && !type.matches("(c|C)haracter(s)?")){
		 		System.out.println("TA MALL!!!!");
		 		querieAux+=" {?movie nameSpace:hasID 0}";
		 	}
		 		
		 	for(i=1;i< moviesIds.size() ;i++){
		 		querieAux+=" UNION {?movie nameSpace:hasID "+moviesIds.get(i)+"}";
		 	}
	 	
		 	
		 	
		 querieAux+="}";
	 	//System.out.println(queryString);
	 	/*for(i=0;i<classes.size();i++){
	 		
	 	}*/
	 	String name="";		
	 	ArrayList<Result> res=new ArrayList<Result>();		
	 		
 		System.out.println(query+"\n"+querieAux);
	    QueryExecution qExec = QueryExecutionFactory.create(query+querieAux, dataset) ;
	    ResultSet rs = qExec.execSelect() ;
	    String columnName;
		try {
				//moviesList=new ArrayList<Movie>();

				
				while(rs.hasNext()){
					Result resAux=new Result();
					resAux.type=type;
					QuerySolution row= rs.next();
					Iterator columns=row.varNames();
					//System.out.println("Nova linha!");
					while (columns.hasNext()) {
						
					      RDFNode cell = row.get((columnName=columns.next().toString()));
					      if(type.compareTo("Characters")!=0){
						      if(columnName.compareTo("name")==0){
						    	  String campoAux=cell.asLiteral().toString();				    	
					    		  name=campoAux.substring(0,campoAux.indexOf('^'));
					    		  resAux.name=name;
						      }else if(columnName.compareTo("id")==0){
						    	  String campoAux=cell.asLiteral().toString();				    	
					    		  name=campoAux.substring(0,campoAux.indexOf('^'));
					    		  resAux.id=Integer.parseInt(name);
						      }else if(columnName.matches("name(.)*")){
					    		  String campoAux=cell.asLiteral().toString();				    	
					    		  name=campoAux.substring(0,campoAux.indexOf('^'));
					    		  resAux.name1=name;
					    	  }else if(columnName.matches("id(.)*")){
					    		  String campoAux=cell.asLiteral().toString();
					    		  name=campoAux.substring(0,campoAux.indexOf('^'));
					    		  resAux.id1=Integer.parseInt(name);
					    	  }
						      System.out.println(name);
						   }else{
						   	if(columnName.compareTo("personName")==0){
						    	  String campoAux=cell.asLiteral().toString();				    	
					    		  name=campoAux.substring(0,campoAux.indexOf('^'));
					    		  resAux.name=name;
						      }else if(columnName.compareTo("personId")==0){
						    	  String campoAux=cell.asLiteral().toString();				    	
					    		  name=campoAux.substring(0,campoAux.indexOf('^'));
					    		  resAux.id=Integer.parseInt(name);
						      }else if(columnName.matches("name(.)*")){
					    		  String campoAux=cell.asLiteral().toString();				    	
					    		  name=campoAux.substring(0,campoAux.indexOf('^'));
					    		  resAux.name1=name;
					    	  }else if(columnName.matches("id")){
					    		  String campoAux=cell.asLiteral().toString();
					    		  name=campoAux.substring(0,campoAux.indexOf('^'));
					    		  resAux.id1=Integer.parseInt(name);
					    	  }
						      System.out.println(name);
						   }
				      
				 }

				resAux.description=description;
				res.add(resAux);
			 }
			
	
		} finally {  }

	 
		return res;
    }
	
	public ArrayList<Tuple1> getEntities(ArrayList<String> other){
		ArrayList<Tuple1> list=new ArrayList<Tuple1>();
		
		String name="";
		String nameSpace="http://www.owl-ontologies.com/creationOwl#";
		
	
			   
	 		
	 	for(i=0;i<other.size();i++){
	 		 String queryString ="prefix nameSpace: <"+nameSpace+"> "
		 		+"prefix ns: <"+ns+"> "
		 		+"SELECT DISTINCT ?name ?type WHERE {{?x nameSpace:hasName ?name . ?x ns:type ?type . FILTER (regex(?name,\"(^|((.)* ))"+other.get(i)+"($| ((.)*))\",'i'))} UNION {?x nameSpace:hasName ?name . ?x nameSpace:hasTopic ?topic . ?x ns:type ?type . FILTER (regex(?topic,\"(^|((.)* ))"+other.get(i)+"($| ((.)*))\",'i'))}}";
	 		
	 	
	 	

		 	System.out.println("->>>>>"+other.get(i));
		    QueryExecution qExec = QueryExecutionFactory.create(queryString, dataset) ;
		    ResultSet rs = qExec.execSelect() ;
		    String columnName;

				while(rs.hasNext()){
			
					QuerySolution row= rs.next();
					Iterator columns=row.varNames();
					Tuple1 tuple=new Tuple1();
				
					while (columns.hasNext()) {
						
					      RDFNode cell = row.get((columnName=columns.next().toString()));

				    	  if(columnName.compareTo("name")==0){
				    		  String campoAux=cell.asLiteral().toString();
				    		  name=campoAux.substring(0,campoAux.indexOf('^'));
				    		  tuple.name=name;
				    	  }else if(columnName.compareTo("type")==0){
				    		  Node campoAux=cell.asNode();
				    		  name=campoAux.getURI();
				    		  tuple.type=name;
				    	  }
				    	 System.out.println(name);
					}
					list.add(tuple);
				}
			
			
		}
		
		return list;
	}
	
}

class Tuple1{
	String name;
	String type;
	
	Tuple1(){}
}