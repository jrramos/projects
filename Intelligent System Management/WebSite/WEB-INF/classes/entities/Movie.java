package entities;

import java.util.ArrayList;
import java.util.List;




public class Movie {
	
	private List<Genre> genres;
	
	private List<Language> languages;
	
	private List<Crew> crews;
	
	private List<Actor> actors;
	
	private int year;

	private int id;
	private double rank;

	private String description;

	private String title;
	
	public Movie(){
		crews=new ArrayList<Crew>();
		languages=new ArrayList<Language>();
		genres=new ArrayList<Genre>();
		actors=new ArrayList<Actor>();
		crews=new ArrayList<Crew>();
	}
	
	public Movie(int id,String title,int year,double rank,String description){
		this.setId(id);
		this.setTitle(title);
		this.setYear(year);
		this.setRank(rank);
		this.setDescription(description);
		crews=new ArrayList<Crew>();
		languages=new ArrayList<Language>();
		genres=new ArrayList<Genre>();
		actors=new ArrayList<Actor>();
		crews=new ArrayList<Crew>();
	}
	
	public Actor getActors(int i) {
		return actors.get(i);
	}

	public void setActor(Actor actor) {
		actors.add(actor);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public double getRank() {
		return rank;
	}

	public void setRank(double rank) {
		this.rank = rank;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Genre getGenre(int i) {
		return genres.get(i);
	}

	public void setGenre(Genre genre) {
		genres.add(genre);
	}
	
	public void setLanguage(Language language) {
		languages.add(language);
	}
	
	public Language getLanguage(int i) {
		return languages.get(i);
	}
	
	
	public void setCrew(Crew crew) {
		crews.add(crew);
	}
	
	public Crew getCrew(int i) {
		return crews.get(i);
	}
	
	public int getGenresSize(){
		return genres.size();
	}
	
	public int getCrewsSize(){
		return crews.size();
	}
	
	public int getActorsSize(){
		return actors.size();
	}
	
	public int getLanguagesSize(){
		return languages.size();
	}
}
