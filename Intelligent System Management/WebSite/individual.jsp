<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8"> 
    <title>.:Movie Website:.</title>
    <link rel="stylesheet" href="css/bootstrap.css"  type="text/css"/>
    <style type="text/css">
      .divElement{
        position: absolute;
        left: 50%;
        margin-left: -50px;
      }​
      .divElement{
        position: absolute;
        left: 50%;
        margin-left: -50px;
      }​
      .box-content {
          display: inline-block;
          width: 250px;
      }

      .border-row {
          border-bottom: 1px solid #ccc;
      }

      .left {
          border-right: 1px solid #ccc;
      }
    </style>
  </head>

<body>
   <jsp:include page="header.jsp"></jsp:include>
   <%@page import="beans.*,entities.*,java.util.ArrayList"%>
<%
    DBBean bean=new DBBean();
    ArrayList<Tuple> aux=new ArrayList<Tuple>();
    if(request.getParameter("p")!=null){
%>
<div class="span10">
<b>Person Name:<%=request.getParameter("name")%> </b>
<%
  aux=bean.getJobsByPersonURI(Integer.parseInt(request.getParameter("p")));

  if(aux.size()!=0){
%>
<br><br>
<div class="border-row">
<br>
</div>

<h4>Crew Jobs </h4>
<br>
<table class="table table-striped">  
        <thead>  
          <tr>  
            <th>Job</th>  
            <th>Movie</th>  
          </tr>  
        </thead> 
        <tbody> 
<% 
  
    for(int i=0;i<aux.size();i++)
      out.println("<tr><td><a href='job.jsp?p="+aux.get(i).getId()+"&name="+aux.get(i).getName()+"'>"+aux.get(i).getName()+"</a></td><td><a href='movie.jsp?p="+aux.get(i).getId1()+"&name="+aux.get(i).getName1()+"'>"+aux.get(i).getName1()+"</td>");


  }
%>
  </tbody>  
      </table>  
<%
  aux=bean.getCharactersByPersonURI(Integer.parseInt(request.getParameter("p")));

  if(aux.size()!=0){
%>

<br><br>
<div class="border-row">
<br>
</div>
<h4>Cast Jobs </h4>
<br>
<table class="table table-striped">  
        <thead>  
          <tr>  
            <th>Character</th>  
            <th>Movie</th>  
          </tr>  
        </thead>  
        <tbody>  
<% 
  aux=bean.getCharactersByPersonURI(Integer.parseInt(request.getParameter("p")));
  for(int i=0;i<aux.size();i++)
    out.println("<tr><td><a href='character.jsp?p="+aux.get(i).getId()+"&name="+aux.get(i).getName()+"'>"+aux.get(i).getName()+"</a></td><td><a href='movie.jsp?p="+aux.get(i).getId1()+"&name="+aux.get(i).getName1()+"'>"+aux.get(i).getName1()+"</a></td>");
%>
</tbody>  
      </table>  

<%
  }
    }

%>
</div>
</body>
</html>