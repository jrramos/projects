/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package opennlp;

/**
 *
 * @author c
 */

import java.util.Comparator;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import opennlp.tools.cmdline.PerformanceMonitor;
import opennlp.tools.cmdline.parser.TaggerModelReplacerTool;
import opennlp.tools.cmdline.postag.POSModelLoader;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSSample;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.tokenize.WhitespaceTokenizer;
import opennlp.tools.util.InvalidFormatException;
import opennlp.tools.util.ObjectStream;
import opennlp.tools.util.PlainTextByLineStream;

import org.apache.commons.collections.comparators.ComparableComparator;


import org.tartarus.snowball.ext.PorterStemmer;

public class OpenNLP {

	String path ="C:\\Users\\c\\Desktop\\SIGC\\Project\\Meta2\\";
        POSModel model;
        
        ArrayList<String>listaPalavras = new ArrayList<>();
        ArrayList<Sentiment> sentimentWordsList = new ArrayList<>();
        ArrayList<String> sentenceArray  = new ArrayList<>();
        ArrayList<Sentence> mainArray = new ArrayList<>();
	ArrayList<Phrases> phrases = new ArrayList<>();
        ArrayList<String>listaNomeAnotados = new ArrayList<>();
        ArrayList<String>listaVerbosAnotados = new ArrayList<>();
        ArrayList<String>listaAdjectivosAnotados = new ArrayList<>();
        ArrayList<String>listaAdverbiosAnotados = new ArrayList<>();
        ArrayList<String> listNames = new ArrayList<>();
        
	ArrayList<SentencePhrase> arraySeparated = new ArrayList<>();
        
	public static void main(String[] args) throws IOException {
        new OpenNLP();
	}
	
	public OpenNLP() throws IOException{
            model =  new POSModel((new FileInputStream(path+ "libs\\en-pos-maxent.bin")));
            ExtractKnowledge("testes\\"+"sentiments-en.txt");
            ExtractNames("testes\\"+"listNamesWoman.txt");
            ExtractNames("testes\\"+"listNamesMan");
            SentenceDetect("testes\\"+"teste.txt");
            parse();
      /*      System.out.println("Nomes "+ listaNomeAnotados.size()+"\n"
                    +"Verbos "+ listaVerbosAnotados.size()+"\n"+
                    "Adjectivos "+ listaAdjectivosAnotados.size()+"\n"+
                    "Adverbios "+ listaAdverbiosAnotados.size()+"\n");
            int soma = listaNomeAnotados.size()+listaVerbosAnotados.size()+listaAdjectivosAnotados.size()+listaAdverbiosAnotados.size();
            System.out.println(soma);*/
            ExtractSentiment();
            
            
            saveToFile();
        }
        
        public void saveToFile() throws FileNotFoundException, UnsupportedEncodingException{
            PrintWriter writer = new PrintWriter(path+"names.txt", "UTF-8");
            
            for(String name : listNames)
                writer.println(name);

            writer.close();
        }
        
        public double checkInList(String palavra){
            for(Sentiment sent : sentimentWordsList){   
                
                if (sent.word.equals(palavra)){
                    return sent.value;
                }
            }
            return 0.0;
        }
        public void ExtractSentiment(){
            
            POSTaggerME  posTagger = new POSTaggerME(model);
            for(int i = 0;i< arraySeparated.size();i++){
                double value =0.0;
                String [] div = arraySeparated.get(i).noum.split(";");
                for (int j=0;j<div.length;j++){
                    String pos = posTagger.tag(div[j]);
                    if (verNomes(div[j].toUpperCase())==false){
                        arraySeparated.get(i).suj += div[j]+",";
                    }
                }
                
                String [] cenas = arraySeparated.get(i).noum.split(";");
                for (String cen : cenas )
                    value+=checkInList(cen);

                
                value+=checkInList(arraySeparated.get(i).adv)/6;
                
//                if (checkInList(arraySeparated.get(i).adj)>0){
//                    //positivo
//                    if (Value>=0)
//                        value+=checkInList(arraySeparated.get(i).adj);
//                    else 
//                        value-=checkInList(arraySeparated.get(i).adj);
//                        
//                }
//                else {
//                    //negativo
//                    if (Value>=0.0){
//                        value+=(checkInList(arraySeparated.get(i).adj));
//                    }
//                    
//                    else 
//                        value-=checkInList(arraySeparated.get(i).adj);
//                }
//                
                Double valorAdv = checkInList(arraySeparated.get(i).adv);
                
                Double Value = checkInList(arraySeparated.get(i).adj);
                Double valorVerbo =checkInList(arraySeparated.get(i).verb);
                Double valorFinal = Value + valorVerbo;
              //  System.out.println((arraySeparated.get(i).adv));
                if (valorAdv>=0.0)
                    value+=valorFinal;
                else 
                    value-=valorFinal;

                    //nome + adj + verb + adv
                arraySeparated.get(i).value = value;
            }
            
            for(int i = 0;i< arraySeparated.size();i++){
                if (arraySeparated.get(i).value>=0.0)
                    System.out.println("temática " +arraySeparated.get(i).suj +" foi positiva " );
                else
                    System.out.println("temática " +arraySeparated.get(i).suj +" foi negativa " );
                
            }
            
        }
        
        
        
        
        public boolean verNomes(String nome ){
            for (String tt : listNames){
                if(tt.toLowerCase().equals(nome.toLowerCase())){
                    return true;
                }
            }
            return false;
        }
	
        public boolean separate(int i){
            String [] separate = {".",",",";","!","?"};
            for (int j = 0;j< separate.length;j++){
                if (mainArray.get(i).tag.equals(separate[j]))
                    return true;
            }
            return false;
        }
        
        
        public void parse () throws IOException{
            int ini = 0;
            for(int i = 0;i< mainArray.size();i++){
                if (separate(i)){
                    ArrayList<Sentence> aa = new ArrayList<>();
                    if (ini ==0){
                        for(int j = ini;j<=i;j++){
                            aa.add(mainArray.get(j));
                        }
                    }
                    else{
                        for(int j = ini+1;j<=i;j++){
                            aa.add(mainArray.get(j));
                       }
                    }
                    phrases.add(new Phrases(aa));
                    ini = i;
                }
            }
            for (int i= 0;i< phrases.size();i++){
                if (phrases.get(i).phrase.get(phrases.get(i).phrase.size()-1).nome.equals("but")){
                    phrases.get(i).intention = false;
                }
                phrases.get(i).phrase.remove(phrases.get(i).phrase.size()-1);
            }
            parsePhrases();
        }
        
        public void parsePhrases() throws IOException{            
            String NN ="";
            String VERB ="";
            String ADJ ="";
            String  ADV = "";
            for (int i= 0;i< phrases.size();i++){
                NN ="";ADV = "";ADJ ="";VERB ="";
                for (int j= 0;j< phrases.get(i).phrase.size();j++){
                    if (phrases.get(i).phrase.get(j).tag.equals("NNPS")||phrases.get(i).phrase.get(j).tag.equals("NNS")||phrases.get(i).phrase.get(j).tag.equals("NNP")||phrases.get(i).phrase.get(j).tag.equals("NN")){
                        NN += phrases.get(i).phrase.get(j).nome+";";
                    }
                    else if (phrases.get(i).phrase.get(j).tag.equals("VBZ")||phrases.get(i).phrase.get(j).tag.equals("VB")||phrases.get(i).phrase.get(j).tag.equals("VBP")||phrases.get(i).phrase.get(j).tag.equals("VBN")||phrases.get(i).phrase.get(j).tag.equals("VBD")||phrases.get(i).phrase.get(j).tag.equals("VBG")){
                        VERB = phrases.get(i).phrase.get(j).nome;
                    }
                    else if (phrases.get(i).phrase.get(j).tag.equals("JJ")||phrases.get(i).phrase.get(j).tag.equals("JJR")|phrases.get(i).phrase.get(j).tag.equals("JJS")){
                        ADJ = phrases.get(i).phrase.get(j).nome;
                    }
                    else if (phrases.get(i).phrase.get(j).tag.equals("RB")||phrases.get(i).phrase.get(j).tag.equals("IN") ){
                        ADV += phrases.get(i).phrase.get(j).nome;
                    }  
                        
                }
                arraySeparated.add(new SentencePhrase(NN,VERB,ADV,ADJ,i));
            }
            printSeparated();
        }
        
        
        public void printSeparated(){
            for(int i = 0;i< arraySeparated.size();i++)
                System.out.println("["+arraySeparated.get(i).noum+","+arraySeparated.get(i).adj+","+arraySeparated.get(i).verb+","+arraySeparated.get(i).adv+"]");
        }
        
        
	public void ExtractKnowledge(String filename)  {
            int i;
            String folder=path;
            String[] tokens;
            try {
                ArrayList<String> paragraph=readFile(folder+filename);
                createCategories(paragraph);
            } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
            }
	}
        
        
	public void ExtractNames(String filename)  {
            int i;
            String folder=path;
            String[] tokens;
            try {
                ArrayList<String> paragraph=readFile(folder+filename);
                for(String pa : paragraph)
                    listNames.add(pa.split("!")[0]);
            
            } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
            }
	}
        
	public void SentenceDetect(String filename)  {
            int i;
            String folder=path;
            String[] tokens;
            try {
                ArrayList<String> paragraph=readFile(folder+filename);
                parsingNews(paragraph);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
	}
        
        public void createCategories(ArrayList<String>paragraph){
            for(int i =0;i< paragraph.size();i++){
                
                String nome =paragraph.get(i).split("!")[1];
                double prob =Double.parseDouble(paragraph.get(i).split("!")[0]);
                sentimentWordsList.add(new Sentiment(nome,prob));
                
            }
            try {
                POSTAGSentiments ();
            } catch (IOException ex) {
                Logger.getLogger(OpenNLP.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
	
        
        
	public void parsingNews(ArrayList<String> paragraph) throws InvalidFormatException, IOException{
            int size=0;

            Set<String> words=new HashSet<String>();

            while(size<paragraph.size()){
                InputStream is;
                is = new FileInputStream(path+ "libs\\en-sent.bin");
                SentenceModel model = new SentenceModel(is);
                SentenceDetectorME sdetector = new SentenceDetectorME(model);
                String sentences[] = sdetector.sentDetect(paragraph.get(size));

                for(int i=0;i<sentences.length;i++){
                    ArrayList<Sentence> aux = POSTAG(sentences[i]) ;
                    for(Sentence aa : aux)
                        mainArray.add(aa);

                }	
                size++;
            }
	}

   
        public ArrayList<Sentence> POSTAG(String phrase) throws IOException{
            ArrayList<Sentence> array = new ArrayList<>();
            POSTaggerME  posTagger = new POSTaggerME(model);

            InputStream is = new FileInputStream(path+ "libs\\en-token.bin");
            TokenizerModel model1 = new TokenizerModel(is);
            
            Tokenizer tokenizer = new TokenizerME(model1);
            
            ObjectStream<String> lineStream = new PlainTextByLineStream(new StringReader(phrase));

            String line;
            int i;
            String samples = "";
            while ((line = lineStream.read()) != null) {
                    i=0;
                    String whitespaceTokenizerLine[] = tokenizer.tokenize(line);
                    String [] tags = posTagger.tag(whitespaceTokenizerLine);
                    POSSample sample = new POSSample(whitespaceTokenizerLine, tags);
                    for (String tag: sample.getTags()){
                        Sentence aa = new Sentence(whitespaceTokenizerLine[i],tag);
                        i++;
                        array.add(aa);
                    }
            
            }
            return array;
            
        }
        public void POSTAGSentiments () throws IOException{
            POSTaggerME  posTagger = new POSTaggerME(model);

            
            
            for(Sentiment sent : sentimentWordsList){
                String tags = posTagger.tag(stemTerm(sent.word));
                if (tags.split("/").length == 2){
                    sent.postag = tags.split("/")[1];
                }
            }
            
            for(Sentiment sent :sentimentWordsList){
                String NN ="", VERB = "", ADJ= "", ADV ="";
                if (sent.postag.equals("NNPS")||sent.postag.equals("NNS")||sent.postag.equals("NNP")||sent.postag.equals("NN")){
                    listaNomeAnotados.add(sent.word);
                }
                if (sent.postag.equals("VBZ")||sent.postag.equals("VB")||sent.postag.equals("VBP")||sent.postag.equals("VBN")
                        ||sent.postag.equals("VBD")||sent.postag.equals("VBG")){
                    listaVerbosAnotados.add(sent.word);
                }
                if (sent.postag.equals("JJ")||sent.postag.equals("JJR")||sent.postag.equals("JJS")){
                    listaAdjectivosAnotados.add(sent.word);
                }
                if (sent.postag.equals("RB")){
                    listaAdverbiosAnotados.add(sent.word);
                }  
            }
            
            
        }
	
	public static String stemTerm (String term) {
	    PorterStemmer stemmer = new PorterStemmer();
	    stemmer.setCurrent(term);
	    stemmer.stem();
	    return stemmer.getCurrent();
	}

	public String[] tokenizeSentences(String phrase) throws InvalidFormatException, IOException {
		InputStream is = new FileInputStream(path+ "libs\\en-token.bin");
	
		TokenizerModel model = new TokenizerModel(is);
	
		Tokenizer tokenizer = new TokenizerME(model);
	
		String tokens[] = tokenizer.tokenize(phrase);
	
		is.close();
		
		return tokens;
	}
	
	ArrayList<String> readFile(String fileName) throws IOException {
		
	    BufferedReader br = new BufferedReader(new FileReader(fileName));
	    ArrayList<String> paragraph=new  ArrayList<String>();
	    try {
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();

	        while (line != null) {
	        	paragraph.add(line);
	            line = br.readLine();
	        }
	        return paragraph;
	    } finally {
	        br.close();
	    }
	}
}
